using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using HslCommunication.BasicFramework;
using HslCommunication.Core;
using HslCommunication.Core.IMessage;
using HslCommunication.Core.Net;

namespace HslCommunication.Profinet.Omron
{
	/// <summary>
	/// 欧姆龙的虚拟服务器，支持DM区，CIO区，Work区，Hold区，Auxiliary区，可以方便的进行测试<br />
	/// Omron's virtual server supports DM area, CIO area, Work area, Hold area, and Auxiliary area, which can be easily tested
	/// </summary>
	public class OmronFinsServer : NetworkDataServerBase
	{
		private SoftBuffer dBuffer;

		private SoftBuffer cioBuffer;

		private SoftBuffer wBuffer;

		private SoftBuffer hBuffer;

		private SoftBuffer arBuffer;

		private const int DataPoolLength = 65536;

		/// <inheritdoc cref="P:HslCommunication.Core.ByteTransformBase.DataFormat" />
		public DataFormat DataFormat
		{
			get
			{
				return base.ByteTransform.DataFormat;
			}
			set
			{
				base.ByteTransform.DataFormat = value;
			}
		}

		/// <summary>
		/// 实例化一个Fins协议的服务器<br />
		/// Instantiate a Fins protocol server
		/// </summary>
		public OmronFinsServer()
		{
			dBuffer = new SoftBuffer(131072);
			cioBuffer = new SoftBuffer(131072);
			wBuffer = new SoftBuffer(131072);
			hBuffer = new SoftBuffer(131072);
			arBuffer = new SoftBuffer(131072);
			dBuffer.IsBoolReverseByWord = true;
			cioBuffer.IsBoolReverseByWord = true;
			wBuffer.IsBoolReverseByWord = true;
			hBuffer.IsBoolReverseByWord = true;
			arBuffer.IsBoolReverseByWord = true;
			base.WordLength = 1;
			base.ByteTransform = new ReverseWordTransform();
			base.ByteTransform.DataFormat = DataFormat.CDAB;
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Omron.OmronFinsNet.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<OmronFinsDataType, byte[]> operateResult = OmronFinsNetHelper.AnalysisAddress(address, isBit: false);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.DM.WordCode)
			{
				return OperateResult.CreateSuccessResult(dBuffer.GetBytes((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2, length * 2));
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.CIO.WordCode)
			{
				return OperateResult.CreateSuccessResult(cioBuffer.GetBytes((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2, length * 2));
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.WR.WordCode)
			{
				return OperateResult.CreateSuccessResult(wBuffer.GetBytes((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2, length * 2));
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.HR.WordCode)
			{
				return OperateResult.CreateSuccessResult(hBuffer.GetBytes((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2, length * 2));
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.AR.WordCode)
			{
				return OperateResult.CreateSuccessResult(arBuffer.GetBytes((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2, length * 2));
			}
			return new OperateResult<byte[]>(StringResources.Language.NotSupportedFunction);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Omron.OmronFinsNet.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			OperateResult<OmronFinsDataType, byte[]> operateResult = OmronFinsNetHelper.AnalysisAddress(address, isBit: false);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (operateResult.Content1.WordCode == OmronFinsDataType.DM.WordCode)
			{
				dBuffer.SetBytes(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2);
			}
			else if (operateResult.Content1.WordCode == OmronFinsDataType.CIO.WordCode)
			{
				cioBuffer.SetBytes(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2);
			}
			else if (operateResult.Content1.WordCode == OmronFinsDataType.WR.WordCode)
			{
				wBuffer.SetBytes(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2);
			}
			else if (operateResult.Content1.WordCode == OmronFinsDataType.HR.WordCode)
			{
				hBuffer.SetBytes(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2);
			}
			else
			{
				if (operateResult.Content1.WordCode != OmronFinsDataType.AR.WordCode)
				{
					return new OperateResult(StringResources.Language.NotSupportedFunction);
				}
				arBuffer.SetBytes(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 2);
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Omron.OmronFinsNet.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			OperateResult<OmronFinsDataType, byte[]> operateResult = OmronFinsNetHelper.AnalysisAddress(address, isBit: true);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.DM.BitCode)
			{
				return OperateResult.CreateSuccessResult(dBuffer.GetBool((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2], length));
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.CIO.BitCode)
			{
				return OperateResult.CreateSuccessResult(cioBuffer.GetBool((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2], length));
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.WR.BitCode)
			{
				return OperateResult.CreateSuccessResult(wBuffer.GetBool((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2], length));
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.HR.BitCode)
			{
				return OperateResult.CreateSuccessResult(hBuffer.GetBool((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2], length));
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.AR.BitCode)
			{
				return OperateResult.CreateSuccessResult(arBuffer.GetBool((operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2], length));
			}
			return new OperateResult<bool[]>(StringResources.Language.NotSupportedFunction);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Omron.OmronFinsNet.Write(System.String,System.Boolean[])" />
		public override OperateResult Write(string address, bool[] value)
		{
			OperateResult<OmronFinsDataType, byte[]> operateResult = OmronFinsNetHelper.AnalysisAddress(address, isBit: true);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			if (operateResult.Content1.BitCode == OmronFinsDataType.DM.BitCode)
			{
				dBuffer.SetBool(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2]);
			}
			else if (operateResult.Content1.BitCode == OmronFinsDataType.CIO.BitCode)
			{
				cioBuffer.SetBool(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2]);
			}
			else if (operateResult.Content1.BitCode == OmronFinsDataType.WR.BitCode)
			{
				wBuffer.SetBool(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2]);
			}
			else if (operateResult.Content1.BitCode == OmronFinsDataType.HR.BitCode)
			{
				hBuffer.SetBool(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2]);
			}
			else
			{
				if (operateResult.Content1.BitCode != OmronFinsDataType.AR.BitCode)
				{
					return new OperateResult(StringResources.Language.NotSupportedFunction);
				}
				arBuffer.SetBool(value, (operateResult.Content2[0] * 256 + operateResult.Content2[1]) * 16 + operateResult.Content2[2]);
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc />
		protected override void ThreadPoolLoginAfterClientCheck(Socket socket, IPEndPoint endPoint)
		{
			FinsMessage netMessage = new FinsMessage();
			OperateResult<byte[]> operateResult = ReceiveByMessage(socket, 5000, netMessage);
			if (!operateResult.IsSuccess)
			{
				return;
			}
			OperateResult operateResult2 = Send(socket, SoftBasic.HexStringToBytes("46 49 4E 53 00 00 00 10 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01"));
			if (operateResult2.IsSuccess)
			{
				AppSession appSession = new AppSession();
				appSession.IpEndPoint = endPoint;
				appSession.WorkSocket = socket;
				try
				{
					socket.BeginReceive(new byte[0], 0, 0, SocketFlags.None, SocketAsyncCallBack, appSession);
					AddClient(appSession);
				}
				catch
				{
					socket.Close();
					base.LogNet?.WriteDebug(ToString(), string.Format(StringResources.Language.ClientOfflineInfo, endPoint));
				}
			}
		}

		private void SocketAsyncCallBack(IAsyncResult ar)
		{
			AppSession appSession = ar.AsyncState as AppSession;
			if (appSession == null)
			{
				return;
			}
			try
			{
				int num = appSession.WorkSocket.EndReceive(ar);
				OperateResult<byte[]> operateResult = ReceiveByMessage(appSession.WorkSocket, 5000, new FinsMessage());
				if (!operateResult.IsSuccess)
				{
					RemoveClient(appSession);
					return;
				}
				base.LogNet?.WriteDebug(ToString(), "Tcp " + StringResources.Language.Receive + "：" + operateResult.Content.ToHexString(' '));
				byte[] array = ReadFromFinsCore(operateResult.Content);
				if (array != null)
				{
					appSession.WorkSocket.Send(array);
					base.LogNet?.WriteDebug(ToString(), "Tcp " + StringResources.Language.Send + "：" + array.ToHexString(' '));
					RaiseDataReceived(operateResult.Content);
					appSession.WorkSocket.BeginReceive(new byte[0], 0, 0, SocketFlags.None, SocketAsyncCallBack, appSession);
				}
				else
				{
					RemoveClient(appSession);
				}
			}
			catch
			{
				RemoveClient(appSession);
			}
		}

		/// <summary>
		/// 当收到mc协议的报文的时候应该触发的方法，允许继承重写，来实现自定义的返回，或是数据监听。
		/// </summary>
		/// <param name="finsCore">mc报文</param>
		/// <returns>返回的报文信息</returns>
		protected virtual byte[] ReadFromFinsCore(byte[] finsCore)
		{
			if (finsCore[26] == 1 && finsCore[27] == 1)
			{
				return PackCommand(ReadByCommand(SoftBasic.ArrayRemoveBegin(finsCore, 26)));
			}
			if (finsCore[26] == 1 && finsCore[27] == 2)
			{
				return PackCommand(WriteByMessage(SoftBasic.ArrayRemoveBegin(finsCore, 26)));
			}
			return null;
		}

		private byte[] PackCommand(byte[] data)
		{
			byte[] array = new byte[30 + data.Length];
			SoftBasic.HexStringToBytes("46 49 4E 53 00 00 00 0000 00 00 00 00 00 00 0000 00 00 00 00 00 00 00 00 00 00 00 00 00").CopyTo(array, 0);
			if (data.Length != 0)
			{
				data.CopyTo(array, 30);
			}
			byte[] bytes = BitConverter.GetBytes(array.Length - 8);
			Array.Reverse(bytes);
			bytes.CopyTo(array, 4);
			return array;
		}

		private byte[] ReadByCommand(byte[] command)
		{
			if (command[2] == OmronFinsDataType.DM.BitCode || command[2] == OmronFinsDataType.CIO.BitCode || command[2] == OmronFinsDataType.WR.BitCode || command[2] == OmronFinsDataType.HR.BitCode || command[2] == OmronFinsDataType.AR.BitCode)
			{
				ushort length = (ushort)(command[6] * 256 + command[7]);
				int destIndex = (command[3] * 256 + command[4]) * 16 + command[5];
				if (command[2] == OmronFinsDataType.DM.BitCode)
				{
					return (from m in dBuffer.GetBool(destIndex, length)
						select (byte)(m ? 1 : 0)).ToArray();
				}
				if (command[2] == OmronFinsDataType.CIO.BitCode)
				{
					return (from m in cioBuffer.GetBool(destIndex, length)
						select (byte)(m ? 1 : 0)).ToArray();
				}
				if (command[2] == OmronFinsDataType.WR.BitCode)
				{
					return (from m in wBuffer.GetBool(destIndex, length)
						select (byte)(m ? 1 : 0)).ToArray();
				}
				if (command[2] == OmronFinsDataType.HR.BitCode)
				{
					return (from m in hBuffer.GetBool(destIndex, length)
						select (byte)(m ? 1 : 0)).ToArray();
				}
				if (command[2] == OmronFinsDataType.AR.BitCode)
				{
					return (from m in arBuffer.GetBool(destIndex, length)
						select (byte)(m ? 1 : 0)).ToArray();
				}
				throw new Exception(StringResources.Language.NotSupportedDataType);
			}
			if (command[2] == OmronFinsDataType.DM.WordCode || command[2] == OmronFinsDataType.CIO.WordCode || command[2] == OmronFinsDataType.WR.WordCode || command[2] == OmronFinsDataType.HR.WordCode || command[2] == OmronFinsDataType.AR.WordCode)
			{
				ushort num = (ushort)(command[6] * 256 + command[7]);
				int num2 = command[3] * 256 + command[4];
				if (command[2] == OmronFinsDataType.DM.WordCode)
				{
					return dBuffer.GetBytes(num2 * 2, num * 2);
				}
				if (command[2] == OmronFinsDataType.CIO.WordCode)
				{
					return cioBuffer.GetBytes(num2 * 2, num * 2);
				}
				if (command[2] == OmronFinsDataType.WR.WordCode)
				{
					return wBuffer.GetBytes(num2 * 2, num * 2);
				}
				if (command[2] == OmronFinsDataType.HR.WordCode)
				{
					return hBuffer.GetBytes(num2 * 2, num * 2);
				}
				if (command[2] == OmronFinsDataType.AR.WordCode)
				{
					return arBuffer.GetBytes(num2 * 2, num * 2);
				}
				throw new Exception(StringResources.Language.NotSupportedDataType);
			}
			return new byte[0];
		}

		private byte[] WriteByMessage(byte[] command)
		{
			if (command[2] == OmronFinsDataType.DM.BitCode || command[2] == OmronFinsDataType.CIO.BitCode || command[2] == OmronFinsDataType.WR.BitCode || command[2] == OmronFinsDataType.HR.BitCode || command[2] == OmronFinsDataType.AR.BitCode)
			{
				ushort num = (ushort)(command[6] * 256 + command[7]);
				int destIndex = (command[3] * 256 + command[4]) * 16 + command[5];
				bool[] value = (from m in SoftBasic.ArrayRemoveBegin(command, 8)
					select m == 1).ToArray();
				if (command[2] == OmronFinsDataType.DM.BitCode)
				{
					dBuffer.SetBool(value, destIndex);
				}
				else if (command[2] == OmronFinsDataType.CIO.BitCode)
				{
					cioBuffer.SetBool(value, destIndex);
				}
				else if (command[2] == OmronFinsDataType.WR.BitCode)
				{
					wBuffer.SetBool(value, destIndex);
				}
				else if (command[2] == OmronFinsDataType.HR.BitCode)
				{
					hBuffer.SetBool(value, destIndex);
				}
				else
				{
					if (command[2] != OmronFinsDataType.AR.BitCode)
					{
						throw new Exception(StringResources.Language.NotSupportedDataType);
					}
					arBuffer.SetBool(value, destIndex);
				}
				return new byte[0];
			}
			ushort num2 = (ushort)(command[6] * 256 + command[7]);
			int num3 = command[3] * 256 + command[4];
			byte[] data = SoftBasic.ArrayRemoveBegin(command, 8);
			if (command[2] == OmronFinsDataType.DM.WordCode)
			{
				dBuffer.SetBytes(data, num3 * 2);
			}
			else if (command[2] == OmronFinsDataType.CIO.WordCode)
			{
				cioBuffer.SetBytes(data, num3 * 2);
			}
			else if (command[2] == OmronFinsDataType.WR.WordCode)
			{
				wBuffer.SetBytes(data, num3 * 2);
			}
			else if (command[2] == OmronFinsDataType.HR.WordCode)
			{
				hBuffer.SetBytes(data, num3 * 2);
			}
			else
			{
				if (command[2] != OmronFinsDataType.AR.WordCode)
				{
					throw new Exception(StringResources.Language.NotSupportedDataType);
				}
				arBuffer.SetBytes(data, num3 * 2);
			}
			return new byte[0];
		}

		/// <inheritdoc />
		protected override void LoadFromBytes(byte[] content)
		{
			if (content.Length < 786432)
			{
				throw new Exception("File is not correct");
			}
			dBuffer.SetBytes(content, 0, 0, 131072);
			cioBuffer.SetBytes(content, 131072, 0, 131072);
			wBuffer.SetBytes(content, 262144, 0, 131072);
			hBuffer.SetBytes(content, 393216, 0, 131072);
			arBuffer.SetBytes(content, 524288, 0, 131072);
			arBuffer.SetBytes(content, 655360, 0, 131072);
		}

		/// <inheritdoc />
		protected override byte[] SaveToBytes()
		{
			byte[] array = new byte[786432];
			Array.Copy(dBuffer.GetBytes(), 0, array, 0, 131072);
			Array.Copy(cioBuffer.GetBytes(), 0, array, 131072, 131072);
			Array.Copy(wBuffer.GetBytes(), 0, array, 262144, 131072);
			Array.Copy(hBuffer.GetBytes(), 0, array, 393216, 131072);
			Array.Copy(arBuffer.GetBytes(), 0, array, 524288, 131072);
			Array.Copy(arBuffer.GetBytes(), 0, array, 655360, 131072);
			return array;
		}

		/// <inheritdoc cref="M:System.IDisposable.Dispose" />
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				dBuffer?.Dispose();
				cioBuffer?.Dispose();
				wBuffer?.Dispose();
				hBuffer?.Dispose();
				arBuffer?.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"OmronFinsServer[{base.Port}]";
		}
	}
}
