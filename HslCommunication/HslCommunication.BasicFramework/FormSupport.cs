using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using HslCommunication.Properties;

namespace HslCommunication.BasicFramework
{
	/// <summary>
	/// 作者的技术支持的窗口界面
	/// </summary>
	public class FormSupport : Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		private PictureBox pictureBox1;

		private Label label1;

		private Label label3;

		private Label label4;

		private Label label5;

		private PictureBox pictureBox2;

		private Label label6;

		private Label label2;

		private Label label7;

		private Label label8;

		private Label label9;

		private Label label10;

		/// <summary>
		/// 实例化一个默认的界面
		/// </summary>
		public FormSupport()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			pictureBox1 = new System.Windows.Forms.PictureBox();
			pictureBox2 = new System.Windows.Forms.PictureBox();
			label6 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			label7 = new System.Windows.Forms.Label();
			label8 = new System.Windows.Forms.Label();
			label9 = new System.Windows.Forms.Label();
			label10 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
			((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
			SuspendLayout();
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(32, 356);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(608, 17);
			label1.TabIndex = 1;
			label1.Text = "如果这个组件真的帮到了你或你们公司，那么非常感谢您的支持，个人打赏，请视个人能力选择金额，感谢支持。";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(32, 501);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(560, 17);
			label3.TabIndex = 3;
			label3.Text = "如果您的公司使用了本产品，那么非常感谢对本产品的信任，企业赞助或是合作请加群后专门联系作者。";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(312, 519);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(387, 17);
			label4.TabIndex = 4;
			label4.Text = "作者：Richard.Hu 上图为支付宝和微信账户的收钱码，请认准官方账户";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(32, 373);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(488, 17);
			label5.TabIndex = 5;
			label5.Text = "如果不小心点错了，需要退款，请通过邮箱联系作者，提供付款的截图或是其他证明即可。";
			pictureBox1.Image = HslCommunication.Properties.Resources.alipay;
			pictureBox1.Location = new System.Drawing.Point(35, 13);
			pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(267, 339);
			pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox1.TabIndex = 0;
			pictureBox1.TabStop = false;
			pictureBox2.Image = HslCommunication.Properties.Resources.mm_facetoface_collect_qrcode_1525331158525;
			pictureBox2.Location = new System.Drawing.Point(411, 10);
			pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			pictureBox2.Name = "pictureBox2";
			pictureBox2.Size = new System.Drawing.Size(247, 342);
			pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox2.TabIndex = 6;
			pictureBox2.TabStop = false;
			label6.AutoSize = true;
			label6.ForeColor = System.Drawing.Color.Blue;
			label6.Location = new System.Drawing.Point(32, 396);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(653, 34);
			label6.TabIndex = 7;
			label6.Text = "技术支持及探讨学习群，需要赞助240元以上才能加入，申请入群时请提供微信或是支付宝的付款时间，方便管理员核对，\r\n谢谢支持：群号：838185568  群功能如下：";
			label2.AutoSize = true;
			label2.ForeColor = System.Drawing.Color.FromArgb(192, 64, 0);
			label2.Location = new System.Drawing.Point(32, 439);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(226, 17);
			label2.TabIndex = 8;
			label2.Text = "1. 作者给与一定的经验分享，疑问解答。";
			label7.AutoSize = true;
			label7.ForeColor = System.Drawing.Color.FromArgb(192, 64, 0);
			label7.Location = new System.Drawing.Point(352, 439);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(262, 17);
			label7.TabIndex = 9;
			label7.Text = "2. 群里聚集了各个行业的朋友，行业经验交流。";
			label8.AutoSize = true;
			label8.ForeColor = System.Drawing.Color.FromArgb(192, 64, 0);
			label8.Location = new System.Drawing.Point(32, 456);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(228, 17);
			label8.TabIndex = 10;
			label8.Text = "3. 探讨交流上位机开发，MES系统开发。";
			label9.AutoSize = true;
			label9.ForeColor = System.Drawing.Color.FromArgb(192, 64, 0);
			label9.Location = new System.Drawing.Point(352, 456);
			label9.Name = "label9";
			label9.Size = new System.Drawing.Size(310, 17);
			label9.TabIndex = 11;
			label9.Text = "4. 时不时会有项目需求发布，面向个人，外包或是公司。";
			label10.AutoSize = true;
			label10.ForeColor = System.Drawing.Color.FromArgb(192, 64, 0);
			label10.Location = new System.Drawing.Point(32, 473);
			label10.Name = "label10";
			label10.Size = new System.Drawing.Size(505, 17);
			label10.TabIndex = 12;
			label10.Text = "5. 免费使用HslControls控件库，demo参照 https://github.com/dathlin/HslControlsDemo";
			base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 17f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.AliceBlue;
			base.ClientSize = new System.Drawing.Size(710, 542);
			base.Controls.Add(label10);
			base.Controls.Add(label9);
			base.Controls.Add(label8);
			base.Controls.Add(label7);
			base.Controls.Add(label2);
			base.Controls.Add(label6);
			base.Controls.Add(pictureBox2);
			base.Controls.Add(label5);
			base.Controls.Add(label4);
			base.Controls.Add(label3);
			base.Controls.Add(label1);
			base.Controls.Add(pictureBox1);
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			base.Name = "FormSupport";
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			Text = "开发不易，如果您觉得类库好用，并应用到了实际项目，感谢赞助";
			((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
			((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
