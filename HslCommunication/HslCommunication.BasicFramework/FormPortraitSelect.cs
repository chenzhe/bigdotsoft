using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using HslCommunication.Controls;

namespace HslCommunication.BasicFramework
{
	/// <summary>
	/// 一个正方形图形选择窗口，可以获取指定的分辨率
	/// </summary>
	public class FormPortraitSelect : Form
	{
		private Brush brush = new SolidBrush(Color.FromArgb(120, Color.Gray));

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		private PictureBox pictureBox1;

		private PictureBox pictureBox2;

		private UserButton userButton1;

		private UserButton userButton2;

		private Label label1;

		private PictureBox pictureBox3;

		private PictureBox pictureBox4;

		private Label label2;

		private Label label3;

		private Label label4;

		private GroupBox groupBox1;

		private GroupBox groupBox2;

		private GroupBox groupBox3;

		/// <summary>
		/// 是否有图片存在
		/// </summary>
		private bool HasPicture
		{
			get;
			set;
		}

		/// <summary>
		/// 已选择的图形大小
		/// </summary>
		private Rectangle RectangleSelected
		{
			get;
			set;
		}

		private Rectangle RectangleMoved
		{
			get;
			set;
		}

		/// <summary>
		/// 在控件显示的图片的大小，按照比例缩放以后
		/// </summary>
		private Rectangle RectangleImage
		{
			get;
			set;
		}

		private bool IsMouseOver
		{
			get;
			set;
		}

		private bool IsMouseOverOnImage
		{
			get;
			set;
		}

		private bool IsMouseDown
		{
			get;
			set;
		}

		private Point MouseDownPoint
		{
			get;
			set;
		}

		private Point MouseMovePrecives
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public FormPortraitSelect()
		{
			InitializeComponent();
		}

		private void pictureBox1_MouseEnter(object sender, EventArgs e)
		{
			IsMouseOver = true;
			pictureBox1.Refresh();
		}

		private void pictureBox1_MouseLeave(object sender, EventArgs e)
		{
			IsMouseOver = false;
			pictureBox1.Refresh();
		}

		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			if (!IsMouseOver || !HasPicture)
			{
				return;
			}
			if (RectangleSelected.Contains(e.Location))
			{
				Cursor = Cursors.SizeAll;
				if (IsMouseDown)
				{
					MoveByPoint(e.Location);
				}
			}
			else
			{
				Cursor = Cursors.Default;
			}
		}

		private void MoveByPoint(Point point)
		{
			if (RectangleImage.Width >= RectangleImage.Height)
			{
				int num = point.X - MouseMovePrecives.X;
				int num2 = num + RectangleSelected.X;
				if (num2 >= 0 && num2 <= RectangleImage.Width - RectangleSelected.Width)
				{
					RectangleSelected = new Rectangle(num2, RectangleSelected.Y, RectangleSelected.Width, RectangleSelected.Height);
					pictureBox1.Refresh();
				}
			}
			else
			{
				int num3 = point.Y - MouseMovePrecives.Y;
				int num4 = num3 + RectangleSelected.Y;
				if (num4 >= 0 && num4 <= RectangleImage.Height - RectangleSelected.Height)
				{
					RectangleSelected = new Rectangle(RectangleSelected.X, num4, RectangleSelected.Width, RectangleSelected.Height);
					pictureBox1.Refresh();
				}
			}
			MouseMovePrecives = point;
		}

		private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			if (IsMouseOver && HasPicture && RectangleSelected.Contains(e.Location))
			{
				IsMouseDown = true;
				MouseDownPoint = e.Location;
				MouseMovePrecives = e.Location;
			}
			pictureBox1.Focus();
		}

		private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
		{
			if (HasPicture && RectangleSelected.Contains(e.Location))
			{
				SetImgaeMiniShow();
			}
			IsMouseDown = false;
			pictureBox1.Refresh();
		}

		private void SetImgaeMiniShow()
		{
			Rectangle des = RectangleRestore();
			pictureBox2.Image?.Dispose();
			pictureBox2.Image = GetSpecicalSizeFromImage(100, des);
			pictureBox3.Image?.Dispose();
			pictureBox3.Image = GetSpecicalSizeFromImage(64, des);
			pictureBox4.Image?.Dispose();
			pictureBox4.Image = GetSpecicalSizeFromImage(32, des);
		}

		private Rectangle RectangleRestore()
		{
			int width = pictureBox1.Image.Width;
			int height = pictureBox1.Image.Height;
			if (width < height)
			{
				return new Rectangle(0, RectangleSelected.Y * height / 370, width, width);
			}
			return new Rectangle(RectangleSelected.X * width / 370, 0, height, height);
		}

		private Bitmap GetSpecicalSizeFromImage(int size, Rectangle des)
		{
			Bitmap bitmap = new Bitmap(size, size);
			using (Graphics graphics = Graphics.FromImage(bitmap))
			{
				graphics.DrawImage(pictureBox1.Image, new Rectangle(0, 0, size, size), des, GraphicsUnit.Pixel);
			}
			return bitmap;
		}

		private void userButton1_Click(object sender, EventArgs e)
		{
			using OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Multiselect = false;
			openFileDialog.Title = "请选择一张图片";
			openFileDialog.Filter = "图片文件(*.jpg)|*.jpg|图片文件(*.png)|*.png";
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				LoadPictureFile(openFileDialog.FileName);
			}
		}

		/// <summary>
		/// 增加一张图片的路径
		/// </summary>
		/// <param name="picPath"></param>
		private void LoadPictureFile(string picPath)
		{
			Bitmap bitmap = null;
			try
			{
				bitmap = (Bitmap)Image.FromFile(picPath);
				int width = bitmap.Width;
				int height = bitmap.Height;
				if (width > height)
				{
					height = height * 370 / width;
					width = 369;
				}
				else
				{
					width = width * 370 / height;
					height = 369;
				}
				label1.Text = $"({width},{height})";
				RectangleImage = new Rectangle((370 - width) / 2, (370 - height) / 2, width, height);
				if (RectangleImage.Width >= RectangleImage.Height)
				{
					RectangleSelected = new Rectangle(RectangleImage.X, RectangleImage.Y, RectangleImage.Height, RectangleImage.Height);
				}
				else
				{
					RectangleSelected = new Rectangle(RectangleImage.X, RectangleImage.Y, RectangleImage.Width, RectangleImage.Width);
				}
				HasPicture = true;
				pictureBox1.Refresh();
			}
			catch (Exception ex)
			{
				SoftBasic.ShowExceptionMessage(ex);
				return;
			}
			pictureBox1.Image = bitmap;
			SetImgaeMiniShow();
		}

		private void FormPortraitSelect_Load(object sender, EventArgs e)
		{
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
			if (HasPicture)
			{
				Graphics graphics = e.Graphics;
				graphics.FillRectangle(brush, RectangleSelected);
				graphics.DrawRectangle(Pens.LightSkyBlue, RectangleSelected);
			}
		}

		private void userButton2_Click(object sender, EventArgs e)
		{
			if (HasPicture)
			{
				base.DialogResult = DialogResult.OK;
			}
			else
			{
				base.DialogResult = DialogResult.Cancel;
			}
		}

		private void pictureBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
		}

		/// <summary>
		/// 获取指定大小的图片，该图片将会按照比例压缩
		/// </summary>
		/// <param name="size">图片的横向分辨率</param>
		/// <returns>缩放后的图形</returns>
		public Bitmap GetSpecifiedSizeImage(int size)
		{
			if (HasPicture)
			{
				return GetSpecicalSizeFromImage(size, RectangleRestore());
			}
			return null;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HslCommunication.BasicFramework.FormPortraitSelect));
			pictureBox1 = new System.Windows.Forms.PictureBox();
			pictureBox2 = new System.Windows.Forms.PictureBox();
			userButton1 = new HslCommunication.Controls.UserButton();
			userButton2 = new HslCommunication.Controls.UserButton();
			label1 = new System.Windows.Forms.Label();
			pictureBox3 = new System.Windows.Forms.PictureBox();
			pictureBox4 = new System.Windows.Forms.PictureBox();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			groupBox1 = new System.Windows.Forms.GroupBox();
			groupBox2 = new System.Windows.Forms.GroupBox();
			groupBox3 = new System.Windows.Forms.GroupBox();
			((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
			((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
			((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
			((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
			groupBox1.SuspendLayout();
			groupBox2.SuspendLayout();
			groupBox3.SuspendLayout();
			SuspendLayout();
			pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			pictureBox1.Location = new System.Drawing.Point(26, 17);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(372, 372);
			pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			pictureBox1.TabIndex = 0;
			pictureBox1.TabStop = false;
			pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(pictureBox1_Paint);
			pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseDown);
			pictureBox1.MouseEnter += new System.EventHandler(pictureBox1_MouseEnter);
			pictureBox1.MouseLeave += new System.EventHandler(pictureBox1_MouseLeave);
			pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseMove);
			pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseUp);
			pictureBox1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(pictureBox1_PreviewKeyDown);
			pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			pictureBox2.Location = new System.Drawing.Point(18, 33);
			pictureBox2.Name = "pictureBox2";
			pictureBox2.Size = new System.Drawing.Size(102, 102);
			pictureBox2.TabIndex = 1;
			pictureBox2.TabStop = false;
			userButton1.BackColor = System.Drawing.Color.Transparent;
			userButton1.Cursor = System.Windows.Forms.Cursors.Arrow;
			userButton1.CustomerInformation = "";
			userButton1.EnableColor = System.Drawing.Color.FromArgb(190, 190, 190);
			userButton1.Font = new System.Drawing.Font("微软雅黑", 9f);
			userButton1.Location = new System.Drawing.Point(78, 31);
			userButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			userButton1.Name = "userButton1";
			userButton1.Size = new System.Drawing.Size(105, 39);
			userButton1.TabIndex = 3;
			userButton1.UIText = "select";
			userButton1.Click += new System.EventHandler(userButton1_Click);
			userButton2.BackColor = System.Drawing.Color.Transparent;
			userButton2.CustomerInformation = "";
			userButton2.EnableColor = System.Drawing.Color.FromArgb(190, 190, 190);
			userButton2.Font = new System.Drawing.Font("微软雅黑", 9f);
			userButton2.Location = new System.Drawing.Point(78, 34);
			userButton2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			userButton2.Name = "userButton2";
			userButton2.Size = new System.Drawing.Size(105, 39);
			userButton2.TabIndex = 4;
			userButton2.UIText = "sure";
			userButton2.Click += new System.EventHandler(userButton2_Click);
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label1.Location = new System.Drawing.Point(23, 392);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(16, 17);
			label1.TabIndex = 5;
			label1.Text = "()";
			label1.Visible = false;
			pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			pictureBox3.Location = new System.Drawing.Point(134, 33);
			pictureBox3.Name = "pictureBox3";
			pictureBox3.Size = new System.Drawing.Size(66, 66);
			pictureBox3.TabIndex = 6;
			pictureBox3.TabStop = false;
			pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			pictureBox4.Location = new System.Drawing.Point(218, 33);
			pictureBox4.Name = "pictureBox4";
			pictureBox4.Size = new System.Drawing.Size(34, 34);
			pictureBox4.TabIndex = 7;
			pictureBox4.TabStop = false;
			label2.AutoSize = true;
			label2.Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label2.Location = new System.Drawing.Point(16, 138);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(55, 17);
			label2.TabIndex = 8;
			label2.Text = "100*100";
			label3.AutoSize = true;
			label3.Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label3.Location = new System.Drawing.Point(131, 102);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(41, 17);
			label3.TabIndex = 9;
			label3.Text = "64*64";
			label4.AutoSize = true;
			label4.Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label4.Location = new System.Drawing.Point(215, 70);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(41, 17);
			label4.TabIndex = 10;
			label4.Text = "32*32";
			groupBox1.Controls.Add(pictureBox2);
			groupBox1.Controls.Add(label4);
			groupBox1.Controls.Add(pictureBox3);
			groupBox1.Controls.Add(label3);
			groupBox1.Controls.Add(pictureBox4);
			groupBox1.Controls.Add(label2);
			groupBox1.Location = new System.Drawing.Point(414, 12);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(275, 170);
			groupBox1.TabIndex = 11;
			groupBox1.TabStop = false;
			groupBox1.Text = "Multi Resolution";
			groupBox2.Controls.Add(userButton1);
			groupBox2.Location = new System.Drawing.Point(414, 188);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(275, 101);
			groupBox2.TabIndex = 12;
			groupBox2.TabStop = false;
			groupBox2.Text = "File Selected";
			groupBox3.Controls.Add(userButton2);
			groupBox3.Location = new System.Drawing.Point(414, 298);
			groupBox3.Name = "groupBox3";
			groupBox3.Size = new System.Drawing.Size(275, 91);
			groupBox3.TabIndex = 13;
			groupBox3.TabStop = false;
			groupBox3.Text = "Click to Sure";
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(709, 418);
			base.Controls.Add(groupBox3);
			base.Controls.Add(groupBox2);
			base.Controls.Add(groupBox1);
			base.Controls.Add(label1);
			base.Controls.Add(pictureBox1);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.Name = "FormPortraitSelect";
			base.ShowIcon = false;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			Text = "Image Select";
			base.Load += new System.EventHandler(FormPortraitSelect_Load);
			((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
			((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
			((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
			((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
			groupBox1.ResumeLayout(false);
			groupBox1.PerformLayout();
			groupBox2.ResumeLayout(false);
			groupBox3.ResumeLayout(false);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
