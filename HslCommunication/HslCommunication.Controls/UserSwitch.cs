using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个开关按钮类
	/// </summary>
	[DefaultEvent("Click")]
	public class UserSwitch : UserControl
	{
		private Color color_switch_background = Color.DimGray;

		private Brush brush_switch_background = null;

		private Pen pen_switch_background = null;

		private bool switch_status = false;

		private Color color_switch_foreground = Color.FromArgb(36, 36, 36);

		private Brush brush_switch_foreground = null;

		private StringFormat centerFormat = null;

		private string[] description = new string[2]
		{
			"Off",
			"On"
		};

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置开关按钮的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的背景色")]
		[Category("外观")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color SwitchBackground
		{
			get
			{
				return color_switch_background;
			}
			set
			{
				color_switch_background = value;
				brush_switch_background?.Dispose();
				pen_switch_background?.Dispose();
				brush_switch_background = new SolidBrush(color_switch_background);
				pen_switch_background = new Pen(color_switch_background, 2f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置开关按钮的前景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的前景色")]
		[Category("外观")]
		[DefaultValue(typeof(Color), "[36, 36, 36]")]
		public Color SwitchForeground
		{
			get
			{
				return color_switch_foreground;
			}
			set
			{
				color_switch_foreground = value;
				brush_switch_foreground = new SolidBrush(color_switch_foreground);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置开关按钮的开合状态
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的开合状态")]
		[Category("外观")]
		[DefaultValue(false)]
		public bool SwitchStatus
		{
			get
			{
				return switch_status;
			}
			set
			{
				if (value != switch_status)
				{
					switch_status = value;
					Invalidate();
					this.OnSwitchChanged?.Invoke(this, switch_status);
				}
			}
		}

		/// <summary>
		/// 获取或设置两种开关状态的文本描述，例如：new string[]{"Off","On"}
		/// </summary>
		[Browsable(false)]
		public string[] SwitchStatusDescription
		{
			get
			{
				return description;
			}
			set
			{
				if (value != null && value.Length == 2)
				{
					description = value;
				}
			}
		}

		/// <summary>
		/// 开关按钮发生变化的事件
		/// </summary>
		[Category("操作")]
		[Description("点击了按钮开发后触发")]
		public event Action<object, bool> OnSwitchChanged;

		/// <summary>
		/// 实例化一个开关按钮对象
		/// </summary>
		public UserSwitch()
		{
			InitializeComponent();
			DoubleBuffered = true;
			brush_switch_background = new SolidBrush(color_switch_background);
			pen_switch_background = new Pen(color_switch_background, 2f);
			brush_switch_foreground = new SolidBrush(color_switch_foreground);
			centerFormat = new StringFormat();
			centerFormat.Alignment = StringAlignment.Center;
			centerFormat.LineAlignment = StringAlignment.Center;
		}

		private void UserSwitch_Load(object sender, EventArgs e)
		{
		}

		private void UserSwitch_Paint(object sender, PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			Point centerPoint = GetCenterPoint();
			e.Graphics.TranslateTransform(centerPoint.X, centerPoint.Y);
			int num = 45 * (centerPoint.X * 2 - 30) / 130;
			if (num >= 5)
			{
				Rectangle rect = new Rectangle(-num - 4, -num - 4, 2 * num + 8, 2 * num + 8);
				Rectangle rect2 = new Rectangle(-num, -num, 2 * num, 2 * num);
				e.Graphics.DrawEllipse(pen_switch_background, rect);
				e.Graphics.FillEllipse(brush_switch_background, rect2);
				float angle = -36f;
				if (SwitchStatus)
				{
					angle = 36f;
				}
				e.Graphics.RotateTransform(angle);
				int num2 = 20 * (centerPoint.X * 2 - 30) / 130;
				Rectangle rect3 = new Rectangle(-centerPoint.X / 8, -num - num2, centerPoint.X / 4, num * 2 + num2 * 2);
				e.Graphics.FillRectangle(brush_switch_foreground, rect3);
				Rectangle rect4 = new Rectangle(-centerPoint.X / 16, -num - 10, centerPoint.X / 8, centerPoint.X * 3 / 8);
				e.Graphics.FillEllipse(SwitchStatus ? Brushes.LimeGreen : Brushes.Tomato, rect4);
				Rectangle r = new Rectangle(-50, -num - num2 - 15, 100, 15);
				e.Graphics.DrawString(SwitchStatus ? description[1] : description[0], Font, SwitchStatus ? Brushes.LimeGreen : Brushes.Tomato, r, centerFormat);
				e.Graphics.ResetTransform();
			}
		}

		private Point GetCenterPoint()
		{
			if (base.Height > base.Width)
			{
				return new Point(base.Width / 2, base.Width / 2);
			}
			return new Point(base.Height / 2, base.Height / 2);
		}

		private void UserSwitch_Click(object sender, EventArgs e)
		{
			SwitchStatus = !SwitchStatus;
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 17f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			Cursor = System.Windows.Forms.Cursors.Hand;
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			base.Name = "UserSwitch";
			base.Size = new System.Drawing.Size(214, 207);
			base.Load += new System.EventHandler(UserSwitch_Load);
			base.Click += new System.EventHandler(UserSwitch_Click);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserSwitch_Paint);
			ResumeLayout(false);
		}
	}
}
