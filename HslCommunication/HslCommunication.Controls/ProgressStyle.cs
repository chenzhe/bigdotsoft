namespace HslCommunication.Controls
{
	/// <summary>
	/// 进度条的样式
	/// </summary>
	public enum ProgressStyle
	{
		/// <summary>
		/// 竖直的，纵向的进度条
		/// </summary>
		Vertical,
		/// <summary>
		/// 水平进度条
		/// </summary>
		Horizontal
	}
}
