﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 设备
    /// </summary>
    public class Device
    {
        public int id { get; set; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string device_name { get; set; }
        /// <summary>
        /// 通讯协议
        /// </summary>
        public string protocol { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
          public string device_ip { get; set; }
        /// <summary>
        /// 端口号
        /// </summary>
          public int device_port { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
          public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
          public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
          public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
          public string update_user { get; set; }
        /// <summary>
        /// 变量集合
        /// </summary>
        public List<DeviceVar> VarList { get; set; }

        public override string ToString()
        {
            return device_name;// +"("+device_ip+":"+device_port+")";
        }
    }
}
