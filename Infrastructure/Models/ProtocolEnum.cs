﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    public enum ProtocolEnum
    {
        /// <summary>
        /// Modbus 协议
        /// </summary>
        ModbusTCP,
        /// <summary>
        /// 西门子S7
        /// </summary>
        S7
    }
}
