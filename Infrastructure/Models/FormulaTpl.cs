﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 配方模板表
    /// </summary>
    public class FormulaTpl
    {
         public int id { get; set; }
         /// <summary>
         /// 设备编号
         /// </summary>
         public int device_id { get; set; }
        /// <summary>
        /// 模板名称
        /// </summary>
         public string template_name { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string update_user { get; set; }
        /// <summary>
        /// 参数集合
        /// </summary>
        public List<FormulaTplParam> ParamList { get; set; }
        public string device_name { get; set; }
    }
}
