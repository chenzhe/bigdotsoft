﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 设备变量值
    /// </summary>
    public class DeviceVarVal
    {
        public int id { get; set; }
        /// <summary>
        /// 设备编号
        /// </summary>
        public int device_id { get; set; }
        /// <summary>
        /// 变量编号
        /// </summary>
        public int var_id { get; set; }
        /// <summary>
        /// 变量值
        /// </summary>
        public string var_val { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string update_user { get; set; }

        public string device_name { get; set; }
        public string var_name { get; set; }
        public string var_address { get; set; }
    }
}
