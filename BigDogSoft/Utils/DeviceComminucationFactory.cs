﻿using HslCommunication;
using HslCommunication.Core.Net;
using HslCommunication.ModBus;
using HslCommunication.Profinet.Siemens;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigDogSoft.Utils
{
    /// <summary>
    /// 通讯工厂类，负责管理通讯连接
    /// </summary>
    public class DeviceComminucationFactory
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static Dictionary<string, NetworkDeviceBase> ComminucationCache = new Dictionary<string, NetworkDeviceBase>();
        /// <summary>
        /// 获取设备连接
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public static NetworkDeviceBase GetDeviceNetClient(Device device)
        {
           string key= BuildKey(device);
            NetworkDeviceBase netClient = null;
            OperateResult connect;
           if (ComminucationCache.TryGetValue(key,out netClient))
            {
                return netClient;
            }
            else
            {
                try
                {
                    ProtocolEnum penum;
                    Enum.TryParse<ProtocolEnum>(device.protocol, out penum);
                    switch (penum)
                    {
                        case ProtocolEnum.S7:
                            SiemensPLCS siemensPLCSelected = SiemensPLCS.S1200;
                            netClient = new SiemensS7Net(siemensPLCSelected);
                            netClient.IpAddress = device.device_ip.Replace(" ", "");
                            netClient.Port = device.device_port;
                            connect = netClient.ConnectServer();
                            if (!connect.IsSuccess)
                            {
                                netClient = null;
                            }
                            break;
                        case ProtocolEnum.ModbusTCP:

                            netClient = new ModbusTcpNet();
                            netClient.IpAddress = device.device_ip.Replace(" ", "");
                            netClient.Port = device.device_port;
                            connect = netClient.ConnectServer();
                            if (!connect.IsSuccess)
                            {
                                netClient = null;
                            }
                            break;
                    }
                    if (netClient != null)
                    {
                        ComminucationCache.Add(key, netClient);
                    }
                }catch(Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return netClient;
        }
        /// <summary>
        /// 关闭所有连接
        /// </summary>
        public static void CloseAll()
        {
            foreach (var item in ComminucationCache.Values)
            {
                item.ConnectClose();
            }
        }

        private static string BuildKey(Device device)
        {
            return  device.device_ip.Replace(" ", "") + ":" + device.device_port;
        }
    }
}
