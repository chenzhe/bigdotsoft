﻿namespace BigDogSoft.Pages
{
    partial class UtlHistoryQuery
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDevice = new HZH_Controls.Controls.UCCombox();
            this.cmbVarName = new HZH_Controls.Controls.UCCombox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateBegin = new HZH_Controls.Controls.UCDatePickerExt();
            this.dateEnd = new HZH_Controls.Controls.UCDatePickerExt();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new HZH_Controls.Controls.UCBtnExt();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.btnExportExcel = new HZH_Controls.Controls.UCBtnExt();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(31, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "设备名称";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(424, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "变量名称";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbDevice
            // 
            this.cmbDevice.BackColor = System.Drawing.Color.Transparent;
            this.cmbDevice.BackColorExt = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.cmbDevice.BoxStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbDevice.ConerRadius = 5;
            this.cmbDevice.DropPanelHeight = -1;
            this.cmbDevice.FillColor = System.Drawing.Color.White;
            this.cmbDevice.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbDevice.IsRadius = true;
            this.cmbDevice.IsShowRect = true;
            this.cmbDevice.ItemWidth = 70;
            this.cmbDevice.Location = new System.Drawing.Point(122, 22);
            this.cmbDevice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDevice.Name = "cmbDevice";
            this.cmbDevice.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.cmbDevice.RectWidth = 1;
            this.cmbDevice.SelectedIndex = -1;
            this.cmbDevice.SelectedValue = "";
            this.cmbDevice.Size = new System.Drawing.Size(215, 37);
            this.cmbDevice.Source = null;
            this.cmbDevice.TabIndex = 1;
            this.cmbDevice.TextValue = null;
            this.cmbDevice.TriangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.cmbDevice.SelectedChangedEvent += new System.EventHandler(this.cmbDevice_SelectedChangedEvent);
            // 
            // cmbVarName
            // 
            this.cmbVarName.BackColor = System.Drawing.Color.Transparent;
            this.cmbVarName.BackColorExt = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.cmbVarName.BoxStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbVarName.ConerRadius = 5;
            this.cmbVarName.DropPanelHeight = -1;
            this.cmbVarName.FillColor = System.Drawing.Color.White;
            this.cmbVarName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbVarName.IsRadius = true;
            this.cmbVarName.IsShowRect = true;
            this.cmbVarName.ItemWidth = 70;
            this.cmbVarName.Location = new System.Drawing.Point(538, 21);
            this.cmbVarName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbVarName.Name = "cmbVarName";
            this.cmbVarName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.cmbVarName.RectWidth = 1;
            this.cmbVarName.SelectedIndex = -1;
            this.cmbVarName.SelectedValue = "";
            this.cmbVarName.Size = new System.Drawing.Size(217, 37);
            this.cmbVarName.Source = null;
            this.cmbVarName.TabIndex = 1;
            this.cmbVarName.TextValue = null;
            this.cmbVarName.TriangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(43, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "日期";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dateBegin
            // 
            this.dateBegin.BackColor = System.Drawing.Color.White;
            this.dateBegin.ConerRadius = 5;
            this.dateBegin.CurrentTime = new System.DateTime(2020, 12, 27, 10, 2, 24, 0);
            this.dateBegin.FillColor = System.Drawing.Color.Transparent;
            this.dateBegin.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dateBegin.IsRadius = true;
            this.dateBegin.IsShowRect = true;
            this.dateBegin.Location = new System.Drawing.Point(122, 85);
            this.dateBegin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateBegin.Name = "dateBegin";
            this.dateBegin.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.dateBegin.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.dateBegin.RectWidth = 1;
            this.dateBegin.Size = new System.Drawing.Size(235, 39);
            this.dateBegin.TabIndex = 2;
            this.dateBegin.TimeFontSize = 20;
            this.dateBegin.TimeType = HZH_Controls.Controls.DateTimePickerType.DateTime;
            // 
            // dateEnd
            // 
            this.dateEnd.BackColor = System.Drawing.Color.White;
            this.dateEnd.ConerRadius = 5;
            this.dateEnd.CurrentTime = new System.DateTime(2020, 12, 27, 10, 2, 24, 0);
            this.dateEnd.FillColor = System.Drawing.Color.Transparent;
            this.dateEnd.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dateEnd.IsRadius = true;
            this.dateEnd.IsShowRect = true;
            this.dateEnd.Location = new System.Drawing.Point(413, 85);
            this.dateEnd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.dateEnd.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.dateEnd.RectWidth = 1;
            this.dateEnd.Size = new System.Drawing.Size(235, 39);
            this.dateEnd.TabIndex = 2;
            this.dateEnd.TimeFontSize = 20;
            this.dateEnd.TimeType = HZH_Controls.Controls.DateTimePickerType.DateTime;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(369, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "至";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.White;
            this.btnSearch.BtnBackColor = System.Drawing.Color.White;
            this.btnSearch.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSearch.BtnForeColor = System.Drawing.Color.White;
            this.btnSearch.BtnText = "查询";
            this.btnSearch.ConerRadius = 5;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.EnabledMouseEffect = false;
            this.btnSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSearch.IsRadius = true;
            this.btnSearch.IsShowRect = true;
            this.btnSearch.IsShowTips = false;
            this.btnSearch.Location = new System.Drawing.Point(825, 26);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.btnSearch.RectWidth = 1;
            this.btnSearch.Size = new System.Drawing.Size(79, 37);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.TabStop = false;
            this.btnSearch.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnSearch.TipsText = "";
            this.btnSearch.BtnClick += new System.EventHandler(this.btnSearch_BtnClick);
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Location = new System.Drawing.Point(35, 151);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(757, 224);
            this.cartesianChart1.TabIndex = 4;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.BackColor = System.Drawing.Color.White;
            this.btnExportExcel.BtnBackColor = System.Drawing.Color.White;
            this.btnExportExcel.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnExportExcel.BtnForeColor = System.Drawing.Color.White;
            this.btnExportExcel.BtnText = "导出";
            this.btnExportExcel.ConerRadius = 5;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExportExcel.EnabledMouseEffect = false;
            this.btnExportExcel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.btnExportExcel.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnExportExcel.IsRadius = true;
            this.btnExportExcel.IsShowRect = true;
            this.btnExportExcel.IsShowTips = false;
            this.btnExportExcel.Location = new System.Drawing.Point(825, 81);
            this.btnExportExcel.Margin = new System.Windows.Forms.Padding(0);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.btnExportExcel.RectWidth = 1;
            this.btnExportExcel.Size = new System.Drawing.Size(79, 37);
            this.btnExportExcel.TabIndex = 3;
            this.btnExportExcel.TabStop = false;
            this.btnExportExcel.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnExportExcel.TipsText = "";
            this.btnExportExcel.BtnClick += new System.EventHandler(this.btnExportExcel_BtnClick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGridView1.Location = new System.Drawing.Point(35, 402);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 30;
            this.dataGridView1.Size = new System.Drawing.Size(932, 313);
            this.dataGridView1.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "device_name";
            this.Column1.HeaderText = "设备名称";
            this.Column1.MinimumWidth = 8;
            this.Column1.Name = "Column1";
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "var_name";
            this.Column2.HeaderText = "变量名称";
            this.Column2.MinimumWidth = 8;
            this.Column2.Name = "Column2";
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "var_address";
            this.Column3.HeaderText = "变量地址";
            this.Column3.MinimumWidth = 8;
            this.Column3.Name = "Column3";
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "var_val";
            this.Column4.HeaderText = "变量值";
            this.Column4.MinimumWidth = 8;
            this.Column4.Name = "Column4";
            this.Column4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "create_time";
            this.Column5.HeaderText = "创建时间";
            this.Column5.MinimumWidth = 8;
            this.Column5.Name = "Column5";
            this.Column5.Width = 150;
            // 
            // UtlHistoryQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cartesianChart1);
            this.Controls.Add(this.btnExportExcel);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dateEnd);
            this.Controls.Add(this.dateBegin);
            this.Controls.Add(this.cmbVarName);
            this.Controls.Add(this.cmbDevice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "UtlHistoryQuery";
            this.Size = new System.Drawing.Size(1006, 733);
            this.Load += new System.EventHandler(this.UtlHistoryQuery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private HZH_Controls.Controls.UCCombox cmbDevice;
        private HZH_Controls.Controls.UCCombox cmbVarName;
        private System.Windows.Forms.Label label3;
        private HZH_Controls.Controls.UCDatePickerExt dateBegin;
        private HZH_Controls.Controls.UCDatePickerExt dateEnd;
        private System.Windows.Forms.Label label4;
        private HZH_Controls.Controls.UCBtnExt btnSearch;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private HZH_Controls.Controls.UCBtnExt btnExportExcel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}
