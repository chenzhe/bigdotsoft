﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using BigDogSoft.Pages.forms;
using Infrastructure.Models;
using Infrastructure.DAL;

namespace BigDogSoft.Pages
{
    public partial class UtlFormulaMgr : UCControlBase
    {
        public UtlFormulaMgr()
        {
            InitializeComponent();

        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            var tpl = this.listTpl.SelectedItem as FormulaTpl;
            if (tpl != null)
            {
                FrmFormulaAdd frm = new FrmFormulaAdd(tpl);
                frm.SaveSuccessEvent += ReloadData;
                frm.ShowDialog();
                
            }
            else
            {
                MessageBox.Show("请选择配方模板！");
            }
        }

        private void ReloadData(object sender, EventArgs e)
        {
            if(sender is FormulaTpl)
            {
                var tpl = sender as FormulaTpl;
                FillDataToGridView(tpl);
            }
        }

        private void UtlFormulaMgr_Load(object sender, EventArgs e)
        {
            //初始化数据
            List<FormulaTpl> tpls = FormulaDAL.GetFormulaTpls("");
            this.listTpl.DataSource = tpls;
            this.listTpl.DisplayMember = "template_name";
            this.listTpl.ValueMember = "id";

        }

        private void listTpl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tpl= this.listTpl.SelectedItem as FormulaTpl;
            if (tpl != null)
            {
                this.ucDataGridView1.Columns?.Clear();
                //动态创建表格的列
                List<DataGridViewColumnEntity> lstCulumns = new List<DataGridViewColumnEntity>();
                lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "template_name", HeadText = "配方名称", Width = 120, WidthType = SizeType.Absolute });
                //查询模板下的参数列表
                List<FormulaTplParam> list= FormulaDAL.GetFormulaTplParamsBy(tpl.id);
                foreach (var item in list)
                {
                    lstCulumns.Add(new DataGridViewColumnEntity() { DataField = item.param_field,
                        HeadText = item.param_name, Width = 120, WidthType = SizeType.Absolute });
                }
                lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "create_time", HeadText = "创建时间", Width = 200, WidthType = SizeType.Absolute, Format = (a) => { return ((DateTime)a).ToString("yyyy-MM-dd"); } });
                lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "create_user", HeadText = "创建人", Width = 100, WidthType = SizeType.Absolute });

                this.ucDataGridView1.Columns = lstCulumns;
                this.ucDataGridView1.Refresh();
                //查询数据
                FillDataToGridView(tpl);
            }
        }

        private void FillDataToGridView(FormulaTpl tpl)
        {
            List<Formula> list=  FormulaDAL.GetFormulaList(tpl.id);
            this.ucDataGridView1.DataSource = list;
        }
        /// <summary>
        /// 表格双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucDataGridView1_DoubleClick(object sender, EventArgs e)
        {
           
        }
        /// <summary>
        /// 编辑事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_BtnClick(object sender, EventArgs e)
        {
            var row =
            this.ucDataGridView1.SelectRow.DataSource as Formula;

            if (row != null)
            {
                var tpl = this.listTpl.SelectedItem as FormulaTpl;
                FrmFormulaAdd frm = new FrmFormulaAdd(tpl,row);
                frm.SaveSuccessEvent += ReloadData;
                frm.ShowDialog();

            }
        }
    }
}
