﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infrastructure.DAL;
using Infrastructure.Models;
using Infrastructure.Util;

namespace BigDogSoft.Pages.forms
{
    public partial class FrmUserAdd : Form
    {
        private UserDAL dal = new UserDAL();
        public FrmUserAdd()
        {
            InitializeComponent();
        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            //保存
            Users user = new Users();
            user.name = this.txtName.Text.Trim();
            user.account = this.txtAccount.Text.Trim();
            user.password = this.txtPassword.Text.Trim();
            user.create_time = DateTime.Now;

            if (string.IsNullOrEmpty(user.name)||
                string.IsNullOrEmpty(user.password)
                ||string.IsNullOrEmpty(user.account))
            {
                MessageBox.Show("用户名或账号或密码不能为空！");
                return;
            }
            //判断下账号是否存在
            Users existUser= dal.GetByAccount(user.account);
            if (existUser != null)
            {
                MessageBox.Show("登录账号已存在！");
                return;
            }
            user.password= MD5Helper.GetMD5(user.password);
            int row=dal.Insert(user);
            if (row <= 0)
            {
                MessageBox.Show("保存失败！");
            }
            this.Close();
        }
    }
}
