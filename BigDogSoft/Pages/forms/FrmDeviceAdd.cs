﻿using Infrastructure.DAL;
using Infrastructure.Models;
using Infrastructure.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BigDogSoft.Pages.forms
{
    public partial class FrmDeviceAdd : Form
    {
       
        private BindingList<DeviceVar> dataList = new BindingList<DeviceVar>();
        private Device editDevice = null;
        public FrmDeviceAdd()
        {
            InitializeComponent();
          
            //初始化数据
            InitData();
        }

        public FrmDeviceAdd(Device obj):this()
        {
            editDevice = obj;
            this.txtDeviceName.Text = obj.device_name;
            this.txtIP.Text = obj.device_ip;
            this.cmbProtocol.SelectedItem = obj.protocol;
            this.txtPort.Text = obj.device_port.ToString();
            obj.VarList = DeviceVarDAL.GetDeviceVarListByDeviceId(obj.id);
            foreach (var item in obj.VarList)
            {
                dataList.Add(item);
            }
            this.dataGridView1.Refresh();
            this.Text.Replace("新增", "编辑");
        }

        private void InitData()
        {
            string[] names=   Enum.GetNames(typeof(ProtocolEnum));
            this.cmbProtocol.DataSource = names;
            string[] varTypes = Enum.GetNames(typeof(VarType));
            DataGridViewColumn col= this.dataGridView1.Columns["ColumnVarType"];
            DataGridViewComboBoxColumn cmbCol= col as DataGridViewComboBoxColumn;
            if (cmbCol != null)
            {
                cmbCol.DataSource = varTypes;
            }
            this.dataGridView1.AutoGenerateColumns = false;

            this.dataGridView1.DataSource = dataList;
            
        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 保存事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            Device device = null; 
            if (editDevice == null)
            {
                device= new Device(); 
            }
            else
            {
                //编辑
                device = editDevice;
            }
            device.device_name = this.txtDeviceName.Text.Trim();
            device.device_ip = this.txtIP.Text.Trim();
            int port = 0;
            if (int.TryParse(this.txtPort.Text.Trim(), out port))
            {
                device.device_port = port;
            }
            device.protocol = this.cmbProtocol.SelectedItem.ToString();
            device.create_time = DateTime.Now;
            device.update_time = DateTime.Now;
            device.create_user = SessionContext.CurrentUser.account;
            device.update_user = SessionContext.CurrentUser.account;
            device.VarList = new List<DeviceVar>();
            dataList.ToList().ForEach(p =>
            {
                if (!string.IsNullOrEmpty(p.var_name))
                {
                    p.device_id = device.id;
                    p.create_time = device.create_time;
                    p.create_user = device.create_user;
                    p.update_time = device.update_time;
                    p.update_user = device.update_user;
                    device.VarList.Add(p);
                }
            });
            string error = string.Empty;
            if (string.IsNullOrEmpty(device.device_name))
            {
                error = "设备名称不能为空！";
            }
            else if (device.device_port == 0)
            {
                error = "请设置设备端口号！";
            }
            else if (!InputValidateHelper.IsIP(device.device_ip.Replace(" ","")))
            {
                error = "请输入正确的IP地址格式！";
            }
            
            if (error.Length > 0)
            {
                MessageBox.Show(error);
                return;
            }
            //保存到数据库
             int rows = DeviceVarDAL.SaveDevice(device);
             if (rows>0)
             {
                 this.Close();
             }

        }
    }
}
