namespace HslControls.HMI
{
	public enum DAS_LCDDotStyle
	{
		LCDDS_Circle,
		LCDDS_Squre,
		LCDDS_Diamond,
		LCDMS_LeftTriangle,
		LCDMS_RightTriangle,
		LCDMS_TopTriangle,
		LCDMS_BottomTriangle
	}
}
