namespace HslControls.HMI
{
	public enum DAS_BorderGradientStyle
	{
		BGS_None,
		BGS_Flat,
		BGS_Ring,
		BGS_Linear,
		BGS_Linear2,
		BGS_Path
	}
}
