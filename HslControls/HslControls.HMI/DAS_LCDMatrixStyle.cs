namespace HslControls.HMI
{
	public enum DAS_LCDMatrixStyle
	{
		LCDMS_5x7,
		LCDMS_5x8,
		LCDMS_6x9,
		LCDMS_7x8
	}
}
