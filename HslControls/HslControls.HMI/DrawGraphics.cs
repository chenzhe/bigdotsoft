using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HslControls.HMI
{
	public class DrawGraphics
	{
		public void SetRect(Graphics g, Rectangle rec_width, int m_OuterBorderLength, Color m_OuterBorderDarkColor, Color m_OuterBorderLightColor, int m_MiddleBorderLength, Color m_MiddleBorderColor, int m_InnerBorderLength, Color m_InnerBorderDarkColor, Color m_InnerBorderLightColor, int m_BorderExteriorLength, Color m_BorderExteriorColor, DAS_BorderGradientStyle m_BorderGradientType, int m_BorderGradientAngle, float m_BorderGradientRate, float m_BorderGradientLightPos1, float m_BorderGradientLightPos2, float m_BorderLightIntermediateBrightness)
		{
			if ((m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength) * 2 >= rec_width.Width || (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength) * 2 >= rec_width.Height)
			{
				return;
			}
			Rectangle rectangle = new Rectangle(rec_width.Left + m_BorderExteriorLength, rec_width.Top + m_BorderExteriorLength, rec_width.Width - 2 * m_BorderExteriorLength, rec_width.Height - 2 * m_BorderExteriorLength);
			Rectangle rectangle2 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength));
			Rectangle rectangle3 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength));
			Rectangle rectangle4 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength));
			switch (m_BorderGradientType)
			{
			case DAS_BorderGradientStyle.BGS_Flat:
			{
				PointF[] left = new PointF[5];
				left[0].X = rectangle.Left;
				left[0].Y = rectangle.Top;
				left[1].X = rectangle.Left;
				left[1].Y = rectangle.Top + rectangle.Height;
				if (rectangle.Width < rectangle.Height)
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[2].Y = (float)(rectangle.Top + rectangle.Height) - (float)rectangle.Width / 2f;
					left[3].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Width / 2f;
				}
				else
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Height / 2f;
					left[2].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					left[3].X = (float)(rectangle.Left + rectangle.Width) - (float)rectangle.Height / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
				}
				left[4].X = rectangle.Left + rectangle.Width;
				left[4].Y = rectangle.Top;
				GraphicsPath graphicsPath = new GraphicsPath();
				graphicsPath.AddPolygon(left);
				if (m_OuterBorderLength > 0)
				{
					Region region2 = new Region(rectangle);
					region2.Exclude(rectangle2);
					SolidBrush solidBrush2 = new SolidBrush(m_OuterBorderLightColor);
					Region region10 = region2.Clone();
					region10.Intersect(graphicsPath);
					g.FillRegion(solidBrush2, region10);
					solidBrush2.Dispose();
					region10.Dispose();
					SolidBrush solidBrush3 = new SolidBrush(m_OuterBorderDarkColor);
					Region region11 = region2.Clone();
					region11.Exclude(graphicsPath);
					g.FillRegion(solidBrush3, region11);
					solidBrush3.Dispose();
					region11.Dispose();
					region2.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region12 = new Region(rectangle2);
					region12.Exclude(rectangle3);
					SolidBrush solidBrush4 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush4, region12);
					Pen pen2 = new Pen(solidBrush4);
					g.DrawRectangle(pen2, rectangle2);
					g.DrawRectangle(pen2, rectangle3);
					pen2.Dispose();
					solidBrush4.Dispose();
					region12.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					Region region13 = new Region(rectangle3);
					region13.Exclude(rectangle4);
					SolidBrush solidBrush5 = new SolidBrush(m_InnerBorderDarkColor);
					Region region14 = region13.Clone();
					region14.Intersect(graphicsPath);
					g.FillRegion(solidBrush5, region14);
					solidBrush5.Dispose();
					region14.Dispose();
					SolidBrush solidBrush6 = new SolidBrush(m_InnerBorderLightColor);
					Region region15 = region13.Clone();
					region15.Exclude(graphicsPath);
					g.FillRegion(solidBrush6, region15);
					solidBrush6.Dispose();
					region15.Dispose();
					region13.Dispose();
				}
				graphicsPath.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Path:
			{
				Blend blend = new Blend();
				bool flag = false;
				if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag = true;
					blend.Factors = new float[3];
					blend.Positions = new float[3];
					blend.Factors[0] = 0f;
					blend.Positions[0] = 0f;
					blend.Factors[1] = m_BorderGradientRate;
					blend.Positions[1] = 1f - m_BorderGradientRate;
					blend.Factors[2] = 1f;
					blend.Positions[2] = 1f;
				}
				if (m_OuterBorderLength > 0)
				{
					double int4 = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0;
					GraphicsPath graphicsPath2 = new GraphicsPath();
					Point[] point = new Point[4]
					{
						new Point(rectangle.Left - 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top + rectangle.Height + 1),
						new Point(rectangle.Left - 1, rectangle.Top + rectangle.Height + 1)
					};
					graphicsPath2.AddPolygon(point);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath2);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					PathGradientBrush pathGradientBrush = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					pointF3.Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					PointF x = pointF3;
					float height;
					float single;
					if (Math.Abs(Math.Sin(int4)) < 1E-10)
					{
						single = 0f;
						height = (float)(Math.Cos(int4) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
					}
					else if (Math.Abs(Math.Cos(int4)) >= 1E-10)
					{
						height = (float)((double)(rectangle.Height - m_OuterBorderLength) / (2.0 * Math.Tan(int4)));
						single = (float)(Math.Tan(int4) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
						height = ((Math.Cos(int4) >= 0.0) ? Math.Abs(height) : (0f - Math.Abs(height)));
						single = ((Math.Sin(int4) >= 0.0) ? Math.Abs(single) : (0f - Math.Abs(single)));
						if (height < 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						else if (height > (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						if (single < 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
						else if (single > (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
					}
					else
					{
						height = 0f;
						single = (float)(Math.Sin(int4) * (double)(rectangle.Height - m_OuterBorderLength) / 2.0);
					}
					x.X += height;
					x.Y += single;
					if (flag)
					{
						pathGradientBrush.Blend = blend;
					}
					pathGradientBrush.CenterPoint = x;
					pathGradientBrush.CenterColor = m_OuterBorderLightColor;
					Region region16 = new Region(rectangle);
					region16.Exclude(rectangle2);
					g.FillRegion(pathGradientBrush, region16);
					pathGradientBrush.Dispose();
					region16.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region17 = new Region(rectangle2);
					region17.Exclude(rectangle3);
					SolidBrush solidBrush7 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush7, region17);
					Pen pen3 = new Pen(solidBrush7);
					g.DrawRectangle(pen3, rectangle2);
					g.DrawRectangle(pen3, rectangle3);
					pen3.Dispose();
					solidBrush7.Dispose();
					region17.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					double num = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0 + 3.14159265358979;
					GraphicsPath graphicsPath3 = new GraphicsPath();
					Point[] point = new Point[4]
					{
						new Point(rectangle3.Left, rectangle3.Top),
						new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top),
						new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top + rectangle3.Height),
						new Point(rectangle3.Left, rectangle3.Top + rectangle3.Height)
					};
					graphicsPath3.AddPolygon(point);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath3);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_InnerBorderDarkColor
					};
					PathGradientBrush color4 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle3.Left + (float)rectangle3.Width / 2f;
					pointF3.Y = (float)rectangle3.Top + (float)rectangle3.Height / 2f;
					PointF y = pointF3;
					float width;
					float height2;
					if (Math.Abs(Math.Sin(num)) < 1E-10)
					{
						height2 = 0f;
						width = (float)(Math.Cos(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
					}
					else if (Math.Abs(Math.Cos(num)) >= 1E-10)
					{
						width = (float)((double)(rectangle3.Height - m_InnerBorderLength) / (2.0 * Math.Tan(num)));
						height2 = (float)(Math.Tan(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
						width = ((Math.Cos(num) >= 0.0) ? Math.Abs(width) : (0f - Math.Abs(width)));
						height2 = ((Math.Sin(num) >= 0.0) ? Math.Abs(height2) : (0f - Math.Abs(height2)));
						if (width < 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
						{
							width = 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
						}
						else if (width > (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
						{
							width = (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
						}
						if (height2 < 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
						{
							height2 = 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
						}
						else if (height2 > (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
						{
							height2 = (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
						}
					}
					else
					{
						width = 0f;
						height2 = (float)(Math.Sin(num) * (double)(rectangle3.Height - m_InnerBorderLength) / 2.0);
					}
					y.X += width;
					y.Y += height2;
					if (flag)
					{
						color4.Blend = blend;
					}
					color4.CenterPoint = y;
					color4.CenterColor = m_InnerBorderLightColor;
					Region region3 = new Region(rectangle3);
					region3.Exclude(rectangle4);
					g.FillRegion(color4, region3);
					color4.Dispose();
					region3.Dispose();
					if (m_BorderExteriorLength > 0)
					{
						SolidBrush solidBrush = new SolidBrush(m_BorderExteriorColor);
						Region region = new Region(rec_width);
						region.Exclude(rectangle);
						g.FillRegion(solidBrush, region);
						Pen pen = new Pen(solidBrush, m_BorderExteriorLength);
						g.DrawRectangle(pen, rec_width);
						g.DrawRectangle(pen, rectangle);
						pen.Dispose();
						solidBrush.Dispose();
						region.Dispose();
					}
				}
				else if (m_BorderExteriorLength > 0)
				{
					SolidBrush solidBrush = new SolidBrush(m_BorderExteriorColor);
					Region region = new Region(rec_width);
					region.Exclude(rectangle);
					g.FillRegion(solidBrush, region);
					Pen pen = new Pen(solidBrush, m_BorderExteriorLength);
					g.DrawRectangle(pen, rec_width);
					g.DrawRectangle(pen, rectangle);
					pen.Dispose();
					solidBrush.Dispose();
					region.Dispose();
				}
				return;
			}
			default:
				if (m_BorderExteriorLength > 0)
				{
					SolidBrush solidBrush = new SolidBrush(m_BorderExteriorColor);
					Region region = new Region(rec_width);
					region.Exclude(rectangle);
					g.FillRegion(solidBrush, region);
					Pen pen = new Pen(solidBrush, m_BorderExteriorLength);
					g.DrawRectangle(pen, rec_width);
					g.DrawRectangle(pen, rectangle);
					pen.Dispose();
					solidBrush.Dispose();
					region.Dispose();
				}
				return;
			case DAS_BorderGradientStyle.BGS_Linear:
			case DAS_BorderGradientStyle.BGS_Linear2:
			{
				Blend float0 = new Blend();
				bool flag2 = false;
				float @float = 0f;
				if (m_BorderLightIntermediateBrightness >= 0f && (double)m_BorderLightIntermediateBrightness <= 1.0)
				{
					@float = m_BorderLightIntermediateBrightness;
				}
				if (m_BorderGradientLightPos1 > 0f && (double)m_BorderGradientLightPos1 < 1.0)
				{
					flag2 = true;
					if (m_BorderGradientLightPos2 <= 0f || (double)m_BorderGradientLightPos2 > 1.0)
					{
						float0.Factors = new float[5];
						float0.Positions = new float[5];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = m_BorderGradientRate;
						float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
						float0.Factors[2] = 1f;
						float0.Positions[2] = m_BorderGradientLightPos1;
						float0.Factors[3] = m_BorderGradientRate;
						float0.Positions[3] = (1f - m_BorderGradientLightPos1) * m_BorderGradientRate + m_BorderGradientLightPos1;
						float0.Factors[4] = 0f;
						float0.Positions[4] = 1f;
					}
					else
					{
						float0.Factors = new float[9];
						float0.Positions = new float[9];
						if (m_BorderGradientLightPos1 <= m_BorderGradientLightPos2)
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos1;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos1 + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos2;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos2 + (1f - m_BorderGradientLightPos2) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
							if (m_BorderGradientLightPos1 == m_BorderGradientLightPos2)
							{
								float0.Factors[3] = 1f;
								float0.Factors[4] = 1f;
								float0.Factors[5] = 1f;
							}
							if (m_BorderGradientLightPos2 == 1f)
							{
								float0.Factors[7] = 1f;
								float0.Factors[8] = 1f;
							}
						}
						else
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos2 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos2;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos2 + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos1;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos1 + (1f - m_BorderGradientLightPos1) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
						}
					}
				}
				else if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag2 = true;
					float0.Factors = new float[3];
					float0.Positions = new float[3];
					float0.Factors[0] = 0f;
					float0.Positions[0] = 0f;
					float0.Factors[1] = m_BorderGradientRate;
					float0.Positions[1] = 1f - m_BorderGradientRate;
					float0.Factors[2] = 1f;
					float0.Positions[2] = 1f;
				}
				if (m_OuterBorderLength > 0)
				{
					Rectangle rectangle5 = new Rectangle(rectangle.Left - 1, rectangle.Top - 1, rectangle.Width + 2, rectangle.Height + 2);
					LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(rectangle5, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush2.Blend = float0;
					}
					Region region4 = new Region(rectangle);
					region4.Exclude(rectangle2);
					g.FillRegion(linearGradientBrush2, region4);
					linearGradientBrush2.Dispose();
					region4.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region5 = new Region(rectangle2);
					region5.Exclude(rectangle3);
					SolidBrush solidBrush8 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush8, region5);
					Pen pen4 = new Pen(solidBrush8);
					g.DrawRectangle(pen4, rectangle2);
					g.DrawRectangle(pen4, rectangle3);
					pen4.Dispose();
					solidBrush8.Dispose();
					region5.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					LinearGradientBrush linearGradientBrush = (m_BorderGradientType != DAS_BorderGradientStyle.BGS_Linear) ? new LinearGradientBrush(rectangle3, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderGradientAngle + 180) : new LinearGradientBrush(rectangle3, m_InnerBorderLightColor, m_InnerBorderDarkColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush.Blend = float0;
					}
					Region region6 = new Region(rectangle3);
					region6.Exclude(rectangle4);
					g.FillRegion(linearGradientBrush, region6);
					linearGradientBrush.Dispose();
					region6.Dispose();
				}
				break;
			}
			case DAS_BorderGradientStyle.BGS_Ring:
			{
				Blend blend3 = new Blend();
				blend3.Factors = new float[3];
				blend3.Positions = new float[3];
				Blend blend2 = blend3;
				blend2.Factors[0] = 0f;
				blend2.Positions[0] = 0f;
				blend2.Factors[2] = 1f;
				blend2.Positions[2] = 1f;
				if (m_OuterBorderLength > 0)
				{
					blend2.Factors[1] = m_BorderGradientRate;
					blend2.Positions[1] = 1f - m_BorderGradientRate;
					GraphicsPath graphicsPath4 = new GraphicsPath();
					Point[] point = new Point[4]
					{
						new Point(rectangle.Left, rectangle.Top),
						new Point(rectangle.Left + rectangle.Width, rectangle.Top),
						new Point(rectangle.Left + rectangle.Width, rectangle.Top + rectangle.Height),
						new Point(rectangle.Left, rectangle.Top + rectangle.Height)
					};
					graphicsPath4.AddPolygon(point);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath4);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					pathGradientBrush4.CenterColor = m_OuterBorderLightColor;
					PathGradientBrush pathGradientBrush2 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle2.Width / (float)rectangle.Width;
					pointF3.Y = (float)rectangle2.Height / (float)rectangle.Height;
					PointF pointF = pathGradientBrush2.FocusScales = pointF3;
					Region region7 = new Region(rectangle);
					region7.Exclude(rectangle2);
					g.FillRegion(pathGradientBrush2, region7);
					pathGradientBrush2.Dispose();
					region7.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region8 = new Region(rectangle2);
					region8.Exclude(rectangle3);
					SolidBrush solidBrush9 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush9, region8);
					Pen pen5 = new Pen(solidBrush9);
					g.DrawRectangle(pen5, rectangle2);
					g.DrawRectangle(pen5, rectangle3);
					pen5.Dispose();
					solidBrush9.Dispose();
					region8.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					blend2.Factors[1] = 1f - m_BorderGradientRate;
					blend2.Positions[1] = m_BorderGradientRate;
					GraphicsPath graphicsPath5 = new GraphicsPath();
					Point[] point = new Point[4]
					{
						new Point(rectangle3.Left, rectangle3.Top),
						new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top),
						new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top + rectangle3.Height),
						new Point(rectangle3.Left, rectangle3.Top + rectangle3.Height)
					};
					graphicsPath5.AddPolygon(point);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath5);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_InnerBorderLightColor
					};
					pathGradientBrush4.Blend = blend2;
					pathGradientBrush4.CenterColor = m_InnerBorderDarkColor;
					PathGradientBrush pathGradientBrush3 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle4.Width / (float)rectangle3.Width;
					pointF3.Y = (float)rectangle4.Height / (float)rectangle3.Height;
					PointF pointF2 = pathGradientBrush3.FocusScales = pointF3;
					Region region9 = new Region(rectangle3);
					region9.Exclude(rectangle4);
					g.FillRegion(pathGradientBrush3, region9);
					pathGradientBrush3.Dispose();
					region9.Dispose();
				}
				break;
			}
			}
			if (m_BorderExteriorLength > 0)
			{
				SolidBrush solidBrush = new SolidBrush(m_BorderExteriorColor);
				Region region = new Region(rec_width);
				region.Exclude(rectangle);
				g.FillRegion(solidBrush, region);
				Pen pen = new Pen(solidBrush, m_BorderExteriorLength);
				g.DrawRectangle(pen, rec_width);
				g.DrawRectangle(pen, rectangle);
				pen.Dispose();
				solidBrush.Dispose();
				region.Dispose();
			}
		}

		public void SetCircle(Graphics graphics_0, Rectangle rec, int int_0, Color color_0, Color color_1, int int_1, Color color_2, int int_2, Color color_3, Color color_4, int int_3, Color color_5, DAS_BorderGradientStyle das_BorderGradientStyle_0, long long_0, float float_0, float float_1, float float_2, float float_3)
		{
			Rectangle rectangle = new Rectangle(rec.Left + int_3, rec.Top + int_3, rec.Width - 2 * int_3, rec.Height - 2 * int_3);
			Rectangle rectangle2 = new Rectangle(rec.Left + int_3 + int_0, rec.Top + int_3 + int_0, rec.Width - 2 * (int_3 + int_0), rec.Height - 2 * (int_3 + int_0));
			Rectangle rectangle3 = new Rectangle(rec.Left + int_3 + int_0 + int_1, rec.Top + int_3 + int_0 + int_1, rec.Width - 2 * (int_3 + int_0 + int_1), rec.Height - 2 * (int_3 + int_0 + int_1));
			Rectangle rectangle4 = new Rectangle(rec.Left + int_3 + int_0 + int_1 + int_2, rec.Top + int_3 + int_0 + int_1 + int_2, rec.Width - 2 * (int_3 + int_0 + int_1 + int_2), rec.Height - 2 * (int_3 + int_0 + int_1 + int_2));
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddEllipse(rectangle);
			GraphicsPath graphicsPath2 = new GraphicsPath();
			graphicsPath2.AddEllipse(rectangle2);
			GraphicsPath graphicsPath3 = new GraphicsPath();
			graphicsPath3.AddEllipse(rectangle3);
			GraphicsPath graphicsPath4 = new GraphicsPath();
			graphicsPath4.AddEllipse(rectangle4);
			switch (das_BorderGradientStyle_0)
			{
			case DAS_BorderGradientStyle.BGS_Flat:
			{
				PointF[] left = new PointF[5];
				left[0].X = rectangle.Left;
				left[0].Y = rectangle.Top;
				left[1].X = rectangle.Left;
				left[1].Y = rectangle.Top + rectangle.Height;
				if (rectangle.Width < rectangle.Height)
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[2].Y = (float)(rectangle.Top + rectangle.Height) - (float)rectangle.Width / 2f;
					left[3].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Width / 2f;
				}
				else
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Height / 2f;
					left[2].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					left[3].X = (float)(rectangle.Left + rectangle.Width) - (float)rectangle.Height / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
				}
				left[4].X = rectangle.Left + rectangle.Width;
				left[4].Y = rectangle.Top;
				GraphicsPath graphicsPath5 = new GraphicsPath();
				graphicsPath5.AddPolygon(left);
				if (int_0 > 0)
				{
					Region region = new Region(graphicsPath);
					region.Exclude(graphicsPath2);
					SolidBrush solidBrush = new SolidBrush(color_1);
					Region region2 = region.Clone();
					region2.Intersect(graphicsPath5);
					graphics_0.FillRegion(solidBrush, region2);
					graphics_0.SetClip(graphicsPath5);
					Pen pen = new Pen(solidBrush);
					graphics_0.DrawEllipse(pen, rectangle);
					graphics_0.DrawEllipse(pen, rectangle3);
					pen.Dispose();
					graphics_0.ResetClip();
					solidBrush.Dispose();
					region2.Dispose();
					SolidBrush solidBrush2 = new SolidBrush(color_0);
					Region region13 = region.Clone();
					region13.Exclude(graphicsPath5);
					graphics_0.FillRegion(solidBrush2, region13);
					graphics_0.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen2 = new Pen(solidBrush2);
					graphics_0.DrawEllipse(pen2, rectangle);
					graphics_0.DrawEllipse(pen2, rectangle3);
					pen2.Dispose();
					graphics_0.ResetClip();
					solidBrush2.Dispose();
					region13.Dispose();
					region.Dispose();
				}
				if (int_1 > 0)
				{
					Region region17 = new Region(graphicsPath2);
					region17.Exclude(graphicsPath3);
					SolidBrush solidBrush3 = new SolidBrush(color_2);
					graphics_0.FillRegion(solidBrush3, region17);
					Pen pen8 = new Pen(solidBrush3);
					graphics_0.DrawPath(pen8, graphicsPath2);
					graphics_0.DrawPath(pen8, graphicsPath3);
					pen8.Dispose();
					solidBrush3.Dispose();
					region17.Dispose();
				}
				if (int_2 > 0)
				{
					Region region18 = new Region(graphicsPath3);
					region18.Exclude(graphicsPath4);
					SolidBrush solidBrush4 = new SolidBrush(color_3);
					Region region19 = region18.Clone();
					region19.Intersect(graphicsPath5);
					graphics_0.FillRegion(solidBrush4, region19);
					graphics_0.SetClip(graphicsPath5);
					Pen pen9 = new Pen(solidBrush4);
					graphics_0.DrawEllipse(pen9, rectangle3);
					graphics_0.DrawEllipse(pen9, rectangle4);
					pen9.Dispose();
					graphics_0.ResetClip();
					solidBrush4.Dispose();
					region19.Dispose();
					SolidBrush solidBrush5 = new SolidBrush(color_4);
					Region region20 = region18.Clone();
					region20.Exclude(graphicsPath5);
					graphics_0.FillRegion(solidBrush5, region20);
					graphics_0.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen10 = new Pen(solidBrush5);
					graphics_0.DrawEllipse(pen10, rectangle3);
					graphics_0.DrawEllipse(pen10, rectangle4);
					pen10.Dispose();
					graphics_0.ResetClip();
					solidBrush5.Dispose();
					region20.Dispose();
					region18.Dispose();
				}
				graphicsPath5.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Path:
			{
				Blend blend = new Blend();
				bool flag = false;
				if ((double)float_0 < 0.49999999999 || (double)float_0 > 0.5000000000001)
				{
					flag = true;
					blend.Factors = new float[3];
					blend.Positions = new float[3];
					blend.Factors[0] = 0f;
					blend.Positions[0] = 0f;
					blend.Factors[1] = float_0;
					blend.Positions[1] = 1f - float_0;
					blend.Factors[2] = 1f;
					blend.Positions[2] = 1f;
				}
				if (int_0 > 0)
				{
					double long0 = 3.14159265358979 * (double)long_0 / 180.0;
					GraphicsPath graphicsPath6 = new GraphicsPath();
					graphicsPath6.AddEllipse(rectangle);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath6);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						color_0
					};
					PathGradientBrush pathGradientBrush = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					pointF3.Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					PointF x = pointF3;
					float width = (float)((double)((float)rectangle.Width - (float)int_0 * float_1) * Math.Cos(long0) / 2.0);
					float height = (float)((double)((float)rectangle.Height - (float)int_0 * float_1) * Math.Sin(long0) / 2.0);
					x.X += width;
					x.Y += height;
					if (flag)
					{
						pathGradientBrush.Blend = blend;
					}
					pathGradientBrush.CenterPoint = x;
					pathGradientBrush.CenterColor = color_1;
					Region region21 = new Region(graphicsPath);
					Region region22 = new Region(graphicsPath2);
					region21.Exclude(region22);
					graphics_0.FillRegion(pathGradientBrush, region21);
					Pen pen11 = new Pen(color_0);
					graphics_0.DrawPath(pen11, graphicsPath);
					pen11.Dispose();
					pathGradientBrush.Dispose();
					region21.Dispose();
					region22.Dispose();
				}
				if (int_1 > 0)
				{
					Region region23 = new Region(graphicsPath2);
					region23.Exclude(graphicsPath3);
					SolidBrush solidBrush6 = new SolidBrush(color_2);
					graphics_0.FillRegion(solidBrush6, region23);
					Pen pen12 = new Pen(solidBrush6);
					graphics_0.DrawPath(pen12, graphicsPath2);
					graphics_0.DrawPath(pen12, graphicsPath3);
					pen12.Dispose();
					solidBrush6.Dispose();
					region23.Dispose();
				}
				if (int_2 > 0)
				{
					double num = 3.14159265358979 * (double)long_0 / 180.0 + 3.14159265358979;
					GraphicsPath graphicsPath7 = new GraphicsPath();
					graphicsPath7.AddEllipse(rectangle3);
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath7);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						color_3
					};
					PathGradientBrush color4 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle3.Left + (float)rectangle3.Width / 2f;
					pointF3.Y = (float)rectangle3.Top + (float)rectangle3.Height / 2f;
					PointF y = pointF3;
					float single = (float)((double)((float)rectangle3.Width - (float)int_2 * (1f - float_1)) * Math.Cos(num) / 2.0);
					float height2 = (float)((double)((float)rectangle3.Height - (float)int_2 * (1f - float_1)) * Math.Sin(num) / 2.0);
					y.X += single;
					y.Y += height2;
					if (flag)
					{
						color4.Blend = blend;
					}
					color4.CenterPoint = y;
					color4.CenterColor = color_4;
					Region region3 = new Region(graphicsPath3);
					Region region4 = new Region(graphicsPath4);
					region3.Exclude(region4);
					graphics_0.FillRegion(color4, region3);
					Pen pen13 = new Pen(color_3);
					graphics_0.DrawPath(pen13, graphicsPath3);
					graphics_0.DrawPath(pen13, graphicsPath4);
					pen13.Dispose();
					color4.Dispose();
					region3.Dispose();
					region4.Dispose();
				}
				break;
			}
			case DAS_BorderGradientStyle.BGS_Linear:
			case DAS_BorderGradientStyle.BGS_Linear2:
			{
				Blend float0 = new Blend();
				bool flag2 = false;
				float @float = 0f;
				if (float_3 >= 0f && (double)float_3 <= 1.0)
				{
					@float = float_3;
				}
				if (float_1 > 0f && (double)float_1 < 1.0)
				{
					flag2 = true;
					if (float_2 <= 0f || (double)float_2 > 1.0)
					{
						float0.Factors = new float[5];
						float0.Positions = new float[5];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = float_0;
						float0.Positions[1] = float_1 * (1f - float_0);
						float0.Factors[2] = 1f;
						float0.Positions[2] = float_1;
						float0.Factors[3] = float_0;
						float0.Positions[3] = (1f - float_1) * float_0 + float_1;
						float0.Factors[4] = 0f;
						float0.Positions[4] = 1f;
					}
					else
					{
						float0.Factors = new float[9];
						float0.Positions = new float[9];
						if (float_1 <= float_2)
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = float_0;
							float0.Positions[1] = float_1 * (1f - float_0);
							float0.Factors[2] = 1f;
							float0.Positions[2] = float_1;
							float0.Factors[3] = (1f - @float) * float_0 + @float;
							float0.Positions[3] = float_1 + (float_2 - float_1) / 2f * float_0;
							float0.Factors[4] = @float;
							float0.Positions[4] = (float_1 + float_2) / 2f;
							float0.Factors[5] = (1f - @float) * float_0 + @float;
							float0.Positions[5] = (float_1 + float_2) / 2f + (float_2 - float_1) / 2f * (1f - float_0);
							float0.Factors[6] = 1f;
							float0.Positions[6] = float_2;
							float0.Factors[7] = float_0;
							float0.Positions[7] = float_2 + (1f - float_2) * float_0;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
							if (float_1 == float_2)
							{
								float0.Factors[3] = 1f;
								float0.Factors[4] = 1f;
								float0.Factors[5] = 1f;
							}
							if (float_2 == 1f)
							{
								float0.Factors[7] = 1f;
								float0.Factors[8] = 1f;
							}
						}
						else
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = float_0;
							float0.Positions[1] = float_2 * (1f - float_0);
							float0.Factors[2] = 1f;
							float0.Positions[2] = float_2;
							float0.Factors[3] = (1f - @float) * float_0 + @float;
							float0.Positions[3] = float_2 + (float_1 - float_2) / 2f * float_0;
							float0.Factors[4] = @float;
							float0.Positions[4] = (float_1 + float_2) / 2f;
							float0.Factors[5] = (1f - @float) * float_0 + @float;
							float0.Positions[5] = (float_1 + float_2) / 2f + (float_1 - float_2) / 2f * (1f - float_0);
							float0.Factors[6] = 1f;
							float0.Positions[6] = float_1;
							float0.Factors[7] = float_0;
							float0.Positions[7] = float_1 + (1f - float_1) * float_0;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
						}
					}
				}
				else if ((double)float_0 < 0.49999999999 || (double)float_0 > 0.5000000000001)
				{
					flag2 = true;
					float0.Factors = new float[3];
					float0.Positions = new float[3];
					float0.Factors[0] = 0f;
					float0.Positions[0] = 0f;
					float0.Factors[1] = float_0;
					float0.Positions[1] = 1f - float_0;
					float0.Factors[2] = 1f;
					float0.Positions[2] = 1f;
				}
				if (int_0 > 0)
				{
					Rectangle rectangle5 = new Rectangle(rectangle.Left - 1, rectangle.Top - 1, rectangle.Width + 2, rectangle.Height + 2);
					LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(rectangle5, color_0, color_1, long_0);
					if (flag2)
					{
						linearGradientBrush2.Blend = float0;
					}
					Region region5 = new Region(graphicsPath);
					Region region6 = new Region(graphicsPath2);
					region5.Exclude(region6);
					graphics_0.FillRegion(linearGradientBrush2, region5);
					Pen pen14 = new Pen(linearGradientBrush2);
					graphics_0.DrawPath(pen14, graphicsPath);
					graphics_0.DrawPath(pen14, graphicsPath2);
					pen14.Dispose();
					linearGradientBrush2.Dispose();
					region5.Dispose();
					region6.Dispose();
				}
				if (int_1 > 0)
				{
					Region region7 = new Region(graphicsPath2);
					region7.Exclude(graphicsPath3);
					SolidBrush solidBrush7 = new SolidBrush(color_2);
					graphics_0.FillRegion(solidBrush7, region7);
					Pen pen15 = new Pen(solidBrush7);
					graphics_0.DrawPath(pen15, graphicsPath2);
					graphics_0.DrawPath(pen15, graphicsPath3);
					pen15.Dispose();
					solidBrush7.Dispose();
					region7.Dispose();
				}
				if (int_2 > 0)
				{
					LinearGradientBrush linearGradientBrush = (das_BorderGradientStyle_0 != DAS_BorderGradientStyle.BGS_Linear) ? new LinearGradientBrush(rectangle3, color_3, color_4, long_0 + 180) : new LinearGradientBrush(rectangle3, color_4, color_3, long_0);
					if (flag2)
					{
						linearGradientBrush.Blend = float0;
					}
					Region region8 = new Region(graphicsPath3);
					Region region9 = new Region(graphicsPath4);
					region8.Exclude(region9);
					graphics_0.FillRegion(linearGradientBrush, region8);
					Pen pen3 = new Pen(linearGradientBrush);
					graphics_0.DrawPath(pen3, graphicsPath3);
					graphics_0.DrawPath(pen3, graphicsPath4);
					pen3.Dispose();
					linearGradientBrush.Dispose();
					region8.Dispose();
					region9.Dispose();
				}
				break;
			}
			case DAS_BorderGradientStyle.BGS_Ring:
			{
				Blend blend3 = new Blend();
				blend3.Factors = new float[3];
				blend3.Positions = new float[3];
				Blend blend2 = blend3;
				blend2.Factors[0] = 0f;
				blend2.Positions[0] = 0f;
				blend2.Factors[2] = 1f;
				blend2.Positions[2] = 1f;
				if (int_0 > 0)
				{
					blend2.Factors[1] = float_0;
					blend2.Positions[1] = 1f - float_0;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						color_0
					};
					pathGradientBrush4.CenterColor = color_1;
					PathGradientBrush pathGradientBrush2 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle2.Width / (float)rectangle.Width;
					pointF3.Y = (float)rectangle2.Height / (float)rectangle.Height;
					PointF pointF = pathGradientBrush2.FocusScales = pointF3;
					Region region10 = new Region(graphicsPath);
					Region region11 = new Region(graphicsPath2);
					region10.Exclude(region11);
					graphics_0.FillRegion(pathGradientBrush2, region10);
					Pen pen4 = new Pen(color_0);
					graphics_0.DrawPath(pen4, graphicsPath);
					pen4.Dispose();
					pathGradientBrush2.Dispose();
					region10.Dispose();
					region11.Dispose();
				}
				if (int_1 > 0)
				{
					Region region12 = new Region(graphicsPath2);
					region12.Exclude(graphicsPath3);
					SolidBrush solidBrush8 = new SolidBrush(color_2);
					graphics_0.FillRegion(solidBrush8, region12);
					Pen pen5 = new Pen(solidBrush8);
					graphics_0.DrawPath(pen5, graphicsPath2);
					graphics_0.DrawPath(pen5, graphicsPath3);
					pen5.Dispose();
					solidBrush8.Dispose();
					region12.Dispose();
				}
				if (int_2 > 0)
				{
					blend2.Factors[1] = 1f - float_0;
					blend2.Positions[1] = float_0;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath3);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						color_4
					};
					pathGradientBrush4.Blend = blend2;
					pathGradientBrush4.CenterColor = color_3;
					PathGradientBrush pathGradientBrush3 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle4.Width / (float)rectangle3.Width;
					pointF3.Y = (float)rectangle4.Height / (float)rectangle3.Height;
					PointF pointF2 = pathGradientBrush3.FocusScales = pointF3;
					Region region14 = new Region(graphicsPath3);
					Region region15 = new Region(graphicsPath4);
					region14.Exclude(region15);
					graphics_0.FillRegion(pathGradientBrush3, region14);
					Pen pen6 = new Pen(color_3);
					graphics_0.DrawPath(pen6, graphicsPath4);
					pen6.Dispose();
					pathGradientBrush3.Dispose();
					region14.Dispose();
					region15.Dispose();
				}
				break;
			}
			}
			if (int_3 > 0)
			{
				SolidBrush solidBrush9 = new SolidBrush(color_5);
				GraphicsPath graphicsPath8 = new GraphicsPath();
				graphicsPath8.AddEllipse(rec);
				Region region16 = new Region(graphicsPath8);
				region16.Exclude(graphicsPath);
				graphics_0.FillRegion(solidBrush9, region16);
				Pen pen7 = new Pen(solidBrush9);
				graphics_0.DrawPath(pen7, graphicsPath8);
				graphics_0.DrawPath(pen7, graphicsPath);
				pen7.Dispose();
				solidBrush9.Dispose();
				region16.Dispose();
				graphicsPath8.Dispose();
			}
			graphicsPath4.Dispose();
			graphicsPath3.Dispose();
			graphicsPath.Dispose();
		}

		public void SetRoundRect2(Graphics g, Rectangle rec_width, int m_RoundRadius, int m_OuterBorderLength, Color m_OuterBorderDarkColor, Color m_OuterBorderLightColor, int m_MiddleBorderLength, Color m_MiddleBorderColor, int m_InnerBorderLength, Color m_InnerBorderDarkColor, Color m_InnerBorderLightColor, int m_BorderExteriorLength, Color m_BorderExteriorColor, DAS_BorderGradientStyle m_BorderGradientType, long m_BorderGradientAngle, float m_BorderGradientRate, float m_BorderGradientLightPos1, float m_BorderGradientLightPos2, float m_BorderLightIntermediateBrightness)
		{
			Rectangle rectangle = new Rectangle(rec_width.Left + m_BorderExteriorLength, rec_width.Top + m_BorderExteriorLength, rec_width.Width - 2 * m_BorderExteriorLength, rec_width.Height - 2 * m_BorderExteriorLength);
			Rectangle rectangle2 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength));
			Rectangle rectangle3 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength));
			Rectangle rectangle4 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength));
			int int0 = m_RoundRadius - m_BorderExteriorLength;
			GraphicsPath graphicsPath = new GraphicsPath();
			SetRoundRect(graphicsPath, rectangle, int0);
			int0 = m_RoundRadius - m_BorderExteriorLength - m_OuterBorderLength;
			GraphicsPath graphicsPath2 = new GraphicsPath();
			SetRoundRect(graphicsPath2, rectangle2, int0);
			int0 = m_RoundRadius - m_BorderExteriorLength - m_OuterBorderLength - m_MiddleBorderLength;
			GraphicsPath graphicsPath3 = new GraphicsPath();
			SetRoundRect(graphicsPath3, rectangle3, int0);
			int0 = m_RoundRadius - m_BorderExteriorLength - m_OuterBorderLength - m_MiddleBorderLength - m_InnerBorderLength;
			GraphicsPath graphicsPath4 = new GraphicsPath();
			SetRoundRect(graphicsPath4, rectangle4, int0);
			switch (m_BorderGradientType)
			{
			case DAS_BorderGradientStyle.BGS_Flat:
			{
				PointF[] left = new PointF[5];
				left[0].X = rectangle.Left;
				left[0].Y = rectangle.Top;
				left[1].X = rectangle.Left;
				left[1].Y = rectangle.Top + rectangle.Height;
				if (rectangle.Width < rectangle.Height)
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[2].Y = (float)(rectangle.Top + rectangle.Height) - (float)rectangle.Width / 2f;
					left[3].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Width / 2f;
				}
				else
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Height / 2f;
					left[2].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					left[3].X = (float)(rectangle.Left + rectangle.Width) - (float)rectangle.Height / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
				}
				left[4].X = rectangle.Left + rectangle.Width;
				left[4].Y = rectangle.Top;
				GraphicsPath graphicsPath5 = new GraphicsPath();
				graphicsPath5.AddPolygon(left);
				if (m_OuterBorderLength > 0)
				{
					Region region = new Region(graphicsPath);
					region.Exclude(graphicsPath2);
					SolidBrush solidBrush = new SolidBrush(m_OuterBorderLightColor);
					Region region2 = region.Clone();
					region2.Intersect(graphicsPath5);
					g.FillRegion(solidBrush, region2);
					g.SetClip(graphicsPath5);
					Pen pen = new Pen(solidBrush);
					g.DrawPath(pen, graphicsPath);
					g.DrawPath(pen, graphicsPath2);
					pen.Dispose();
					g.ResetClip();
					solidBrush.Dispose();
					region2.Dispose();
					SolidBrush solidBrush2 = new SolidBrush(m_OuterBorderDarkColor);
					Region region13 = region.Clone();
					region13.Exclude(graphicsPath5);
					g.FillRegion(solidBrush2, region13);
					g.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen2 = new Pen(solidBrush2);
					g.DrawPath(pen2, graphicsPath);
					g.DrawPath(pen2, graphicsPath2);
					pen2.Dispose();
					g.ResetClip();
					solidBrush2.Dispose();
					region13.Dispose();
					region.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region17 = new Region(graphicsPath2);
					region17.Exclude(graphicsPath3);
					SolidBrush solidBrush3 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush3, region17);
					Pen pen8 = new Pen(solidBrush3);
					g.DrawPath(pen8, graphicsPath2);
					g.DrawPath(pen8, graphicsPath3);
					pen8.Dispose();
					solidBrush3.Dispose();
					region17.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					Region region18 = new Region(graphicsPath3);
					region18.Exclude(graphicsPath4);
					SolidBrush solidBrush4 = new SolidBrush(m_InnerBorderDarkColor);
					Region region19 = region18.Clone();
					region19.Intersect(graphicsPath5);
					g.FillRegion(solidBrush4, region19);
					g.SetClip(graphicsPath5);
					Pen pen9 = new Pen(solidBrush4);
					g.DrawPath(pen9, graphicsPath3);
					g.DrawPath(pen9, graphicsPath4);
					pen9.Dispose();
					g.ResetClip();
					solidBrush4.Dispose();
					region19.Dispose();
					SolidBrush solidBrush5 = new SolidBrush(m_InnerBorderLightColor);
					Region region20 = region18.Clone();
					region20.Exclude(graphicsPath5);
					g.FillRegion(solidBrush5, region20);
					g.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen10 = new Pen(solidBrush5);
					g.DrawPath(pen10, graphicsPath3);
					g.DrawPath(pen10, graphicsPath4);
					pen10.Dispose();
					g.ResetClip();
					solidBrush5.Dispose();
					region20.Dispose();
					region18.Dispose();
				}
				graphicsPath5.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Path:
			{
				Blend blend = new Blend();
				bool flag = false;
				if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag = true;
					blend.Factors = new float[3];
					blend.Positions = new float[3];
					blend.Factors[0] = 0f;
					blend.Positions[0] = 0f;
					blend.Factors[1] = m_BorderGradientRate;
					blend.Positions[1] = 1f - m_BorderGradientRate;
					blend.Factors[2] = 1f;
					blend.Positions[2] = 1f;
				}
				Point[] point;
				PathGradientBrush pathGradientBrush4;
				PointF pointF3;
				if (m_OuterBorderLength > 0)
				{
					double long0 = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0;
					GraphicsPath graphicsPath6 = new GraphicsPath();
					point = new Point[4]
					{
						new Point(rectangle.Left - 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top + rectangle.Height + 1),
						new Point(rectangle.Left - 1, rectangle.Top + rectangle.Height + 1)
					};
					graphicsPath6.AddPolygon(point);
					pathGradientBrush4 = new PathGradientBrush(graphicsPath6);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					PathGradientBrush pathGradientBrush = pathGradientBrush4;
					pointF3 = default(PointF);
					pointF3.X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					pointF3.Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					PointF x = pointF3;
					float height;
					float single;
					if (Math.Abs(Math.Sin(long0)) < 1E-10)
					{
						single = 0f;
						height = (float)(Math.Cos(long0) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
					}
					else if (Math.Abs(Math.Cos(long0)) >= 1E-10)
					{
						height = (float)((double)(rectangle.Height - m_OuterBorderLength) / (2.0 * Math.Tan(long0)));
						single = (float)(Math.Tan(long0) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
						height = ((Math.Cos(long0) >= 0.0) ? Math.Abs(height) : (0f - Math.Abs(height)));
						single = ((Math.Sin(long0) >= 0.0) ? Math.Abs(single) : (0f - Math.Abs(single)));
						if (height < 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						else if (height > (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						if (single < 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
						else if (single > (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
					}
					else
					{
						height = 0f;
						single = (float)(Math.Sin(long0) * (double)(rectangle.Height - m_OuterBorderLength) / 2.0);
					}
					x.X += height;
					x.Y += single;
					if (flag)
					{
						pathGradientBrush.Blend = blend;
					}
					pathGradientBrush.CenterPoint = x;
					pathGradientBrush.CenterColor = m_OuterBorderLightColor;
					Region region21 = new Region(graphicsPath);
					Region region22 = new Region(graphicsPath2);
					region21.Exclude(region22);
					g.FillRegion(pathGradientBrush, region21);
					Pen pen11 = new Pen(m_OuterBorderDarkColor);
					g.DrawPath(pen11, graphicsPath);
					g.DrawPath(pen11, graphicsPath2);
					pen11.Dispose();
					pathGradientBrush.Dispose();
					region21.Dispose();
					region22.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region23 = new Region(graphicsPath2);
					region23.Exclude(graphicsPath3);
					SolidBrush solidBrush6 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush6, region23);
					Pen pen12 = new Pen(solidBrush6);
					g.DrawPath(pen12, graphicsPath2);
					g.DrawPath(pen12, graphicsPath3);
					pen12.Dispose();
					solidBrush6.Dispose();
					region23.Dispose();
				}
				if (m_InnerBorderLength <= 0)
				{
					break;
				}
				double num = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0 + 3.14159265358979;
				GraphicsPath graphicsPath7 = new GraphicsPath();
				point = new Point[4]
				{
					new Point(rectangle3.Left, rectangle3.Top),
					new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top),
					new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top + rectangle3.Height),
					new Point(rectangle3.Left, rectangle3.Top + rectangle3.Height)
				};
				graphicsPath7.AddPolygon(point);
				pathGradientBrush4 = new PathGradientBrush(graphicsPath7);
				pathGradientBrush4.SurroundColors = new Color[1]
				{
					m_InnerBorderDarkColor
				};
				PathGradientBrush color4 = pathGradientBrush4;
				pointF3 = default(PointF);
				pointF3.X = (float)rectangle3.Left + (float)rectangle3.Width / 2f;
				pointF3.Y = (float)rectangle3.Top + (float)rectangle3.Height / 2f;
				PointF y = pointF3;
				float width;
				float height2;
				if (Math.Abs(Math.Sin(num)) < 1E-10)
				{
					height2 = 0f;
					width = (float)(Math.Cos(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
				}
				else if (Math.Abs(Math.Cos(num)) >= 1E-10)
				{
					width = (float)((double)(rectangle3.Height - m_InnerBorderLength) / (2.0 * Math.Tan(num)));
					height2 = (float)(Math.Tan(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
					width = ((Math.Cos(num) >= 0.0) ? Math.Abs(width) : (0f - Math.Abs(width)));
					height2 = ((Math.Sin(num) >= 0.0) ? Math.Abs(height2) : (0f - Math.Abs(height2)));
					if (width < 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
					{
						width = 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
					}
					else if (width > (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
					{
						width = (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
					}
					if (height2 < 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
					{
						height2 = 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
					}
					else if (height2 > (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
					{
						height2 = (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
					}
				}
				else
				{
					width = 0f;
					height2 = (float)(Math.Sin(num) * (double)(rectangle3.Height - m_InnerBorderLength) / 2.0);
				}
				y.X += width;
				y.Y += height2;
				if (flag)
				{
					color4.Blend = blend;
				}
				color4.CenterPoint = y;
				color4.CenterColor = m_InnerBorderLightColor;
				Region region3 = new Region(graphicsPath3);
				Region region4 = new Region(graphicsPath4);
				region3.Exclude(region4);
				g.FillRegion(color4, region3);
				Pen pen13 = new Pen(m_InnerBorderDarkColor);
				g.DrawPath(pen13, graphicsPath3);
				g.DrawPath(pen13, graphicsPath4);
				pen13.Dispose();
				color4.Dispose();
				region3.Dispose();
				region4.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Linear:
			case DAS_BorderGradientStyle.BGS_Linear2:
			{
				Blend float0 = new Blend();
				bool flag2 = false;
				float @float = 0f;
				if (m_BorderLightIntermediateBrightness >= 0f && (double)m_BorderLightIntermediateBrightness <= 1.0)
				{
					@float = m_BorderLightIntermediateBrightness;
				}
				if (m_BorderGradientLightPos1 > 0f && (double)m_BorderGradientLightPos1 < 1.0)
				{
					flag2 = true;
					if (m_BorderGradientLightPos2 <= 0f || (double)m_BorderGradientLightPos2 > 1.0)
					{
						float0.Factors = new float[5];
						float0.Positions = new float[5];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = m_BorderGradientRate;
						float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
						float0.Factors[2] = 1f;
						float0.Positions[2] = m_BorderGradientLightPos1;
						float0.Factors[3] = m_BorderGradientRate;
						float0.Positions[3] = (1f - m_BorderGradientLightPos1) * m_BorderGradientRate + m_BorderGradientLightPos1;
						float0.Factors[4] = 0f;
						float0.Positions[4] = 1f;
					}
					else
					{
						float0.Factors = new float[9];
						float0.Positions = new float[9];
						if (m_BorderGradientLightPos1 <= m_BorderGradientLightPos2)
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos1;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos1 + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos2;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos2 + (1f - m_BorderGradientLightPos2) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
							if (m_BorderGradientLightPos1 == m_BorderGradientLightPos2)
							{
								float0.Factors[3] = 1f;
								float0.Factors[4] = 1f;
								float0.Factors[5] = 1f;
							}
							if (m_BorderGradientLightPos2 == 1f)
							{
								float0.Factors[7] = 1f;
								float0.Factors[8] = 1f;
							}
						}
						else
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos2 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos2;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos2 + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos1;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos1 + (1f - m_BorderGradientLightPos1) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
						}
					}
				}
				else if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag2 = true;
					float0.Factors = new float[3];
					float0.Positions = new float[3];
					float0.Factors[0] = 0f;
					float0.Positions[0] = 0f;
					float0.Factors[1] = m_BorderGradientRate;
					float0.Positions[1] = 1f - m_BorderGradientRate;
					float0.Factors[2] = 1f;
					float0.Positions[2] = 1f;
				}
				if (m_OuterBorderLength > 0)
				{
					Rectangle rectangle5 = new Rectangle(rectangle.Left - 1, rectangle.Top - 1, rectangle.Width + 2, rectangle.Height + 2);
					LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(rectangle5, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush2.Blend = float0;
					}
					Region region5 = new Region(graphicsPath);
					Region region6 = new Region(graphicsPath2);
					region5.Exclude(region6);
					g.FillRegion(linearGradientBrush2, region5);
					Pen pen14 = new Pen(linearGradientBrush2);
					g.DrawPath(pen14, graphicsPath);
					g.DrawPath(pen14, graphicsPath2);
					pen14.Dispose();
					linearGradientBrush2.Dispose();
					region5.Dispose();
					region6.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region7 = new Region(graphicsPath2);
					region7.Exclude(graphicsPath3);
					SolidBrush solidBrush7 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush7, region7);
					Pen pen15 = new Pen(solidBrush7);
					g.DrawPath(pen15, graphicsPath2);
					g.DrawPath(pen15, graphicsPath3);
					pen15.Dispose();
					solidBrush7.Dispose();
					region7.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					LinearGradientBrush linearGradientBrush = (m_BorderGradientType != DAS_BorderGradientStyle.BGS_Linear) ? new LinearGradientBrush(rectangle3, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderGradientAngle + 180) : new LinearGradientBrush(rectangle3, m_InnerBorderLightColor, m_InnerBorderDarkColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush.Blend = float0;
					}
					Region region8 = new Region(graphicsPath3);
					Region region9 = new Region(graphicsPath4);
					region8.Exclude(region9);
					g.FillRegion(linearGradientBrush, region8);
					Pen pen3 = new Pen(linearGradientBrush);
					g.DrawPath(pen3, graphicsPath3);
					g.DrawPath(pen3, graphicsPath4);
					pen3.Dispose();
					linearGradientBrush.Dispose();
					region8.Dispose();
					region9.Dispose();
				}
				break;
			}
			case DAS_BorderGradientStyle.BGS_Ring:
			{
				Blend blend3 = new Blend();
				blend3.Factors = new float[3];
				blend3.Positions = new float[3];
				Blend blend2 = blend3;
				blend2.Factors[0] = 0f;
				blend2.Positions[0] = 0f;
				blend2.Factors[2] = 1f;
				blend2.Positions[2] = 1f;
				if (m_OuterBorderLength > 0)
				{
					blend2.Factors[1] = m_BorderGradientRate;
					blend2.Positions[1] = 1f - m_BorderGradientRate;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					pathGradientBrush4.CenterColor = m_OuterBorderLightColor;
					PathGradientBrush pathGradientBrush2 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle2.Width / (float)rectangle.Width;
					pointF3.Y = (float)rectangle2.Height / (float)rectangle.Height;
					PointF pointF = pathGradientBrush2.FocusScales = pointF3;
					Region region10 = new Region(graphicsPath);
					Region region11 = new Region(graphicsPath2);
					region10.Exclude(region11);
					g.FillRegion(pathGradientBrush2, region10);
					Pen pen4 = new Pen(m_OuterBorderDarkColor);
					g.DrawPath(pen4, graphicsPath);
					pen4.Dispose();
					pathGradientBrush2.Dispose();
					region10.Dispose();
					region11.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region12 = new Region(graphicsPath2);
					region12.Exclude(graphicsPath3);
					SolidBrush solidBrush8 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush8, region12);
					Pen pen5 = new Pen(solidBrush8);
					g.DrawPath(pen5, graphicsPath2);
					g.DrawPath(pen5, graphicsPath3);
					pen5.Dispose();
					solidBrush8.Dispose();
					region12.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					blend2.Factors[1] = 1f - m_BorderGradientRate;
					blend2.Positions[1] = m_BorderGradientRate;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath3);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_InnerBorderLightColor
					};
					pathGradientBrush4.Blend = blend2;
					pathGradientBrush4.CenterColor = m_InnerBorderDarkColor;
					PathGradientBrush pathGradientBrush3 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle4.Width / (float)rectangle3.Width;
					pointF3.Y = (float)rectangle4.Height / (float)rectangle3.Height;
					PointF pointF2 = pathGradientBrush3.FocusScales = pointF3;
					Region region14 = new Region(graphicsPath3);
					Region region15 = new Region(graphicsPath4);
					region14.Exclude(region15);
					g.FillRegion(pathGradientBrush3, region14);
					Pen pen6 = new Pen(m_InnerBorderDarkColor);
					g.DrawPath(pen6, graphicsPath4);
					pen6.Dispose();
					pathGradientBrush3.Dispose();
					region14.Dispose();
					region15.Dispose();
				}
				break;
			}
			}
			if (m_BorderExteriorLength > 0)
			{
				SolidBrush solidBrush9 = new SolidBrush(m_BorderExteriorColor);
				GraphicsPath graphicsPath8 = new GraphicsPath();
				SetRoundRect(graphicsPath8, rec_width, m_RoundRadius);
				Region region16 = new Region(graphicsPath8);
				region16.Exclude(graphicsPath);
				g.FillRegion(solidBrush9, region16);
				Pen pen7 = new Pen(solidBrush9);
				g.DrawPath(pen7, graphicsPath8);
				g.DrawPath(pen7, graphicsPath);
				pen7.Dispose();
				solidBrush9.Dispose();
				region16.Dispose();
				graphicsPath8.Dispose();
			}
			graphicsPath4.Dispose();
			graphicsPath3.Dispose();
			graphicsPath.Dispose();
		}

		public void SetValue(Graphics g, Rectangle rectangle_0, DAS_ShapeStyle m_Shape, GraphicsPath graphicsPath_0, Color m_OffColor, Color m_OffGradientColor, DAS_BkGradientStyle m_GradientType, int m_GradientAngle, float m_GradientRate, int m_RoundRadius, int m_ArrowWidth, float m_ShinePosition)
		{
			Blend blend3;
			PathGradientBrush pathGradientBrush3;
			if (m_GradientType != DAS_BkGradientStyle.BKGS_Polygon && m_GradientType != DAS_BkGradientStyle.BKGS_Sphere)
			{
				switch (m_GradientType)
				{
				case DAS_BkGradientStyle.BKGS_Shine:
				{
					blend3 = new Blend();
					blend3.Factors = new float[3];
					blend3.Positions = new float[3];
					Blend blend = blend3;
					blend.Factors[0] = 0f;
					blend.Positions[0] = 0f;
					blend.Factors[1] = m_GradientRate;
					blend.Positions[1] = 1f - m_GradientRate;
					blend.Factors[2] = 1f;
					blend.Positions[2] = 1f;
					double int0 = 3.14159265358979 * (double)m_GradientAngle / 180.0;
					GraphicsPath graphicsPath = new GraphicsPath();
					Rectangle rectangle = new Rectangle(rectangle_0.Left - 1, rectangle_0.Top - 1, rectangle_0.Width + 2, rectangle_0.Height + 2);
					switch (m_Shape)
					{
					case DAS_ShapeStyle.SS_Rect:
						graphicsPath.AddRectangle(rectangle);
						break;
					case DAS_ShapeStyle.SS_RoundRect:
						SetRoundRect(graphicsPath, rectangle, m_RoundRadius);
						break;
					default:
						graphicsPath = (GraphicsPath)graphicsPath_0.Clone();
						break;
					case DAS_ShapeStyle.SS_Circle:
						graphicsPath.AddEllipse(rectangle);
						break;
					}
					pathGradientBrush3 = new PathGradientBrush(graphicsPath);
					pathGradientBrush3.SurroundColors = new Color[1]
					{
						m_OffColor
					};
					PathGradientBrush pathGradientBrush = pathGradientBrush3;
					PointF pointF = default(PointF);
					pointF.X = (float)rectangle_0.Left + (float)rectangle_0.Width / 2f;
					pointF.Y = (float)rectangle_0.Top + (float)rectangle_0.Height / 2f;
					PointF x = pointF;
					float height;
					float single;
					if (Math.Abs(Math.Sin(int0)) < 1E-10)
					{
						single = 0f;
						height = (float)(Math.Cos(int0) * (double)rectangle_0.Width / 2.0);
					}
					else if (Math.Abs(Math.Cos(int0)) >= 1E-10)
					{
						height = (float)((double)rectangle_0.Height / (2.0 * Math.Tan(int0)));
						single = (float)(Math.Tan(int0) * (double)rectangle_0.Width / 2.0);
						height = ((Math.Cos(int0) >= 0.0) ? Math.Abs(height) : (0f - Math.Abs(height)));
						single = ((Math.Sin(int0) >= 0.0) ? Math.Abs(single) : (0f - Math.Abs(single)));
						if (height < 0f - (float)rectangle_0.Width / 2f)
						{
							height = 0f - (float)rectangle_0.Width / 2f;
						}
						else if (height > (float)rectangle_0.Width / 2f)
						{
							height = (float)rectangle_0.Width / 2f;
						}
						if (single < 0f - (float)rectangle_0.Height / 2f)
						{
							single = 0f - (float)rectangle_0.Height / 2f;
						}
						else if (single > (float)rectangle_0.Height / 2f)
						{
							single = (float)rectangle_0.Height / 2f;
						}
					}
					else
					{
						height = 0f;
						single = (float)(Math.Sin(int0) * (double)rectangle_0.Height / 2.0);
					}
					if (m_Shape == DAS_ShapeStyle.SS_Circle)
					{
						height = (float)((double)rectangle_0.Width * Math.Cos(int0) / 2.0);
						single = (float)((double)rectangle_0.Height * Math.Sin(int0) / 2.0);
					}
					x.X += height * m_ShinePosition;
					x.Y += single * m_ShinePosition;
					pathGradientBrush.Blend = blend;
					pathGradientBrush.CenterPoint = x;
					pathGradientBrush.CenterColor = m_OffGradientColor;
					if (graphicsPath_0 != null)
					{
						g.FillPath(pathGradientBrush, graphicsPath_0);
					}
					else if (m_Shape != DAS_ShapeStyle.SS_Circle)
					{
						g.FillRectangle(pathGradientBrush, rectangle_0);
					}
					else
					{
						g.FillEllipse(pathGradientBrush, rectangle);
					}
					pathGradientBrush.Dispose();
					return;
				}
				default:
					return;
				case DAS_BkGradientStyle.BKGS_Linear:
				case DAS_BkGradientStyle.BKGS_Linear2:
				{
					Blend float0 = new Blend();
					if (m_GradientType != 0)
					{
						float0.Factors = new float[5];
						float0.Positions = new float[5];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = m_GradientRate;
						float0.Positions[1] = 0.5f * (1f - m_GradientRate);
						float0.Factors[2] = 1f;
						float0.Positions[2] = 0.5f;
						float0.Factors[3] = m_GradientRate;
						float0.Positions[3] = 0.5f + 0.5f * m_GradientRate;
						float0.Factors[4] = 0f;
						float0.Positions[4] = 1f;
					}
					else
					{
						float0.Factors = new float[3];
						float0.Positions = new float[3];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = m_GradientRate;
						float0.Positions[1] = 0.5f * (1f - m_GradientRate);
						float0.Factors[2] = 1f;
						float0.Positions[2] = 1f;
					}
					Rectangle rectangle2 = new Rectangle(rectangle_0.Left - 1, rectangle_0.Top - 1, rectangle_0.Width + 2, rectangle_0.Height + 2);
					LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rectangle2, m_OffColor, m_OffGradientColor, m_GradientAngle)
					{
						Blend = float0
					};
					if (graphicsPath_0 != null)
					{
						g.FillPath(linearGradientBrush, graphicsPath_0);
					}
					else
					{
						g.FillRectangle(linearGradientBrush, rectangle_0);
					}
					linearGradientBrush.Dispose();
					return;
				}
				case DAS_BkGradientStyle.BKGS_Sphere2:
					break;
				}
			}
			blend3 = new Blend();
			blend3.Factors = new float[3];
			blend3.Positions = new float[3];
			Blend blend2 = blend3;
			blend2.Factors[0] = 0f;
			blend2.Positions[0] = 0f;
			blend2.Factors[2] = 1f;
			blend2.Positions[2] = 1f;
			blend2.Factors[1] = m_GradientRate;
			blend2.Positions[1] = 1f - m_GradientRate;
			GraphicsPath graphicsPath2 = new GraphicsPath();
			if (m_GradientType != DAS_BkGradientStyle.BKGS_Polygon)
			{
				RectangleF rectangleF = (m_Shape == DAS_ShapeStyle.SS_Circle) ? new RectangleF(rectangle_0.Left - 1, rectangle_0.Top - 1, rectangle_0.Width + 2, rectangle_0.Height + 2) : new RectangleF((float)rectangle_0.Left - 0.415f * (float)rectangle_0.Width / 2f, (float)rectangle_0.Top - 0.415f * (float)rectangle_0.Height / 2f, 1.415f * (float)rectangle_0.Width, 1.415f * (float)rectangle_0.Height);
				graphicsPath2.AddEllipse(rectangleF);
			}
			else
			{
				Point[] point = new Point[4]
				{
					new Point(rectangle_0.Left, rectangle_0.Top),
					new Point(rectangle_0.Left + rectangle_0.Width, rectangle_0.Top),
					new Point(rectangle_0.Left + rectangle_0.Width, rectangle_0.Top + rectangle_0.Height),
					new Point(rectangle_0.Left, rectangle_0.Top + rectangle_0.Height)
				};
				graphicsPath2.AddPolygon(point);
			}
			pathGradientBrush3 = new PathGradientBrush(graphicsPath2);
			pathGradientBrush3.SurroundColors = new Color[1]
			{
				m_OffColor
			};
			pathGradientBrush3.Blend = blend2;
			pathGradientBrush3.CenterColor = m_OffGradientColor;
			PathGradientBrush pathGradientBrush2 = pathGradientBrush3;
			if (m_GradientType == DAS_BkGradientStyle.BKGS_Sphere2)
			{
				PointF width = default(PointF);
				if (rectangle_0.Width <= rectangle_0.Height)
				{
					width.X = 0f;
					width.Y = (float)(rectangle_0.Height - rectangle_0.Width) / (float)rectangle_0.Height;
				}
				else
				{
					width.X = (float)(rectangle_0.Width - rectangle_0.Height) / (float)rectangle_0.Width;
					width.Y = 0f;
				}
				pathGradientBrush2.FocusScales = width;
			}
			if (graphicsPath_0 != null)
			{
				g.FillPath(pathGradientBrush2, graphicsPath_0);
			}
			else
			{
				g.FillRectangle(pathGradientBrush2, rectangle_0);
			}
			pathGradientBrush2.Dispose();
		}

		public void SetRoundRect(GraphicsPath graphicsPath_0, Rectangle rec, int m_RoundRadius)
		{
			int int0 = m_RoundRadius;
			int left = rec.Left;
			int top = rec.Top;
			int width = rec.Width - 1;
			int height = rec.Height - 1;
			if (2 * int0 > width)
			{
				int0 = width / 2;
			}
			if (2 * int0 > height)
			{
				int0 = height / 2;
			}
			if (int0 <= 0)
			{
				graphicsPath_0.AddLine(left, top, left + width, top);
				graphicsPath_0.AddLine(left + width, top, left + width, top + height);
				graphicsPath_0.AddLine(left + width, top + height, left, top + height);
				graphicsPath_0.AddLine(left, top + height, left, top);
			}
			else
			{
				graphicsPath_0.AddLine(left + int0, top, left + width - int0, top);
				graphicsPath_0.AddArc(left + width - 2 * int0, top, 2 * int0, 2 * int0, 270f, 90f);
				graphicsPath_0.AddLine(left + width, top + int0, left + width, top + height - int0);
				graphicsPath_0.AddArc(left + width - 2 * int0, top + height - 2 * int0, 2 * int0, 2 * int0, 0f, 90f);
				graphicsPath_0.AddLine(left + width - int0, top + height, left + int0, top + height);
				graphicsPath_0.AddArc(left, top + height - 2 * int0, 2 * int0, 2 * int0, 90f, 90f);
				graphicsPath_0.AddLine(left, top + height - int0, left, top + int0);
				graphicsPath_0.AddArc(left, top, 2 * int0, 2 * int0, 180f, 90f);
			}
			graphicsPath_0.CloseFigure();
		}

		public void SetArrow(Graphics g, Rectangle rec_width, DAS_ShapeStyle m_Shape, int m_RoundRadius, int m_ArrowWidth, int m_OuterBorderLength, Color m_OuterBorderDarkColor, Color m_OuterBorderLightColor, int m_MiddleBorderLength, Color m_MiddleBorderColor, int m_InnerBorderLength, Color m_InnerBorderDarkColor, Color m_InnerBorderLightColor, int m_BorderExteriorLength, Color m_BorderExteriorColor, DAS_BorderGradientStyle m_BorderGradientType, long m_BorderGradientAngle, float m_BorderGradientRate, float m_BorderGradientLightPos1, float m_BorderGradientLightPos2, float m_BorderLightIntermediateBrightness)
		{
			switch (m_Shape)
			{
			case DAS_ShapeStyle.SS_Rect:
				SetRect(g, rec_width, m_OuterBorderLength, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_MiddleBorderLength, m_MiddleBorderColor, m_InnerBorderLength, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderExteriorLength, m_BorderExteriorColor, m_BorderGradientType, (int)m_BorderGradientAngle, m_BorderGradientRate, m_BorderGradientLightPos1, m_BorderGradientLightPos2, m_BorderLightIntermediateBrightness);
				return;
			case DAS_ShapeStyle.SS_RoundRect:
				SetRoundRect2(g, rec_width, m_RoundRadius, m_OuterBorderLength, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_MiddleBorderLength, m_MiddleBorderColor, m_InnerBorderLength, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderExteriorLength, m_BorderExteriorColor, m_BorderGradientType, m_BorderGradientAngle, m_BorderGradientRate, m_BorderGradientLightPos1, m_BorderGradientLightPos2, m_BorderLightIntermediateBrightness);
				return;
			case DAS_ShapeStyle.SS_Circle:
				SetCircle(g, rec_width, m_OuterBorderLength, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_MiddleBorderLength, m_MiddleBorderColor, m_InnerBorderLength, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderExteriorLength, m_BorderExteriorColor, m_BorderGradientType, m_BorderGradientAngle, m_BorderGradientRate, m_BorderGradientLightPos1, m_BorderGradientLightPos2, m_BorderLightIntermediateBrightness);
				return;
			}
			Rectangle rectangle = new Rectangle(rec_width.Left + m_BorderExteriorLength, rec_width.Top + m_BorderExteriorLength, rec_width.Width - 2 * m_BorderExteriorLength, rec_width.Height - 2 * m_BorderExteriorLength);
			Rectangle rectangle2 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength));
			Rectangle rectangle3 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength));
			Rectangle rectangle4 = new Rectangle(rec_width.Left + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Top + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, rec_width.Width - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength), rec_width.Height - 2 * (m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength));
			int int1 = m_ArrowWidth + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength;
			GraphicsPath graphicsPath = new GraphicsPath();
			method_6(graphicsPath, rec_width, m_Shape, int1, m_BorderExteriorLength);
			GraphicsPath graphicsPath2 = new GraphicsPath();
			method_6(graphicsPath2, rec_width, m_Shape, int1, m_BorderExteriorLength + m_OuterBorderLength);
			GraphicsPath graphicsPath3 = new GraphicsPath();
			method_6(graphicsPath3, rec_width, m_Shape, int1, m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength);
			GraphicsPath graphicsPath4 = new GraphicsPath();
			method_6(graphicsPath4, rec_width, m_Shape, int1, m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength);
			switch (m_BorderGradientType)
			{
			case DAS_BorderGradientStyle.BGS_Flat:
			{
				PointF[] left = new PointF[5];
				left[0].X = rectangle.Left;
				left[0].Y = rectangle.Top;
				left[1].X = rectangle.Left;
				left[1].Y = rectangle.Top + rectangle.Height;
				if (rectangle.Width < rectangle.Height)
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[2].Y = (float)(rectangle.Top + rectangle.Height) - (float)rectangle.Width / 2f;
					left[3].X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Width / 2f;
				}
				else
				{
					left[2].X = (float)rectangle.Left + (float)rectangle.Height / 2f;
					left[2].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					left[3].X = (float)(rectangle.Left + rectangle.Width) - (float)rectangle.Height / 2f;
					left[3].Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
				}
				left[4].X = rectangle.Left + rectangle.Width;
				left[4].Y = rectangle.Top;
				GraphicsPath graphicsPath5 = new GraphicsPath();
				graphicsPath5.AddPolygon(left);
				if (m_OuterBorderLength > 0)
				{
					Region region = new Region(graphicsPath);
					region.Exclude(graphicsPath2);
					SolidBrush solidBrush = new SolidBrush(m_OuterBorderLightColor);
					Region region2 = region.Clone();
					region2.Intersect(graphicsPath5);
					g.FillRegion(solidBrush, region2);
					g.SetClip(graphicsPath5);
					Pen pen = new Pen(solidBrush);
					g.DrawPath(pen, graphicsPath);
					g.DrawPath(pen, graphicsPath2);
					pen.Dispose();
					g.ResetClip();
					solidBrush.Dispose();
					region2.Dispose();
					SolidBrush solidBrush2 = new SolidBrush(m_OuterBorderDarkColor);
					Region region13 = region.Clone();
					region13.Exclude(graphicsPath5);
					g.FillRegion(solidBrush2, region13);
					g.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen2 = new Pen(solidBrush2);
					g.DrawPath(pen2, graphicsPath);
					g.DrawPath(pen2, graphicsPath2);
					pen2.Dispose();
					g.ResetClip();
					solidBrush2.Dispose();
					region13.Dispose();
					region.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region17 = new Region(graphicsPath2);
					region17.Exclude(graphicsPath3);
					SolidBrush solidBrush3 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush3, region17);
					Pen pen8 = new Pen(solidBrush3);
					g.DrawPath(pen8, graphicsPath2);
					g.DrawPath(pen8, graphicsPath3);
					pen8.Dispose();
					solidBrush3.Dispose();
					region17.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					Region region18 = new Region(graphicsPath3);
					region18.Exclude(graphicsPath4);
					SolidBrush solidBrush4 = new SolidBrush(m_InnerBorderDarkColor);
					Region region19 = region18.Clone();
					region19.Intersect(graphicsPath5);
					g.FillRegion(solidBrush4, region19);
					g.SetClip(graphicsPath5);
					Pen pen9 = new Pen(solidBrush4);
					g.DrawPath(pen9, graphicsPath3);
					g.DrawPath(pen9, graphicsPath4);
					pen9.Dispose();
					g.ResetClip();
					solidBrush4.Dispose();
					region19.Dispose();
					SolidBrush solidBrush5 = new SolidBrush(m_InnerBorderLightColor);
					Region region20 = region18.Clone();
					region20.Exclude(graphicsPath5);
					g.FillRegion(solidBrush5, region20);
					g.SetClip(graphicsPath5, CombineMode.Exclude);
					Pen pen10 = new Pen(solidBrush5);
					g.DrawPath(pen10, graphicsPath3);
					g.DrawPath(pen10, graphicsPath4);
					pen10.Dispose();
					g.ResetClip();
					solidBrush5.Dispose();
					region20.Dispose();
					region18.Dispose();
				}
				graphicsPath5.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Path:
			{
				Blend blend = new Blend();
				bool flag = false;
				if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag = true;
					blend.Factors = new float[3];
					blend.Positions = new float[3];
					blend.Factors[0] = 0f;
					blend.Positions[0] = 0f;
					blend.Factors[1] = m_BorderGradientRate;
					blend.Positions[1] = 1f - m_BorderGradientRate;
					blend.Factors[2] = 1f;
					blend.Positions[2] = 1f;
				}
				Point[] point;
				PathGradientBrush pathGradientBrush4;
				PointF pointF3;
				if (m_OuterBorderLength > 0)
				{
					double long0 = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0;
					GraphicsPath graphicsPath6 = new GraphicsPath();
					point = new Point[4]
					{
						new Point(rectangle.Left - 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top - 1),
						new Point(rectangle.Left + rectangle.Width + 1, rectangle.Top + rectangle.Height + 1),
						new Point(rectangle.Left - 1, rectangle.Top + rectangle.Height + 1)
					};
					graphicsPath6.AddPolygon(point);
					pathGradientBrush4 = new PathGradientBrush(graphicsPath6);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					PathGradientBrush pathGradientBrush = pathGradientBrush4;
					pointF3 = default(PointF);
					pointF3.X = (float)rectangle.Left + (float)rectangle.Width / 2f;
					pointF3.Y = (float)rectangle.Top + (float)rectangle.Height / 2f;
					PointF x = pointF3;
					float height;
					float single;
					if (Math.Abs(Math.Sin(long0)) < 1E-10)
					{
						single = 0f;
						height = (float)(Math.Cos(long0) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
					}
					else if (Math.Abs(Math.Cos(long0)) >= 1E-10)
					{
						height = (float)((double)(rectangle.Height - m_OuterBorderLength) / (2.0 * Math.Tan(long0)));
						single = (float)(Math.Tan(long0) * (double)(rectangle.Width - m_OuterBorderLength) / 2.0);
						height = ((Math.Cos(long0) >= 0.0) ? Math.Abs(height) : (0f - Math.Abs(height)));
						single = ((Math.Sin(long0) >= 0.0) ? Math.Abs(single) : (0f - Math.Abs(single)));
						if (height < 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = 0f - (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						else if (height > (float)(rectangle.Width - m_OuterBorderLength) / 2f)
						{
							height = (float)(rectangle.Width - m_OuterBorderLength) / 2f;
						}
						if (single < 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = 0f - (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
						else if (single > (float)(rectangle.Height - m_OuterBorderLength) / 2f)
						{
							single = (float)(rectangle.Height - m_OuterBorderLength) / 2f;
						}
					}
					else
					{
						height = 0f;
						single = (float)(Math.Sin(long0) * (double)(rectangle.Height - m_OuterBorderLength) / 2.0);
					}
					x.X += height * m_BorderGradientLightPos1;
					x.Y += single * m_BorderGradientLightPos1;
					if (flag)
					{
						pathGradientBrush.Blend = blend;
					}
					pathGradientBrush.CenterPoint = x;
					pathGradientBrush.CenterColor = m_OuterBorderLightColor;
					Region region21 = new Region(graphicsPath);
					Region region22 = new Region(graphicsPath2);
					region21.Exclude(region22);
					g.FillRegion(pathGradientBrush, region21);
					Pen pen11 = new Pen(m_OuterBorderDarkColor);
					g.DrawPath(pen11, graphicsPath);
					g.DrawPath(pen11, graphicsPath2);
					pen11.Dispose();
					pathGradientBrush.Dispose();
					region21.Dispose();
					region22.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region23 = new Region(graphicsPath2);
					region23.Exclude(graphicsPath3);
					SolidBrush solidBrush6 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush6, region23);
					Pen pen12 = new Pen(solidBrush6);
					g.DrawPath(pen12, graphicsPath2);
					g.DrawPath(pen12, graphicsPath3);
					pen12.Dispose();
					solidBrush6.Dispose();
					region23.Dispose();
				}
				if (m_InnerBorderLength <= 0)
				{
					break;
				}
				double num = 3.14159265358979 * (double)m_BorderGradientAngle / 180.0 + 3.14159265358979;
				GraphicsPath graphicsPath7 = new GraphicsPath();
				point = new Point[4]
				{
					new Point(rectangle3.Left, rectangle3.Top),
					new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top),
					new Point(rectangle3.Left + rectangle3.Width, rectangle3.Top + rectangle3.Height),
					new Point(rectangle3.Left, rectangle3.Top + rectangle3.Height)
				};
				graphicsPath7.AddPolygon(point);
				pathGradientBrush4 = new PathGradientBrush(graphicsPath7);
				pathGradientBrush4.SurroundColors = new Color[1]
				{
					m_InnerBorderDarkColor
				};
				PathGradientBrush color4 = pathGradientBrush4;
				pointF3 = default(PointF);
				pointF3.X = (float)rectangle3.Left + (float)rectangle3.Width / 2f;
				pointF3.Y = (float)rectangle3.Top + (float)rectangle3.Height / 2f;
				PointF y = pointF3;
				float width;
				float height2;
				if (Math.Abs(Math.Sin(num)) < 1E-10)
				{
					height2 = 0f;
					width = (float)(Math.Cos(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
				}
				else if (Math.Abs(Math.Cos(num)) >= 1E-10)
				{
					width = (float)((double)(rectangle3.Height - m_InnerBorderLength) / (2.0 * Math.Tan(num)));
					height2 = (float)(Math.Tan(num) * (double)(rectangle3.Width - m_InnerBorderLength) / 2.0);
					width = ((Math.Cos(num) >= 0.0) ? Math.Abs(width) : (0f - Math.Abs(width)));
					height2 = ((Math.Sin(num) >= 0.0) ? Math.Abs(height2) : (0f - Math.Abs(height2)));
					if (width < 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
					{
						width = 0f - (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
					}
					else if (width > (float)(rectangle3.Width - m_InnerBorderLength) / 2f)
					{
						width = (float)(rectangle3.Width - m_InnerBorderLength) / 2f;
					}
					if (height2 < 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
					{
						height2 = 0f - (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
					}
					else if (height2 > (float)(rectangle3.Height - m_InnerBorderLength) / 2f)
					{
						height2 = (float)(rectangle3.Height - m_InnerBorderLength) / 2f;
					}
				}
				else
				{
					width = 0f;
					height2 = (float)(Math.Sin(num) * (double)(rectangle3.Height - m_InnerBorderLength) / 2.0);
				}
				y.X += width * m_BorderGradientLightPos1;
				y.Y += height2 * m_BorderGradientLightPos1;
				if (flag)
				{
					color4.Blend = blend;
				}
				color4.CenterPoint = y;
				color4.CenterColor = m_InnerBorderLightColor;
				Region region3 = new Region(graphicsPath3);
				Region region4 = new Region(graphicsPath4);
				region3.Exclude(region4);
				g.FillRegion(color4, region3);
				Pen pen13 = new Pen(m_InnerBorderDarkColor);
				g.DrawPath(pen13, graphicsPath3);
				g.DrawPath(pen13, graphicsPath4);
				pen13.Dispose();
				color4.Dispose();
				region3.Dispose();
				region4.Dispose();
				break;
			}
			case DAS_BorderGradientStyle.BGS_Linear:
			case DAS_BorderGradientStyle.BGS_Linear2:
			{
				Blend float0 = new Blend();
				bool flag2 = false;
				float @float = 0f;
				if (m_BorderLightIntermediateBrightness >= 0f && (double)m_BorderLightIntermediateBrightness <= 1.0)
				{
					@float = m_BorderLightIntermediateBrightness;
				}
				if (m_BorderGradientLightPos1 > 0f && (double)m_BorderGradientLightPos1 < 1.0)
				{
					flag2 = true;
					if (m_BorderGradientLightPos2 <= 0f || (double)m_BorderGradientLightPos2 > 1.0)
					{
						float0.Factors = new float[5];
						float0.Positions = new float[5];
						float0.Factors[0] = 0f;
						float0.Positions[0] = 0f;
						float0.Factors[1] = m_BorderGradientRate;
						float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
						float0.Factors[2] = 1f;
						float0.Positions[2] = m_BorderGradientLightPos1;
						float0.Factors[3] = m_BorderGradientRate;
						float0.Positions[3] = (1f - m_BorderGradientLightPos1) * m_BorderGradientRate + m_BorderGradientLightPos1;
						float0.Factors[4] = 0f;
						float0.Positions[4] = 1f;
					}
					else
					{
						float0.Factors = new float[9];
						float0.Positions = new float[9];
						if (m_BorderGradientLightPos1 <= m_BorderGradientLightPos2)
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos1 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos1;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos1 + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos2 - m_BorderGradientLightPos1) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos2;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos2 + (1f - m_BorderGradientLightPos2) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
							if (m_BorderGradientLightPos1 == m_BorderGradientLightPos2)
							{
								float0.Factors[3] = 1f;
								float0.Factors[4] = 1f;
								float0.Factors[5] = 1f;
							}
							if (m_BorderGradientLightPos2 == 1f)
							{
								float0.Factors[7] = 1f;
								float0.Factors[8] = 1f;
							}
						}
						else
						{
							float0.Factors[0] = 0f;
							float0.Positions[0] = 0f;
							float0.Factors[1] = m_BorderGradientRate;
							float0.Positions[1] = m_BorderGradientLightPos2 * (1f - m_BorderGradientRate);
							float0.Factors[2] = 1f;
							float0.Positions[2] = m_BorderGradientLightPos2;
							float0.Factors[3] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[3] = m_BorderGradientLightPos2 + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * m_BorderGradientRate;
							float0.Factors[4] = @float;
							float0.Positions[4] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f;
							float0.Factors[5] = (1f - @float) * m_BorderGradientRate + @float;
							float0.Positions[5] = (m_BorderGradientLightPos1 + m_BorderGradientLightPos2) / 2f + (m_BorderGradientLightPos1 - m_BorderGradientLightPos2) / 2f * (1f - m_BorderGradientRate);
							float0.Factors[6] = 1f;
							float0.Positions[6] = m_BorderGradientLightPos1;
							float0.Factors[7] = m_BorderGradientRate;
							float0.Positions[7] = m_BorderGradientLightPos1 + (1f - m_BorderGradientLightPos1) * m_BorderGradientRate;
							float0.Factors[8] = 0f;
							float0.Positions[8] = 1f;
						}
					}
				}
				else if ((double)m_BorderGradientRate < 0.49999999999 || (double)m_BorderGradientRate > 0.5000000000001)
				{
					flag2 = true;
					float0.Factors = new float[3];
					float0.Positions = new float[3];
					float0.Factors[0] = 0f;
					float0.Positions[0] = 0f;
					float0.Factors[1] = m_BorderGradientRate;
					float0.Positions[1] = 1f - m_BorderGradientRate;
					float0.Factors[2] = 1f;
					float0.Positions[2] = 1f;
				}
				if (m_OuterBorderLength > 0)
				{
					Rectangle rectangle5 = new Rectangle(rectangle.Left - 1, rectangle.Top - 1, rectangle.Width + 2, rectangle.Height + 2);
					LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(rectangle5, m_OuterBorderDarkColor, m_OuterBorderLightColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush2.Blend = float0;
					}
					Region region5 = new Region(graphicsPath);
					Region region6 = new Region(graphicsPath2);
					region5.Exclude(region6);
					g.FillRegion(linearGradientBrush2, region5);
					Pen pen14 = new Pen(linearGradientBrush2);
					g.DrawPath(pen14, graphicsPath);
					g.DrawPath(pen14, graphicsPath2);
					pen14.Dispose();
					linearGradientBrush2.Dispose();
					region5.Dispose();
					region6.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region7 = new Region(graphicsPath2);
					region7.Exclude(graphicsPath3);
					SolidBrush solidBrush7 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush7, region7);
					Pen pen15 = new Pen(solidBrush7);
					g.DrawPath(pen15, graphicsPath2);
					g.DrawPath(pen15, graphicsPath3);
					pen15.Dispose();
					solidBrush7.Dispose();
					region7.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					LinearGradientBrush linearGradientBrush = (m_BorderGradientType != DAS_BorderGradientStyle.BGS_Linear) ? new LinearGradientBrush(rectangle3, m_InnerBorderDarkColor, m_InnerBorderLightColor, m_BorderGradientAngle + 180) : new LinearGradientBrush(rectangle3, m_InnerBorderLightColor, m_InnerBorderDarkColor, m_BorderGradientAngle);
					if (flag2)
					{
						linearGradientBrush.Blend = float0;
					}
					Region region8 = new Region(graphicsPath3);
					Region region9 = new Region(graphicsPath4);
					region8.Exclude(region9);
					g.FillRegion(linearGradientBrush, region8);
					Pen pen3 = new Pen(linearGradientBrush);
					g.DrawPath(pen3, graphicsPath3);
					g.DrawPath(pen3, graphicsPath4);
					pen3.Dispose();
					linearGradientBrush.Dispose();
					region8.Dispose();
					region9.Dispose();
				}
				break;
			}
			case DAS_BorderGradientStyle.BGS_Ring:
			{
				Blend blend3 = new Blend();
				blend3.Factors = new float[3];
				blend3.Positions = new float[3];
				Blend blend2 = blend3;
				blend2.Factors[0] = 0f;
				blend2.Positions[0] = 0f;
				blend2.Factors[2] = 1f;
				blend2.Positions[2] = 1f;
				if (m_OuterBorderLength > 0)
				{
					blend2.Factors[1] = m_BorderGradientRate;
					blend2.Positions[1] = 1f - m_BorderGradientRate;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_OuterBorderDarkColor
					};
					pathGradientBrush4.CenterColor = m_OuterBorderLightColor;
					PathGradientBrush pathGradientBrush2 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle2.Width / (float)rectangle.Width;
					pointF3.Y = (float)rectangle2.Height / (float)rectangle.Height;
					PointF pointF = pathGradientBrush2.FocusScales = pointF3;
					Region region10 = new Region(graphicsPath);
					Region region11 = new Region(graphicsPath2);
					region10.Exclude(region11);
					g.FillRegion(pathGradientBrush2, region10);
					Pen pen4 = new Pen(m_OuterBorderDarkColor);
					g.DrawPath(pen4, graphicsPath);
					pen4.Dispose();
					pathGradientBrush2.Dispose();
					region10.Dispose();
					region11.Dispose();
				}
				if (m_MiddleBorderLength > 0)
				{
					Region region12 = new Region(graphicsPath2);
					region12.Exclude(graphicsPath3);
					SolidBrush solidBrush8 = new SolidBrush(m_MiddleBorderColor);
					g.FillRegion(solidBrush8, region12);
					Pen pen5 = new Pen(solidBrush8);
					g.DrawPath(pen5, graphicsPath2);
					g.DrawPath(pen5, graphicsPath3);
					pen5.Dispose();
					solidBrush8.Dispose();
					region12.Dispose();
				}
				if (m_InnerBorderLength > 0)
				{
					blend2.Factors[1] = 1f - m_BorderGradientRate;
					blend2.Positions[1] = m_BorderGradientRate;
					PathGradientBrush pathGradientBrush4 = new PathGradientBrush(graphicsPath3);
					pathGradientBrush4.SurroundColors = new Color[1]
					{
						m_InnerBorderLightColor
					};
					pathGradientBrush4.Blend = blend2;
					pathGradientBrush4.CenterColor = m_InnerBorderDarkColor;
					PathGradientBrush pathGradientBrush3 = pathGradientBrush4;
					PointF pointF3 = default(PointF);
					pointF3.X = (float)rectangle4.Width / (float)rectangle3.Width;
					pointF3.Y = (float)rectangle4.Height / (float)rectangle3.Height;
					PointF pointF2 = pathGradientBrush3.FocusScales = pointF3;
					Region region14 = new Region(graphicsPath3);
					Region region15 = new Region(graphicsPath4);
					region14.Exclude(region15);
					g.FillRegion(pathGradientBrush3, region14);
					Pen pen6 = new Pen(m_InnerBorderDarkColor);
					g.DrawPath(pen6, graphicsPath4);
					pen6.Dispose();
					pathGradientBrush3.Dispose();
					region14.Dispose();
					region15.Dispose();
				}
				break;
			}
			}
			if (m_BorderExteriorLength > 0)
			{
				SolidBrush solidBrush9 = new SolidBrush(m_BorderExteriorColor);
				GraphicsPath graphicsPath8 = new GraphicsPath();
				method_6(graphicsPath8, rec_width, m_Shape, m_ArrowWidth + m_BorderExteriorLength + m_OuterBorderLength + m_MiddleBorderLength + m_InnerBorderLength, 0);
				Region region16 = new Region(graphicsPath8);
				region16.Exclude(graphicsPath);
				g.FillRegion(solidBrush9, region16);
				Pen pen7 = new Pen(solidBrush9);
				g.DrawPath(pen7, graphicsPath8);
				g.DrawPath(pen7, graphicsPath);
				pen7.Dispose();
				solidBrush9.Dispose();
				region16.Dispose();
				graphicsPath8.Dispose();
			}
			graphicsPath4.Dispose();
			graphicsPath3.Dispose();
			graphicsPath.Dispose();
		}

		internal void method_6(GraphicsPath graphicsPath_0, Rectangle rectangle_0, DAS_ShapeStyle das_ShapeStyle_0, int int_0, int int_1)
		{
			int int0 = int_0;
			int left = rectangle_0.Left;
			int top = rectangle_0.Top;
			int width = rectangle_0.Width - 1;
			int height = rectangle_0.Height - 1;
			float int1 = 0f;
			float single = 0f;
			if (int0 > width - 2)
			{
				int0 = width - 2;
			}
			if (int0 > height - 2)
			{
				int0 = height - 2;
			}
			switch (das_ShapeStyle_0)
			{
			case DAS_ShapeStyle.SS_Left_Triangle:
				int1 = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Width * rectangle_0.Width) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) / ((double)rectangle_0.Height / 2.0));
				single = (float)((double)(int_1 * rectangle_0.Width) / (Math.Sqrt((double)(rectangle_0.Width * rectangle_0.Width) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) - (double)rectangle_0.Height / 2.0));
				graphicsPath_0.AddLine((float)left + int1, top + height / 2, left + width - int_1, (float)top + single);
				graphicsPath_0.AddLine(left + width - int_1, (float)top + single, left + width - int_1, (float)(top + height) - single);
				graphicsPath_0.AddLine(left + width - int_1, (float)(top + height) - single, (float)left + int1, top + height / 2);
				break;
			case DAS_ShapeStyle.SS_Right_Triangle:
				int1 = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Width * rectangle_0.Width) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) / ((double)rectangle_0.Height / 2.0));
				single = (float)((double)(int_1 * rectangle_0.Width) / (Math.Sqrt((double)(rectangle_0.Width * rectangle_0.Width) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) - (double)rectangle_0.Height / 2.0));
				graphicsPath_0.AddLine((float)(left + width) - int1, top + height / 2, left + int_1, (float)top + single);
				graphicsPath_0.AddLine(left + int_1, (float)top + single, left + int_1, (float)(top + height) - single);
				graphicsPath_0.AddLine(left + int_1, (float)(top + height) - single, (float)(left + width) - int1, top + height / 2);
				break;
			case DAS_ShapeStyle.SS_Top_Triangle:
				single = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Width / 2.0));
				int1 = (float)((double)(int_1 * rectangle_0.Height) / (Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) - (double)rectangle_0.Width / 2.0));
				graphicsPath_0.AddLine(left + width / 2, (float)top + single, (float)(left + width) - int1, top + height - int_1);
				graphicsPath_0.AddLine((float)(left + width) - int1, top + height - int_1, (float)left + int1, top + height - int_1);
				graphicsPath_0.AddLine((float)left + int1, top + height - int_1, left + width / 2, (float)top + single);
				break;
			case DAS_ShapeStyle.SS_Bottom_Triangle:
				single = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Width / 2.0));
				int1 = (float)((double)(int_1 * rectangle_0.Height) / (Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) - (double)rectangle_0.Width / 2.0));
				graphicsPath_0.AddLine(left + width / 2, (float)(top + height) - single, (float)(left + width) - int1, top + int_1);
				graphicsPath_0.AddLine((float)(left + width) - int1, top + int_1, (float)left + int1, top + int_1);
				graphicsPath_0.AddLine((float)left + int1, top + int_1, left + width / 2, (float)(top + height) - single);
				break;
			case DAS_ShapeStyle.SS_Diamond:
				int1 = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height + rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Height / 2.0));
				single = (float)((double)int_1 * Math.Sqrt((double)(rectangle_0.Height * rectangle_0.Height + rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Width / 2.0));
				graphicsPath_0.AddLine(left + width / 2, (float)top + single, (float)(left + width) - int1, top + height / 2);
				graphicsPath_0.AddLine((float)(left + width) - int1, top + height / 2, left + width / 2, (float)(top + height) - single);
				graphicsPath_0.AddLine(left + width / 2, (float)(top + height) - single, (float)left + int1, top + height / 2);
				graphicsPath_0.AddLine((float)left + int1, top + height / 2, left + width / 2, (float)top + single);
				break;
			case DAS_ShapeStyle.SS_Left_Arrow:
				int1 = (float)((double)int_1 * Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) / ((double)rectangle_0.Height / 2.0));
				single = (float)((double)(int_1 * int0) / (Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) - (double)rectangle_0.Height / 2.0));
				graphicsPath_0.AddLine((float)left + int1, top + height / 2, left + int0 - int_1, (float)top + single);
				graphicsPath_0.AddLine(left + int0 - int_1, (float)top + single, left + int0 - int_1, top + (height - int0) / 2 + int_1);
				graphicsPath_0.AddLine(left + int0 - int_1, top + (height - int0) / 2 + int_1, left + width - int_1, top + (height - int0) / 2 + int_1);
				graphicsPath_0.AddLine(left + width - int_1, top + (height - int0) / 2 + int_1, left + width - int_1, top + (height + int0) / 2 - int_1);
				graphicsPath_0.AddLine(left + width - int_1, top + (height + int0) / 2 - int_1, left + int0 - int_1, top + (height + int0) / 2 - int_1);
				graphicsPath_0.AddLine(left + int0 - int_1, top + (height + int0) / 2 - int_1, left + int0 - int_1, (float)(top + height) - single);
				graphicsPath_0.AddLine(left + int0 - int_1, (float)(top + height) - single, (float)left + int1, top + height / 2);
				break;
			case DAS_ShapeStyle.SS_Right_Arrow:
				int1 = (float)((double)int_1 * Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) / ((double)rectangle_0.Height / 2.0));
				single = (float)((double)(int_1 * int0) / (Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Height * rectangle_0.Height) / 4.0) - (double)rectangle_0.Height / 2.0));
				graphicsPath_0.AddLine((float)(left + width) - int1, top + height / 2, left + width - int0 + int_1, (float)top + single);
				graphicsPath_0.AddLine(left + width - int0 + int_1, (float)top + single, left + width - int0 + int_1, top + (height - int0) / 2 + int_1);
				graphicsPath_0.AddLine(left + width - int0 + int_1, top + (height - int0) / 2 + int_1, left + int_1, top + (height - int0) / 2 + int_1);
				graphicsPath_0.AddLine(left + int_1, top + (height - int0) / 2 + int_1, left + int_1, top + (height + int0) / 2 - int_1);
				graphicsPath_0.AddLine(left + int_1, top + (height + int0) / 2 - int_1, left + width - int0 + int_1, top + (height + int0) / 2 - int_1);
				graphicsPath_0.AddLine(left + width - int0 + int_1, top + (height + int0) / 2 - int_1, left + width - int0 + int_1, (float)(top + height) - single);
				graphicsPath_0.AddLine(left + width - int0 + int_1, (float)(top + height) - single, (float)(left + width) - int1, top + height / 2);
				break;
			case DAS_ShapeStyle.SS_Top_Arrow:
				single = (float)((double)int_1 * Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Width / 2.0));
				int1 = (float)((double)(int_1 * int0) / (Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) - (double)rectangle_0.Width / 2.0));
				graphicsPath_0.AddLine(left + width / 2, (float)top + single, (float)left + int1, top + int0 - int_1);
				graphicsPath_0.AddLine((float)left + int1, top + int0 - int_1, left + (width - int0) / 2 + int_1, top + int0 - int_1);
				graphicsPath_0.AddLine(left + (width - int0) / 2 + int_1, top + int0 - int_1, left + (width - int0) / 2 + int_1, top + height - int_1);
				graphicsPath_0.AddLine(left + (width - int0) / 2 + int_1, top + height - int_1, left + (width + int0) / 2 - int_1, top + height - int_1);
				graphicsPath_0.AddLine(left + (width + int0) / 2 - int_1, top + height - int_1, left + (width + int0) / 2 - int_1, top + int0 - int_1);
				graphicsPath_0.AddLine(left + (width + int0) / 2 - int_1, top + int0 - int_1, (float)(left + width) - int1, top + int0 - int_1);
				graphicsPath_0.AddLine((float)(left + width) - int1, top + int0 - int_1, left + width / 2, (float)top + single);
				break;
			case DAS_ShapeStyle.SS_Bottom_Arrow:
				single = (float)((double)int_1 * Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) / ((double)rectangle_0.Width / 2.0));
				int1 = (float)((double)(int_1 * int0) / (Math.Sqrt((double)(int0 * int0) + (double)(rectangle_0.Width * rectangle_0.Width) / 4.0) - (double)rectangle_0.Width / 2.0));
				graphicsPath_0.AddLine(left + width / 2, (float)(top + height) - single, (float)left + int1, top + (height - int0) + int_1);
				graphicsPath_0.AddLine((float)left + int1, top + (height - int0) + int_1, left + (width - int0) / 2 + int_1, top + (height - int0) + int_1);
				graphicsPath_0.AddLine(left + (width - int0) / 2 + int_1, top + (height - int0) + int_1, left + (width - int0) / 2 + int_1, top + int_1);
				graphicsPath_0.AddLine(left + (width - int0) / 2 + int_1, top + int_1, left + (width + int0) / 2 - int_1, top + int_1);
				graphicsPath_0.AddLine(left + (width + int0) / 2 - int_1, top + int_1, left + (width + int0) / 2 - int_1, top + (height - int0) + int_1);
				graphicsPath_0.AddLine(left + (width + int0) / 2 - int_1, top + (height - int0) + int_1, (float)(left + width) - int1, top + (height - int0) + int_1);
				graphicsPath_0.AddLine((float)(left + width) - int1, top + (height - int0) + int_1, left + width / 2, (float)(top + height) - single);
				break;
			}
			graphicsPath_0.CloseFigure();
		}

		internal void method_7(Graphics graphics_0, Rectangle rectangle_0, GraphicsPath graphicsPath_0, Color color_0, int int_0, float float_0)
		{
			graphics_0.TranslateTransform(int_0, int_0);
			PathGradientBrush pathGradientBrush = new PathGradientBrush(graphicsPath_0);
			Blend blend2 = new Blend();
			blend2.Factors = new float[3];
			blend2.Positions = new float[3];
			Blend blend = blend2;
			blend.Factors[0] = 0f;
			blend.Positions[0] = 0f;
			blend.Factors[2] = 1f;
			blend.Positions[2] = 1f;
			blend.Factors[1] = float_0;
			blend.Positions[1] = 1f - float_0;
			pathGradientBrush.SurroundColors = new Color[1]
			{
				Color.Transparent
			};
			pathGradientBrush.Blend = blend;
			pathGradientBrush.CenterColor = color_0;
			PointF pointF2 = default(PointF);
			pointF2.X = (float)(rectangle_0.Width - 2 * int_0) / (float)rectangle_0.Width;
			pointF2.Y = (float)(rectangle_0.Height - 2 * int_0) / (float)rectangle_0.Height;
			PointF pointF = pathGradientBrush.FocusScales = pointF2;
			graphics_0.FillPath(pathGradientBrush, graphicsPath_0);
			pathGradientBrush.Dispose();
			graphics_0.ResetTransform();
		}
	}
}
