namespace HslControls.HMI
{
	public enum DAS_ProgressHeadStyle
	{
		PHS_None,
		PHS_Round,
		PHS_Triangle,
		PHS_Arrow,
		PHS_Image
	}
}
