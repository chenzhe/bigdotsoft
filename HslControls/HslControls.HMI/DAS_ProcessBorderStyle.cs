namespace HslControls.HMI
{
	public enum DAS_ProcessBorderStyle
	{
		MBS_None,
		MBS_Flat,
		MBS_Raised,
		MBS_Sunken
	}
}
