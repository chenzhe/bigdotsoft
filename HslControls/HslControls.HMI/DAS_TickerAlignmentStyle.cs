namespace HslControls.HMI
{
	public enum DAS_TickerAlignmentStyle
	{
		TAS_Text_Left,
		TAS_Text_Right,
		TAS_Text_LeftRight,
		TAS_Center
	}
}
