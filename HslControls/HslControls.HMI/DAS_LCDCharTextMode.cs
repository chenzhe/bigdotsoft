namespace HslControls.HMI
{
	public enum DAS_LCDCharTextMode
	{
		LCTM_SingleText,
		LCTM_TwoText,
		LCTM_MultiLineConfiguration
	}
}
