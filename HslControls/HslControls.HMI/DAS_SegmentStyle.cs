namespace HslControls.HMI
{
	public enum DAS_SegmentStyle
	{
		SS_Ladder,
		SS_TShape,
		SS_Rhomb
	}
}
