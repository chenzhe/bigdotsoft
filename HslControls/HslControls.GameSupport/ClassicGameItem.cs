using System.Drawing;

namespace HslControls.GameSupport
{
	/// <summary>
	/// 游戏内容的最基本的单元
	/// </summary>
	public class ClassicGameItem
	{
		/// <summary>
		/// 指代不同的东西
		/// </summary>
		public int ItemCode
		{
			get;
			set;
		} = 1;


		/// <summary>
		/// 当前元素的背景色
		/// </summary>
		public Color ItemColor
		{
			get;
			set;
		}

		/// <summary>
		/// 当前元素是否有值
		/// </summary>
		public bool HasItem
		{
			get;
			set;
		} = false;


		/// <summary>
		///
		/// </summary>
		public Point Position
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public ClassicGameItem()
		{
			ItemColor = Color.Black;
			Position = new Point(0, 0);
		}
	}
}
