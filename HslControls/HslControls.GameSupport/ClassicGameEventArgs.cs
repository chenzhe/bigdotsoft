using System;
using System.Drawing;

namespace HslControls.GameSupport
{
	public class ClassicGameEventArgs : EventArgs
	{
		public Bitmap RenderBitmap
		{
			get;
			set;
		}
	}
}
