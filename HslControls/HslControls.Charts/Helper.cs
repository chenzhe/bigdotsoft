using System.Drawing;
using System.Drawing.Drawing2D;

namespace HslControls.Charts
{
	/// <summary>
	/// 帮助类，包含一些静态的GDI+绘图方法。
	/// </summary>
	public class Helper
	{
		/// <summary>
		/// 尺寸转换，计算旋转后的尺寸。
		/// </summary>
		/// <param name="size"></param>
		/// <param name="angle"></param>
		/// <returns></returns>
		public static SizeF ConvertSize(SizeF size, float angle)
		{
			Matrix matrix = new Matrix();
			matrix.Rotate(angle);
			PointF[] pts = new PointF[4];
			pts[0].X = (0f - size.Width) / 2f;
			pts[0].Y = (0f - size.Height) / 2f;
			pts[1].X = (0f - size.Width) / 2f;
			pts[1].Y = size.Height / 2f;
			pts[2].X = size.Width / 2f;
			pts[2].Y = size.Height / 2f;
			pts[3].X = size.Width / 2f;
			pts[3].Y = (0f - size.Height) / 2f;
			matrix.TransformPoints(pts);
			float left = float.MaxValue;
			float right = float.MinValue;
			float top = float.MaxValue;
			float bottom = float.MinValue;
			PointF[] array = pts;
			for (int i = 0; i < array.Length; i++)
			{
				PointF pt = array[i];
				if (pt.X < left)
				{
					left = pt.X;
				}
				if (pt.X > right)
				{
					right = pt.X;
				}
				if (pt.Y < top)
				{
					top = pt.Y;
				}
				if (pt.Y > bottom)
				{
					bottom = pt.Y;
				}
			}
			return new SizeF(right - left, bottom - top);
		}

		/// <summary>
		/// 绘制旋转文本
		/// </summary>
		/// <param name="g"></param>
		/// <param name="s"></param>
		/// <param name="font"></param>
		/// <param name="brush"></param>
		/// <param name="point"></param>
		/// <param name="format"></param>
		/// <param name="angle"></param>
		public static void DrawString(Graphics g, string s, Font font, Brush brush, PointF point, StringFormat format, float angle)
		{
			Matrix mtxSave = g.Transform;
			Matrix mtxRotate = g.Transform;
			mtxRotate.RotateAt(angle, point);
			g.Transform = mtxRotate;
			g.DrawString(s, font, brush, point, format);
			g.Transform = mtxSave;
		}
	}
}
