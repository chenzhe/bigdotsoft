using System.Drawing;

namespace HslControls.Advanced
{
	/// <summary>
	/// 城市信息
	/// </summary>
	public class HslCity
	{
		/// <summary>
		/// 城市的名称
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// 城市等级，0：首都  1：省会（直辖市）  2：地级市
		/// </summary>
		public int Degree
		{
			get;
			set;
		}

		/// <summary>
		/// 城市的位置
		/// </summary>
		public Point Location
		{
			get;
			set;
		}

		/// <summary>
		/// 0是右下角，1是右上角
		/// </summary>
		public int ShowLocation
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 显示的大小
		/// </summary>
		public int Size
		{
			get;
			set;
		}

		/// <summary>
		/// 标记的大小信息
		/// </summary>
		public int MarkSize
		{
			get;
			set;
		} = -1;


		/// <summary>
		/// 标记的颜色信息
		/// </summary>
		public Brush MarkBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslCity()
		{
			Size = 2;
		}
	}
}
