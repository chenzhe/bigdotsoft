using System;
using System.Drawing;

namespace HslControls.Advanced
{
	/// <summary>
	/// 图形元素的基础
	/// </summary>
	public class HslFigureBase : IHslFigure
	{
		private static Random random = new Random();

		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.FigureId" />
		public string FigureId
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.FigureType" />
		public string FigureType
		{
			get;
			set;
		} = "HslFigureBase";


		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.Name" />
		public string Name
		{
			get;
			set;
		} = "HslFigureBase";


		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.Location" />
		public PointF Location
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.Width" />
		public float Width
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.Height" />
		public float Height
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslControls.Advanced.IHslFigure.Selected" />
		public bool Selected
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslFigureBase()
		{
			FigureId = GetFigureUniqueId();
		}

		/// <inheritdoc cref="M:HslControls.Advanced.IHslFigure.GetMiddleDatum" />
		public virtual int GetMiddleDatum()
		{
			return (int)(Height / 2f);
		}

		/// <inheritdoc cref="M:HslControls.Advanced.IHslFigure.IsMouseIn(System.Drawing.PointF)" />
		public virtual bool IsMouseIn(PointF cursor)
		{
			return false;
		}

		/// <inheritdoc cref="M:HslControls.Advanced.IHslFigure.DrawCadFigure(System.Drawing.Graphics,System.Drawing.Color,System.Drawing.Color,System.Drawing.Font,System.Single)" />
		public virtual void DrawCadFigure(Graphics g, Color themeColor, Color backColor, Font font, float scale)
		{
		}

		private static string GetFigureUniqueId()
		{
			return Guid.NewGuid().ToString("N") + random.Next(1000, 10000);
		}
	}
}
