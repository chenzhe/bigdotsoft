using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 水箱的控件
	/// </summary>
	[Description("一个水箱控件，可以设置水箱的高度，设置颜色信息")]
	public class HslWaterBox : UserControl
	{
		private float edgeWidth = 10f;

		private Color edgeColor = Color.FromArgb(253, 233, 208);

		private Brush brushEdge = new SolidBrush(Color.FromArgb(253, 233, 208));

		private Color boderColor = Color.FromArgb(234, 205, 152);

		private Pen penBorder = new Pen(Color.FromArgb(234, 205, 152));

		private Color waterColor = Color.FromArgb(207, 235, 246);

		private Brush brushWater = new SolidBrush(Color.FromArgb(207, 235, 246));

		private float waterValue = 96f;

		private StringFormat sf = null;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置水池的高度信息，值为百分比的内容。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置水池的高度信息，值为百分比的内容。")]
		[Category("HslControls")]
		[DefaultValue(96f)]
		public float Value
		{
			get
			{
				return waterValue;
			}
			set
			{
				waterValue = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置水池边框的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置水池边框的颜色。")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[234, 205, 152]")]
		public Color BorderColor
		{
			get
			{
				return boderColor;
			}
			set
			{
				boderColor = value;
				penBorder?.Dispose();
				penBorder = new Pen(boderColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置水池边界的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置水池边界的颜色。")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[253, 233, 208]")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				brushEdge?.Dispose();
				brushEdge = new SolidBrush(edgeColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置水池的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置水池的颜色。")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[207,235, 246]")]
		public Color WaterColor
		{
			get
			{
				return waterColor;
			}
			set
			{
				waterColor = value;
				brushWater?.Dispose();
				brushWater = new SolidBrush(waterColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置水池边界的宽度
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置水池边界的宽度。")]
		[Category("HslControls")]
		[DefaultValue(10f)]
		public float EdgeWidth
		{
			get
			{
				return edgeWidth;
			}
			set
			{
				edgeWidth = value;
				if (edgeWidth < 0f)
				{
					edgeWidth = 0f;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslWaterBox()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			if (height < edgeWidth + 2f)
			{
				return;
			}
			g.TranslateTransform(width / 2f, 0f);
			float waterTmp = waterValue;
			if (waterTmp > 100f)
			{
				waterTmp = 100f;
			}
			if (waterTmp < 0f)
			{
				waterTmp = 0f;
			}
			float location_y = (height - edgeWidth - 1f) * waterTmp / 100f;
			g.FillRectangle(rect: new RectangleF((0f - width) / 2f, height - location_y - edgeWidth - 1f, width, location_y), brush: brushWater);
			g.TranslateTransform((0f - width) / 2f, 0f);
			PointF[] points = new PointF[9]
			{
				new PointF(0f, 0f),
				new PointF(edgeWidth, 0f),
				new PointF(edgeWidth, height - edgeWidth - 1f),
				new PointF(width - edgeWidth, height - edgeWidth - 1f),
				new PointF(width - edgeWidth, 0f),
				new PointF(width - 1f, 0f),
				new PointF(width - 1f, height - 1f),
				new PointF(0f, height - 1f),
				new PointF(0f, 0f)
			};
			g.FillPolygon(brushEdge, points);
			g.DrawPolygon(penBorder, points);
			if (!string.IsNullOrEmpty(Text))
			{
				using Brush fontBrush = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, fontBrush, new RectangleF(0f, 0f, width, height), sf);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslWaterBox";
			base.Size = new System.Drawing.Size(134, 119);
			ResumeLayout(false);
		}
	}
}
