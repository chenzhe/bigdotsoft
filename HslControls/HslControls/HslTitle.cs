using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	[Description("一个标题看板控件，用来显示重点的标题信息，两侧可以设置其他的数据信息")]
	public class HslTitle : UserControl
	{
		private StringFormat sf = null;

		private Color edgeColor = Color.LightGray;

		private Color leftRightEdgeColor = Color.DimGray;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		private float widthPercent = 0.5f;

		private Font fontRight = null;

		private Font fontLeft = null;

		private string textRight = "HslControl";

		private string textLeft = "00:00:00";

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置中间信息的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置中间信息的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LightGray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置两侧的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置两侧的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color LeftRightEdgeColor
		{
			get
			{
				return leftRightEdgeColor;
			}
			set
			{
				leftRightEdgeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置标题控件是否是横向的还是纵向的
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置标题控件是否是横向的还是纵向的")]
		[Category("HslControls")]
		[DefaultValue(typeof(HslDirectionStyle), "Horizontal")]
		public HslDirectionStyle TitleStyle
		{
			get
			{
				return hslValvesStyle;
			}
			set
			{
				hslValvesStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置右侧文件的字体信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置右侧文本的字体信息")]
		[Category("HslControls")]
		public Font FontRight
		{
			get
			{
				return fontRight;
			}
			set
			{
				fontRight = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置左侧文件的字体信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置左侧文本的字体信息")]
		[Category("HslControls")]
		public Font FontLeft
		{
			get
			{
				return fontLeft;
			}
			set
			{
				fontLeft = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置右侧文本的信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置右侧文本的信息")]
		[Category("HslControls")]
		[DefaultValue("HslControl")]
		public string TextRight
		{
			get
			{
				return textRight;
			}
			set
			{
				textRight = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置左侧文本的信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置左侧文本的信息")]
		[Category("HslControls")]
		[DefaultValue("00:00:00")]
		public string TextLeft
		{
			get
			{
				return textLeft;
			}
			set
			{
				textLeft = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置中间文本的宽度百分比，0.5表示50%
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置中间文本的宽度百分比，0.5表示50%")]
		[Category("HslControls")]
		[DefaultValue(0.5f)]
		public float WidthPercent
		{
			get
			{
				return widthPercent;
			}
			set
			{
				widthPercent = value;
				Invalidate();
			}
		}

		public HslTitle()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.5f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				leftRightEdgeColor,
				HslHelper.GetColorLight(leftRightEdgeColor),
				leftRightEdgeColor
			};
			LinearGradientBrush brush = new LinearGradientBrush(new PointF(0f, 0f), new PointF(0f, height), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, 0f, 0f, width - 1f, height - 1f);
			colorBlend.Colors = new Color[3]
			{
				edgeColor,
				HslHelper.GetColorLight(edgeColor),
				edgeColor
			};
			brush.InterpolationColors = colorBlend;
			if (width * 0.1f < height)
			{
				g.FillPolygon(brush, new PointF[4]
				{
					new PointF(width * (0.5f - widthPercent / 2f), 0f),
					new PointF(width * (0.5f + widthPercent / 2f), 0f),
					new PointF(width * (0.4f + widthPercent / 2f), height - 1f),
					new PointF(width * (0.6f - widthPercent / 2f), height - 1f)
				});
			}
			else
			{
				g.FillPolygon(brush, new PointF[4]
				{
					new PointF(width * (0.5f - widthPercent / 2f), 0f),
					new PointF(width * (0.5f + widthPercent / 2f), 0f),
					new PointF(width * (0.5f + widthPercent / 2f) - height, height - 1f),
					new PointF(width * (0.5f - widthPercent / 2f) + height, height - 1f)
				});
			}
			brush.Dispose();
			brush = new LinearGradientBrush(new PointF(0f, height * 0.5f - (float)Font.Height), new PointF(0f, height * 0.5f + (float)Font.Height), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			colorBlend.Colors = new Color[3]
			{
				leftRightEdgeColor,
				Color.Yellow,
				leftRightEdgeColor
			};
			brush.InterpolationColors = colorBlend;
			using (Brush foreBrush = new SolidBrush(ForeColor))
			{
				g.DrawString(Text, Font, foreBrush, new RectangleF(0f, 0f, width - 1f, height - 1f), sf);
			}
			if (!string.IsNullOrEmpty(textRight))
			{
				g.DrawString(textRight, fontRight ?? Font, brush, new RectangleF(width * (0.5f + widthPercent / 2f), 0f, width * (0.5f - widthPercent / 2f), height - 1f), sf);
			}
			colorBlend.Colors = new Color[3]
			{
				leftRightEdgeColor,
				Color.Cyan,
				leftRightEdgeColor
			};
			brush.InterpolationColors = colorBlend;
			if (!string.IsNullOrEmpty(textLeft))
			{
				g.DrawString(textLeft, fontLeft ?? Font, brush, new RectangleF(0f, 0f, width * (0.5f - widthPercent / 2f), height - 1f), sf);
			}
			brush.Dispose();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslTitle";
			base.Size = new System.Drawing.Size(771, 65);
			ResumeLayout(false);
		}
	}
}
