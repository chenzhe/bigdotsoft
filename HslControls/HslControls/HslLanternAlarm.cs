using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个用于报警的信号灯
	/// </summary>
	[Description("报警灯控件，支持闪烁，颜色设置")]
	public class HslLanternAlarm : UserControl
	{
		private Color bottomColor = Color.DimGray;

		private Brush bottomBrush = new SolidBrush(Color.DimGray);

		private Color lightColor = Color.Tomato;

		private Brush lightBrush = new SolidBrush(Color.Tomato);

		private Color alarmColor = Color.Tomato;

		private Color normalColor = Color.LightGray;

		private bool isAlarm = false;

		private bool alarmStatus = false;

		private Timer timer = new Timer();

		private StringFormat sf = null;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置底座的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置底座的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color BottomColor
		{
			get
			{
				return bottomColor;
			}
			set
			{
				bottomColor = value;
				bottomBrush.Dispose();
				bottomBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置报警灯的颜色，可以设置为任意的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置报警灯的颜色，可以设置为任意的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color LightColor
		{
			get
			{
				return lightColor;
			}
			set
			{
				lightColor = value;
				lightBrush.Dispose();
				lightBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置报警状态下的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置报警状态下的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color AlarmColor
		{
			get
			{
				return alarmColor;
			}
			set
			{
				alarmColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置正常状态的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置正常状态的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LightGray")]
		public Color NormalColor
		{
			get
			{
				return normalColor;
			}
			set
			{
				normalColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 一旦设置本属性，启动信号灯闪烁
		/// </summary>
		[Browsable(true)]
		[Description("一旦设置本属性，启动信号灯闪烁")]
		[Category("HslControls")]
		[DefaultValue(false)]
		public bool IsAlarm
		{
			get
			{
				return isAlarm;
			}
			set
			{
				isAlarm = value;
				if (isAlarm)
				{
					timer.Start();
				}
				else
				{
					timer.Stop();
				}
			}
		}

		/// <summary>
		/// 获取或设置报警灯闪烁的频率，单位是毫秒，默认为500
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置报警灯闪烁的频率，单位是毫秒，默认为500")]
		[Category("HslControls")]
		[DefaultValue(500)]
		public int AlarmFlushTime
		{
			get
			{
				return timer.Interval;
			}
			set
			{
				timer.Interval = value;
			}
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslLanternAlarm()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			timer.Interval = 500;
			timer.Tick += Timer_Tick;
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (isAlarm)
			{
				if (alarmStatus)
				{
					LightColor = AlarmColor;
				}
				else
				{
					LightColor = NormalColor;
				}
				alarmStatus = !alarmStatus;
			}
			else
			{
				LightColor = NormalColor;
			}
		}

		/// <summary>
		/// 重绘控件的模型
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				Graphics g = e.Graphics;
				int heightBottom = 8;
				g.FillRectangle(bottomBrush, new Rectangle(0, base.Height - 1 - heightBottom, base.Width, heightBottom));
				g.FillRectangle(bottomBrush, new Rectangle(base.Width / 20, base.Height - 1 - heightBottom * 2, base.Width - base.Width / 10, heightBottom));
				int radis = base.Width * 4 / 10;
				if (base.Height - radis - 2 * heightBottom > 0)
				{
					g.FillRectangle(lightBrush, base.Width / 10, radis, base.Width * 8 / 10, base.Height - radis - 2 * heightBottom);
				}
				g.FillPie(lightBrush, base.Width / 10, 1, base.Width * 8 / 10, radis * 2, 180, 180);
				base.OnPaint(e);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslLanternAlarm";
			base.Size = new System.Drawing.Size(99, 129);
			ResumeLayout(false);
		}
	}
}
