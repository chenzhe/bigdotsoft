using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 进度条控件，采用标准的线段绘制，支持横向和纵向的两种模式，支持动画效果
	/// </summary>
	[Description("进度条控件，采用标准的线段绘制，支持横向和纵向的两种模式，支持动画效果")]
	public class HslProgressLine : UserControl
	{
		private StringFormat sf = null;

		private Color backColor = Color.LightGray;

		private Color progressColor = Color.Tomato;

		private int max = 100;

		private int m_value = 50;

		private int m_actual = 50;

		private int m_speed = 1;

		private bool useAnimation = false;

		private int m_version = 0;

		private HslProgressStyle m_progressStyle = HslProgressStyle.Horizontal;

		private bool isTextRender = true;

		private Action m_UpdateAction;

		private string textRenderFormat = string.Empty;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置进度条的背景色
		/// </summary>
		[Description("获取或设置进度条的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LightGray")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Browsable(true)]
		public Color ProgressBackColor
		{
			get
			{
				return backColor;
			}
			set
			{
				backColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度的颜色
		/// </summary>
		[Description("获取或设置进度条的前景色")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color ProgressColor
		{
			get
			{
				return progressColor;
			}
			set
			{
				progressColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度条的最大值，默认为100
		/// </summary>
		[Description("获取或设置进度条的最大值，默认为100")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(100)]
		public int Max
		{
			get
			{
				return max;
			}
			set
			{
				if (value > 1)
				{
					max = value;
				}
				if (m_value > max)
				{
					m_value = max;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 当前进度条的值，不能大于最大值或小于0
		/// </summary>
		[Description("获取或设置当前进度条的值")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(50)]
		public int Value
		{
			get
			{
				return m_value;
			}
			set
			{
				if (value >= 0 && value <= max && value != m_value)
				{
					m_value = value;
					if (UseAnimation)
					{
						int version = Interlocked.Increment(ref m_version);
						ThreadPool.QueueUserWorkItem(ThreadPoolUpdateProgress, version);
					}
					else
					{
						m_actual = value;
						Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 进度条的样式
		/// </summary>
		[Description("获取或设置进度条的样式")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(HslProgressStyle), "Horizontal")]
		public HslProgressStyle ProgressStyle
		{
			get
			{
				return m_progressStyle;
			}
			set
			{
				m_progressStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置进度变更的速度
		/// </summary>
		[Description("获取或设置进度条的变化进度")]
		[Category("Appearance")]
		[Browsable(true)]
		[DefaultValue(1)]
		public int ValueChangeSpeed
		{
			get
			{
				return m_speed;
			}
			set
			{
				if (value >= 1)
				{
					m_speed = value;
				}
			}
		}

		/// <summary>
		/// 获取或设置进度条变化的时候是否采用动画效果
		/// </summary>
		[Description("获取或设置进度条变化的时候是否采用动画效果")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(false)]
		public bool UseAnimation
		{
			get
			{
				return useAnimation;
			}
			set
			{
				useAnimation = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 是否显示进度
		/// </summary>
		[Description("获取或设置是否显示进度文本")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(true)]
		public bool IsTextRender
		{
			get
			{
				return isTextRender;
			}
			set
			{
				isTextRender = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义的格式化文本信息
		/// </summary>
		[Description("获取或设置自定义的格式化文本信息")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue("")]
		public string TextRenderFormat
		{
			get
			{
				return textRenderFormat;
			}
			set
			{
				textRenderFormat = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的进度条控件
		/// </summary>
		public HslProgressLine()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			m_UpdateAction = UpdateRender;
		}

		/// <summary>
		/// 重绘控件的外观
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf() || base.Width < 3 || base.Height < 2)
			{
				return;
			}
			int circle_width = Math.Min(base.Width, base.Height);
			try
			{
				Graphics g = e.Graphics;
				Rectangle rectangle = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				switch (m_progressStyle)
				{
				case HslProgressStyle.Vertical:
				{
					using (Pen pen3 = new Pen(backColor, base.Width - 2))
					{
						pen3.StartCap = LineCap.Round;
						pen3.EndCap = LineCap.Round;
						g.DrawLine(pen3, base.Width / 2, base.Height - 1 - base.Width / 2, base.Width / 2, base.Width / 2);
					}
					using (Pen pen4 = new Pen(progressColor, base.Width - 2))
					{
						pen4.StartCap = LineCap.Round;
						pen4.EndCap = LineCap.Round;
						int height = (int)((long)m_actual * (long)(base.Height - 2 - base.Width) / max);
						g.DrawLine(pen4, base.Width / 2, base.Height - 1 - base.Width / 2, base.Width / 2, base.Height - 1 - height - base.Width / 2);
					}
					break;
				}
				case HslProgressStyle.Horizontal:
				{
					using (Pen backPen = new Pen(backColor, base.Height - 2))
					{
						backPen.StartCap = LineCap.Round;
						backPen.EndCap = LineCap.Round;
						g.DrawLine(backPen, base.Height / 2, base.Height / 2, base.Width - 1 - base.Height / 2, base.Height / 2);
					}
					using (Pen forePen = new Pen(progressColor, base.Height - 2))
					{
						forePen.StartCap = LineCap.Round;
						forePen.EndCap = LineCap.Round;
						int width = (int)((long)m_actual * (long)(base.Width - 2 - base.Height) / max);
						g.DrawLine(forePen, base.Height / 2, base.Height / 2, width + 1 + base.Height / 2, base.Height / 2);
					}
					break;
				}
				case HslProgressStyle.Circular:
				{
					int lineWidth = circle_width / 10;
					using (Pen pen = new Pen(backColor, lineWidth))
					{
						pen.StartCap = LineCap.Round;
						pen.EndCap = LineCap.Round;
						g.DrawArc(pen, lineWidth, lineWidth, circle_width - 1 - 2 * lineWidth, circle_width - 1 - 2 * lineWidth, -90, 360);
					}
					using (Pen pen2 = new Pen(progressColor, lineWidth))
					{
						pen2.StartCap = LineCap.Round;
						pen2.EndCap = LineCap.Round;
						int angle = (int)((long)m_actual * 360L / max);
						g.DrawArc(pen2, lineWidth, lineWidth, circle_width - 1 - 2 * lineWidth, circle_width - 1 - 2 * lineWidth, -90, angle);
					}
					break;
				}
				}
				rectangle = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
				if (isTextRender)
				{
					string str = string.IsNullOrEmpty(textRenderFormat) ? ((long)m_actual * 100L / max + "%") : string.Format(textRenderFormat, Value, Max);
					using Brush brush = new SolidBrush(ForeColor);
					if (m_progressStyle == HslProgressStyle.Circular)
					{
						rectangle = new Rectangle(0, 0, circle_width - 1, circle_width - 1);
					}
					g.DrawString(str, Font, brush, rectangle, sf);
				}
			}
			catch (Exception)
			{
			}
			base.OnPaint(e);
		}

		private void ThreadPoolUpdateProgress(object obj)
		{
			try
			{
				int version = (int)obj;
				if (m_speed < 1)
				{
					m_speed = 1;
				}
				while (m_actual != m_value)
				{
					Thread.Sleep(17);
					if (version != m_version)
					{
						break;
					}
					int newActual = 0;
					if (m_actual > m_value)
					{
						int offect2 = m_actual - m_value;
						if (offect2 > m_speed)
						{
							offect2 = m_speed;
						}
						newActual = m_actual - offect2;
					}
					else
					{
						int offect = m_value - m_actual;
						if (offect > m_speed)
						{
							offect = m_speed;
						}
						newActual = m_actual + offect;
					}
					m_actual = newActual;
					if (version == m_version)
					{
						if (base.IsHandleCreated)
						{
							Invoke(m_UpdateAction);
						}
						continue;
					}
					break;
				}
			}
			catch (Exception)
			{
			}
		}

		private void UpdateRender()
		{
			Invalidate();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslProgressLine";
			base.Size = new System.Drawing.Size(390, 16);
			ResumeLayout(false);
		}
	}
}
