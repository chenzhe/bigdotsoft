using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个电池控件，用来展示电池的多少的
	/// </summary>
	[Description("一个电池控件，类似进度条，可以设置颜色，设置进度条的格式化操作")]
	public class HslBattery : UserControl
	{
		private StringFormat sf = null;

		private Color batteryBackEdgeColor = Color.FromArgb(142, 196, 216);

		private Brush batteryBackBrush = new SolidBrush(Color.FromArgb(142, 196, 216));

		private Pen batteryBackPen = new Pen(Color.FromArgb(142, 196, 216));

		private Color batteryForeEdgeColor = Color.Gray;

		private Pen edgePen = new Pen(Color.Gray, 1f);

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		private Color color_1 = Color.Green;

		private Color color_2 = Color.LimeGreen;

		private Color color_3 = Color.Orange;

		private Color color_4 = Color.Tomato;

		private Color color_5 = Color.Red;

		private float value_max = 100f;

		private float value_min = 0f;

		private float value_now = 0f;

		private float value_paint = 0f;

		private float value_color1 = 0.85f;

		private float value_color2 = 0.6f;

		private float value_color3 = 0.4f;

		private float value_color4 = 0.15f;

		private string stringFormat = "{0} %";

		private bool isTextRender = false;

		private bool isUseAnimation = true;

		private Action m_UpdateAction;

		private int m_version = 0;

		private object hybirdLock;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置电池的背景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的背景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[142, 196, 216]")]
		public Color BatteryBackColor
		{
			get
			{
				return batteryBackEdgeColor;
			}
			set
			{
				batteryBackEdgeColor = value;
				batteryBackBrush.Dispose();
				batteryBackBrush = new SolidBrush(value);
				batteryBackPen.Dispose();
				batteryBackPen = new Pen(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前的数值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前的数值")]
		[Category("HslControls")]
		[DefaultValue(0f)]
		public float Value
		{
			get
			{
				return value_now;
			}
			set
			{
				if (value != value_now)
				{
					value_now = value;
					if (isUseAnimation)
					{
						int version = Interlocked.Increment(ref m_version);
						ThreadPool.QueueUserWorkItem(ThreadPoolUpdateProgress, version);
					}
					else
					{
						value_paint = value;
						Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 获取或设置当前数值的最大值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前数值的最大值")]
		[Category("HslControls")]
		[DefaultValue(100f)]
		public float ValueMax
		{
			get
			{
				return value_max;
			}
			set
			{
				value_max = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前数值的最小值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前数值的最小值")]
		[Category("HslControls")]
		[DefaultValue(0f)]
		public float ValueMin
		{
			get
			{
				return value_min;
			}
			set
			{
				value_min = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池控件是否是横向的还是纵向的
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池控件是否是横向的还是纵向的")]
		[Category("HslControls")]
		[DefaultValue(typeof(HslDirectionStyle), "Horizontal")]
		public HslDirectionStyle BatteryStyle
		{
			get
			{
				return hslValvesStyle;
			}
			set
			{
				hslValvesStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置是否显示文本提示信息的文本
		/// </summary>
		[Description("获取或设置是否显示文本提示信息的文本")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(false)]
		public bool IsTextRender
		{
			get
			{
				return isTextRender;
			}
			set
			{
				isTextRender = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置显示的文本的信息内容
		/// </summary>
		[Description("获取或设置显示的文本的信息内容")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue("{0} %")]
		public string StringFormat
		{
			get
			{
				return stringFormat;
			}
			set
			{
				stringFormat = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第一区间的前景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第一区间的前景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Green")]
		public Color Color1
		{
			get
			{
				return color_1;
			}
			set
			{
				color_1 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第二区间的前景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第二区间的前景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LimeGreen")]
		public Color Color2
		{
			get
			{
				return color_2;
			}
			set
			{
				color_2 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第三区间的前景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第三区间的前景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Orange")]
		public Color Color3
		{
			get
			{
				return color_3;
			}
			set
			{
				color_3 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第四区间的前景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第四区间的前景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color Color4
		{
			get
			{
				return color_4;
			}
			set
			{
				color_4 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第五区间的前景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第五区间的前景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Red")]
		public Color Color5
		{
			get
			{
				return color_5;
			}
			set
			{
				color_5 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第一区间和第二区间的分割值的百分比，默认0.85
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第一区间和第二区间的分割值的百分比，默认0.85")]
		[Category("HslControls")]
		[DefaultValue(0.85f)]
		public float Separatrix1
		{
			get
			{
				return value_color1;
			}
			set
			{
				value_color1 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第二区间和第三区间的分割值的百分比，默认0.60
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第二区间和第三区间的分割值的百分比，默认0.60")]
		[Category("HslControls")]
		[DefaultValue(0.6f)]
		public float Separatrix2
		{
			get
			{
				return value_color2;
			}
			set
			{
				value_color2 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第三区间和第四区间的分割值的百分比，默认0.40
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第三区间和第四区间的分割值的百分比，默认0.40")]
		[Category("HslControls")]
		[DefaultValue(0.4f)]
		public float Separatrix3
		{
			get
			{
				return value_color3;
			}
			set
			{
				value_color3 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电池的第四区间和第五区间的分割值的百分比，默认0.15
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置电池的第四区间和第五区间的分割值的百分比，默认0.15")]
		[Category("HslControls")]
		[DefaultValue(0.15f)]
		public float Separatrix4
		{
			get
			{
				return value_color4;
			}
			set
			{
				value_color4 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前的控件是否启用动画信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前的控件是否启用动画信息")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool UseAnimation
		{
			get
			{
				return isUseAnimation;
			}
			set
			{
				isUseAnimation = value;
			}
		}

		/// <summary>
		/// 实例化一个默认的电池控件对象
		/// </summary>
		public HslBattery()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			hybirdLock = new object();
			m_UpdateAction = base.Invalidate;
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			RectangleF rectangle = default(RectangleF);
			if (hslValvesStyle == HslDirectionStyle.Horizontal)
			{
				rectangle = new RectangleF(0f, (float)base.Height * 0.1f, base.Width, (float)base.Height * 0.9f);
				PaintMain(g, base.Width, base.Height);
			}
			else
			{
				rectangle = new RectangleF(0f, 0f, (float)base.Width * 0.9f, base.Height);
				g.TranslateTransform(base.Width, 0f);
				g.RotateTransform(90f);
				PaintMain(g, base.Height, base.Width);
				g.ResetTransform();
			}
			if (isTextRender)
			{
				using Brush foreBrush = new SolidBrush(ForeColor);
				g.DrawString(string.Format(stringFormat, value_now), Font, foreBrush, rectangle, sf);
			}
			base.OnPaint(e);
		}

		/// <summary>
		/// 本控件的主要绘制方法
		/// </summary>
		/// <param name="g">绘图对象</param>
		/// <param name="width">宽度</param>
		/// <param name="height">高度</param>
		public void PaintMain(Graphics g, float width, float height)
		{
			g.TranslateTransform(width / 2f, 0f);
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.75f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				batteryBackEdgeColor,
				Color.WhiteSmoke,
				batteryBackEdgeColor
			};
			LinearGradientBrush brush = new LinearGradientBrush(new PointF((0f - width) / 2f, 0f), new PointF(width / 2f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillEllipse(brush, (0f - width) * 0.5f - 1f, height * 0.93f, width + 1f, height * 0.06f);
			g.DrawEllipse(batteryBackPen, (0f - width) * 0.5f - 1f, height * 0.93f, width + 1f, height * 0.06f);
			g.FillRectangle(brush, (0f - width) * 0.5f - 1f, height * 0.1f, width + 1f, height * 0.86f);
			g.FillEllipse(batteryBackBrush, (0f - width) * 0.5f - 1f, height * 0.07f, width + 1f, height * 0.06f);
			brush.Dispose();
			brush = new LinearGradientBrush(new PointF((0f - width) * 0.15f, 0f), new PointF(width * 0.15f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillEllipse(brush, (0f - width) * 0.15f, height * 0.085f, width * 0.3f, height * 0.02f);
			g.FillRectangle(brush, (0f - width) * 0.15f, height * 0.03f, width * 0.3f, height * 0.065f);
			g.FillEllipse(batteryBackBrush, (0f - width) * 0.15f, height * 0.02f, width * 0.3f, height * 0.02f);
			brush.Dispose();
			Color colorBattery = getColorFromValue(value_now);
			colorBlend.Colors = new Color[3]
			{
				colorBattery,
				Color.WhiteSmoke,
				colorBattery
			};
			float value_Y = height * 0.86f - (value_paint - value_min) / (value_max - value_min) * height * 0.86f + height * 0.1f;
			if (value_Y < height * 0.1f)
			{
				value_Y = height * 0.1f;
			}
			if (value_Y > height * 0.96f)
			{
				value_Y = height * 0.96f;
			}
			brush = new LinearGradientBrush(new PointF((0f - width) / 2f, 0f), new PointF(width / 2f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillEllipse(brush, (0f - width) * 0.5f - 1f, height * 0.93f, width + 1f, height * 0.06f);
			using (Pen batteryForePen = new Pen(colorBattery))
			{
				g.DrawEllipse(batteryForePen, (0f - width) * 0.5f - 1f, height * 0.93f, width + 1f, height * 0.06f);
			}
			g.FillRectangle(brush, (0f - width) * 0.5f - 1f, value_Y, width + 1f, height * 0.86f - (value_Y - height * 0.1f));
			using (Brush batteryForeBrush = new SolidBrush(colorBattery))
			{
				g.FillEllipse(batteryForeBrush, (0f - width) * 0.5f - 1f, value_Y - height * 0.03f, width + 1f, height * 0.06f);
			}
			brush.Dispose();
			g.TranslateTransform((0f - width) / 2f, 0f);
		}

		private Color getColorFromValue(float value)
		{
			if (value > (value_max - value_min) * value_color1 + value_min)
			{
				return color_1;
			}
			if (value > (value_max - value_min) * value_color2 + value_min)
			{
				return color_2;
			}
			if (value > (value_max - value_min) * value_color3 + value_min)
			{
				return color_3;
			}
			if (value > (value_max - value_min) * value_color4 + value_min)
			{
				return color_4;
			}
			return color_5;
		}

		private void ThreadPoolUpdateProgress(object obj)
		{
			try
			{
				int version = (int)obj;
				if (value_paint == value_now)
				{
					return;
				}
				float m_speed = Math.Abs(value_paint - value_now) / 10f;
				if (m_speed == 0f)
				{
					m_speed = 1f;
				}
				while (value_paint != value_now)
				{
					Thread.Sleep(17);
					if (version != m_version)
					{
						break;
					}
					lock (hybirdLock)
					{
						float newActual = 0f;
						if (value_paint > value_now)
						{
							float offect2 = value_paint - value_now;
							if (offect2 > m_speed)
							{
								offect2 = m_speed;
							}
							newActual = value_paint - offect2;
						}
						else
						{
							float offect = value_now - value_paint;
							if (offect > m_speed)
							{
								offect = m_speed;
							}
							newActual = value_paint + offect;
						}
						value_paint = newActual;
					}
					if (version == m_version)
					{
						if (base.IsHandleCreated)
						{
							Invoke(m_UpdateAction);
						}
						continue;
					}
					break;
				}
			}
			catch (Exception)
			{
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslBattery";
			base.Size = new System.Drawing.Size(93, 171);
			ResumeLayout(false);
		}
	}
}
