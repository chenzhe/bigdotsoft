using System.Collections.Generic;
using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// AI电脑的算法核心
	/// </summary>
	internal class ComputerAI
	{
		private GobangPoint[,] main_chess = null;

		/// <summary>
		/// 实例化一个电脑的AI算法对象
		/// </summary>
		/// <param name="chess">棋盘对象</param>
		public ComputerAI(GobangPoint[,] chess)
		{
			main_chess = chess;
		}

		public void CalculateAllPoints()
		{
			for (int i = 0; i < 21; i++)
			{
				for (int j = 0; j < 21; j++)
				{
					main_chess[i, j].WeightScore = GetWeight(i, j, GobangPlayer.Player2).TotleScore;
				}
			}
		}

		/// <summary>
		/// 计算电脑玩家的落子
		/// </summary>
		/// <returns>位置信息</returns>
		public Point CalculateComputerAI()
		{
			ChessWeight[,] m_Weights_Computer = new ChessWeight[21, 21];
			ChessWeight[,] m_Weights_Player1 = new ChessWeight[21, 21];
			for (int i = 0; i <= 20; i++)
			{
				for (int n = 0; n <= 20; n++)
				{
					if (main_chess[i, n].GobangPlayer != GobangPlayer.NonePlayer)
					{
						m_Weights_Computer[i, n] = new ChessWeight(new int[4]
						{
							-1,
							-1,
							-1,
							-1
						}, GobangPlayer.Player1);
						m_Weights_Player1[i, n] = new ChessWeight(new int[4]
						{
							-1,
							-1,
							-1,
							-1
						}, GobangPlayer.Player2);
					}
					else
					{
						m_Weights_Computer[i, n] = GetWeight(i, n, GobangPlayer.Player1);
						m_Weights_Player1[i, n] = GetWeight(i, n, GobangPlayer.Player2);
						m_Weights_Computer[i, n].WeightOpponent = m_Weights_Player1[i, n].WeightMax;
						m_Weights_Player1[i, n].WeightOpponent = m_Weights_Computer[i, n].WeightMax;
					}
				}
			}
			List<Point> MaxPointComputer = new List<Point>();
			int MaxComputer = 0;
			int MaxPlayer1 = 0;
			List<Point> MaxPointPlayer1 = new List<Point>();
			for (int k = 0; k <= 20; k++)
			{
				for (int j2 = 0; j2 <= 20; j2++)
				{
					if (m_Weights_Computer[k, j2].TotleScore > MaxComputer)
					{
						MaxComputer = m_Weights_Computer[k, j2].TotleScore;
						MaxPointComputer.Clear();
						MaxPointComputer.Add(new Point(k, j2));
					}
					else if (m_Weights_Computer[k, j2].TotleScore == MaxComputer)
					{
						MaxPointComputer.Add(new Point(k, j2));
					}
					if (m_Weights_Player1[k, j2].TotleScore > MaxPlayer1)
					{
						MaxPlayer1 = m_Weights_Player1[k, j2].TotleScore;
						MaxPointPlayer1.Clear();
						MaxPointPlayer1.Add(new Point(k, j2));
					}
					else if (m_Weights_Player1[k, j2].TotleScore == MaxPlayer1)
					{
						MaxPointPlayer1.Add(new Point(k, j2));
					}
				}
			}
			if ((MaxComputer > 49 && MaxPlayer1 < 200) || (MaxComputer >= 200 && MaxPlayer1 >= 200))
			{
				int MaxTemp2 = 0;
				Point MaxPoint3 = default(Point);
				for (int m = 0; m < MaxPointComputer.Count; m++)
				{
					if (m_Weights_Computer[MaxPointComputer[m].X, MaxPointComputer[m].Y].WeightOpponent > MaxTemp2)
					{
						MaxTemp2 = m_Weights_Computer[MaxPointComputer[m].X, MaxPointComputer[m].Y].WeightOpponent;
						MaxPoint3 = MaxPointComputer[m];
					}
				}
				return MaxPoint3;
			}
			if (MaxComputer >= MaxPlayer1)
			{
				int MaxTemp3 = 0;
				Point MaxPoint2 = default(Point);
				for (int l = 0; l < MaxPointComputer.Count; l++)
				{
					if (m_Weights_Computer[MaxPointComputer[l].X, MaxPointComputer[l].Y].WeightOpponent > MaxTemp3)
					{
						MaxTemp3 = m_Weights_Computer[MaxPointComputer[l].X, MaxPointComputer[l].Y].WeightOpponent;
						MaxPoint2 = MaxPointComputer[l];
					}
				}
				return MaxPoint2;
			}
			int MaxTemp = 0;
			Point MaxPoint = default(Point);
			for (int j = 0; j < MaxPointPlayer1.Count; j++)
			{
				if (m_Weights_Player1[MaxPointPlayer1[j].X, MaxPointPlayer1[j].Y].WeightOpponent > MaxTemp)
				{
					MaxTemp = m_Weights_Player1[MaxPointPlayer1[j].X, MaxPointPlayer1[j].Y].WeightOpponent;
					MaxPoint = MaxPointPlayer1[j];
				}
			}
			return MaxPoint;
		}

		private GobangPoint GetClassPoint(int x, int y, GobangPlayer m_play)
		{
			if (x >= 0 && x < 21 && y >= 0 && y < 21)
			{
				return main_chess[x, y];
			}
			if (m_play == GobangPlayer.Player1)
			{
				return new GobangPoint
				{
					GobangPlayer = GobangPlayer.Player2
				};
			}
			return new GobangPoint
			{
				GobangPlayer = GobangPlayer.Player1
			};
		}

		private ChessWeight GetWeight(int m_x, int m_y, GobangPlayer m_play)
		{
			int[] m_Weight = new int[4];
			for (int i = 0; i < 4; i++)
			{
				m_Weight[i] = GetWeightSingle(i, m_x, m_y, m_play);
			}
			return new ChessWeight(m_Weight, m_play);
		}

		private int GetWeightSingle(int Direction, int x, int y, GobangPlayer player)
		{
			GobangPoint[] LEFT_TO_RIGHT = new GobangPoint[9];
			switch (Direction)
			{
			case 0:
			{
				for (int i = 0; i < 9; i++)
				{
					LEFT_TO_RIGHT[i] = GetClassPoint(x + i - 4, y, player);
				}
				break;
			}
			case 1:
			{
				for (int j = 0; j < 9; j++)
				{
					LEFT_TO_RIGHT[j] = GetClassPoint(x, y + j - 4, player);
				}
				break;
			}
			case 2:
			{
				for (int k = 0; k < 9; k++)
				{
					LEFT_TO_RIGHT[k] = GetClassPoint(x + k - 4, y + k - 4, player);
				}
				break;
			}
			default:
			{
				for (int l = 0; l < 9; l++)
				{
					LEFT_TO_RIGHT[l] = GetClassPoint(x - (l - 4), y + l - 4, player);
				}
				break;
			}
			}
			GobangPlayer player_enemy = GobangPlayer.Player1;
			if (player == GobangPlayer.Player1)
			{
				player_enemy = GobangPlayer.Player2;
			}
			GobangPoint OwnPawn = new GobangPoint
			{
				GobangPlayer = player
			};
			GobangPoint OtherPawn = new GobangPoint
			{
				GobangPlayer = player_enemy
			};
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 1;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn)
			{
				return 200;
			}
			if (LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn)
			{
				return 200;
			}
			if (LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn)
			{
				return 200;
			}
			if (LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn)
			{
				return 200;
			}
			if (LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OwnPawn)
			{
				return 200;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 50;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 50;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 50;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 50;
			}
			if (LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn)
			{
				return 50;
			}
			if (LEFT_TO_RIGHT[0] == OtherPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OtherPawn)
			{
				return 12;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty && LEFT_TO_RIGHT[8] == OwnPawn)
			{
				return 11;
			}
			if (LEFT_TO_RIGHT[0] == OtherPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[0] == OtherPawn && LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty && LEFT_TO_RIGHT[8] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 10;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 10;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 10;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 9;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 8;
			}
			if (LEFT_TO_RIGHT[0] == OtherPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[0] == OtherPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[8] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[8] == OtherPawn)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[8] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3].IsEmpty)
			{
				return 7;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[7] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[0] == OwnPawn && LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 6;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[7].IsEmpty && LEFT_TO_RIGHT[8] == OwnPawn)
			{
				return 6;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[1].IsEmpty && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[0].IsEmpty && LEFT_TO_RIGHT[1] == OwnPawn && LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[7] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[8].IsEmpty)
			{
				return 5;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 4;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6].IsEmpty)
			{
				return 4;
			}
			if (LEFT_TO_RIGHT[2].IsEmpty && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 4;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 4;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[7].IsEmpty)
			{
				return 3;
			}
			if (LEFT_TO_RIGHT[1] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 3;
			}
			if (LEFT_TO_RIGHT[5] == OtherPawn && LEFT_TO_RIGHT[2] == OwnPawn && LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[1].IsEmpty)
			{
				return 3;
			}
			if (LEFT_TO_RIGHT[7] == OtherPawn && LEFT_TO_RIGHT[6] == OwnPawn && LEFT_TO_RIGHT[5].IsEmpty && LEFT_TO_RIGHT[3].IsEmpty)
			{
				return 3;
			}
			if (LEFT_TO_RIGHT[2] == OtherPawn && LEFT_TO_RIGHT[3] == OwnPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OwnPawn && LEFT_TO_RIGHT[6] == OtherPawn)
			{
				return 0;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 1;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5].IsEmpty)
			{
				return 2;
			}
			if (LEFT_TO_RIGHT[3].IsEmpty && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 2;
			}
			if (LEFT_TO_RIGHT[3] == OtherPawn && LEFT_TO_RIGHT[5] == OtherPawn)
			{
				return 0;
			}
			return 1;
		}
	}
}
