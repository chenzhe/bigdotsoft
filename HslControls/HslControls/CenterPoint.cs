using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 计算的基础数据 
	/// </summary>
	public class CenterPoint
	{
		/// <summary>
		/// 中心点的位置
		/// </summary>
		public Point Point
		{
			get;
			set;
		}

		/// <summary>
		/// 半径
		/// </summary>
		public int Radius
		{
			get;
			set;
		}

		/// <summary>
		/// 角度
		/// </summary>
		public double Angle
		{
			get;
			set;
		}
	}
}
