using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 箭头控件类
	/// </summary>
	[Description("一个箭头控件，支持设置不同的方向，颜色，文本信息")]
	public class HslArrow : UserControl
	{
		private StringFormat sf = null;

		private Color edgeColor = Color.Gray;

		private Pen edgePen = new Pen(Color.Gray, 1f);

		private Color arrowBackColor = Color.DodgerBlue;

		private Brush brushArrow = new SolidBrush(Color.DodgerBlue);

		private ArrowDirection arrowDirection = ArrowDirection.Right;

		private float factorX = 0.75f;

		private float factorY = 0.27f;

		private bool useGradient = true;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件的前景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的前景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "White")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// 获取或设置箭头控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置箭头控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				edgePen = new Pen(value, 1f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置箭头控件的方向
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置箭头控件的方向")]
		[Category("HslControls")]
		[DefaultValue(typeof(ArrowDirection), "Right")]
		public ArrowDirection ArrowDirection
		{
			get
			{
				return arrowDirection;
			}
			set
			{
				arrowDirection = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置箭头背景的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置箭头背景的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public Color ArrowBackColor
		{
			get
			{
				return arrowBackColor;
			}
			set
			{
				arrowBackColor = value;
				brushArrow?.Dispose();
				brushArrow = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置横向宽度的比例值，范围0-1
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置横向宽度的比例值，范围0-1")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public float FactorX
		{
			get
			{
				return factorX;
			}
			set
			{
				if (value >= 0f && value <= 1f)
				{
					factorX = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置纵向宽度的比例值，范围0-0.5
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置纵向宽度的比例值，范围0-0.5")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public float FactorY
		{
			get
			{
				return factorY;
			}
			set
			{
				if (value >= 0f && value <= 0.5f)
				{
					factorY = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置本箭头控件是否使用渐进色
		/// </summary>
		[Category("HslControls")]
		[Description("获取或设置本箭头控件是否使用渐进色")]
		[Browsable(true)]
		[DefaultValue(true)]
		public bool UseGradient
		{
			get
			{
				return useGradient;
			}
			set
			{
				useGradient = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的箭头控件
		/// </summary>
		public HslArrow()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			PaintHslControls(g, base.Width, base.Height);
			base.OnPaint(e);
		}

		/// <summary>
		/// 绘制当前控件的界面到指定的绘图上下文里面，默认位置0，0，需要指定宽度和高度信息
		/// </summary>
		/// <param name="g">绘图上下文</param>
		/// <param name="width">宽度信息</param>
		/// <param name="height">高度信息</param>
		public void PaintHslControls(Graphics g, float width, float height)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			PointF[] points = null;
			RectangleF rectText = default(RectangleF);
			switch (arrowDirection)
			{
			case ArrowDirection.Right:
				points = new PointF[8]
				{
					new PointF(0f, height * factorY),
					new PointF(width * factorX, height * factorY),
					new PointF(width * factorX, 0f),
					new PointF(width - 1f, height * 0.5f),
					new PointF(width * factorX, height - 1f),
					new PointF(width * factorX, height * (1f - factorY)),
					new PointF(0f, height * (1f - factorY)),
					new PointF(0f, height * factorY)
				};
				rectText = new RectangleF(0f, height * factorY, width * (1f - (1f - factorX) / 2f), height * (1f - factorY * 2f));
				break;
			case ArrowDirection.Left:
				points = new PointF[8]
				{
					new PointF(0f, height * 0.5f),
					new PointF(width * (1f - factorX), 0f),
					new PointF(width * (1f - factorX), height * factorY),
					new PointF(width - 1f, height * factorY),
					new PointF(width - 1f, height * (1f - factorY)),
					new PointF(width * (1f - factorX), height * (1f - factorY)),
					new PointF(width * (1f - factorX), height - 1f),
					new PointF(0f, height * 0.5f)
				};
				rectText = new RectangleF((1f - factorX) / 2f * width, height * factorY, width * (1f - (1f - factorX) / 2f), height * (1f - factorY * 2f));
				break;
			case ArrowDirection.Up:
				points = new PointF[8]
				{
					new PointF(width * 0.5f, 0f),
					new PointF(width - 1f, height * (1f - factorX)),
					new PointF(width * (1f - factorY), height * (1f - factorX)),
					new PointF(width * (1f - factorY), height - 1f),
					new PointF(width * factorY, height - 1f),
					new PointF(width * factorY, height * (1f - factorX)),
					new PointF(0f, height * (1f - factorX)),
					new PointF(width * 0.5f, 0f)
				};
				rectText = new RectangleF(width * factorY, height * (1f - factorX) / 2f, width * (1f - factorY * 2f), height * (1f - (1f - factorX) / 2f));
				break;
			case ArrowDirection.Down:
				points = new PointF[8]
				{
					new PointF(width * (1f - factorY), 0f),
					new PointF(width * (1f - factorY), height * factorX),
					new PointF(width - 1f, height * factorX),
					new PointF(width / 2f, height - 1f),
					new PointF(0f, height * factorX),
					new PointF(width * factorY, height * factorX),
					new PointF(width * factorY, 0f),
					new PointF(width * (1f - factorY), 0f)
				};
				rectText = new RectangleF(width * factorY, 0f, width * (1f - factorY * 2f), height * (1f - (1f - factorX) / 2f));
				break;
			}
			if (points == null)
			{
				return;
			}
			if (useGradient)
			{
				ColorBlend colorBlend = new ColorBlend();
				colorBlend.Positions = new float[3]
				{
					0f,
					0.5f,
					1f
				};
				colorBlend.Colors = new Color[3]
				{
					arrowBackColor,
					HslHelper.GetColorLight(arrowBackColor),
					arrowBackColor
				};
				if (arrowDirection == ArrowDirection.Up || arrowDirection == ArrowDirection.Down)
				{
					using LinearGradientBrush linearGradientBrush = new LinearGradientBrush(new PointF(0f, 0f), new PointF(width - 1f, 0f), arrowBackColor, arrowBackColor);
					linearGradientBrush.InterpolationColors = colorBlend;
					g.FillPolygon(linearGradientBrush, points);
				}
				else
				{
					using LinearGradientBrush brush = new LinearGradientBrush(new PointF(0f, 0f), new PointF(0f, height - 1f), arrowBackColor, arrowBackColor);
					brush.InterpolationColors = colorBlend;
					g.FillPolygon(brush, points);
				}
			}
			else
			{
				g.FillPolygon(brushArrow, points);
			}
			g.DrawPolygon(edgePen, points);
			using Brush fontBrush = new SolidBrush(ForeColor);
			g.DrawString(Text, Font, fontBrush, rectText, sf);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			ForeColor = System.Drawing.Color.White;
			base.Name = "HslArrow";
			base.Size = new System.Drawing.Size(176, 49);
			ResumeLayout(false);
		}
	}
}
