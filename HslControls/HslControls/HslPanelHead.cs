using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 带有文本头的Panel控件
	/// </summary>
	[Description("Panel控件，继承自系统控件，控件上部分携带一个渐变色的文本区域")]
	public class HslPanelHead : Panel
	{
		private int headHeight = 30;

		private Color themeColor = Color.DodgerBlue;

		private StringFormat sf = null;

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的主题色
		/// </summary>
		[Description("获取或设置当前控件的主题色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public Color ThemeColor
		{
			get
			{
				return themeColor;
			}
			set
			{
				themeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件顶部文本的高度
		/// </summary>
		[Description("获取或设置控件顶部文本的高度")]
		[Category("HslControls")]
		[DefaultValue(30)]
		public int HeadHeight
		{
			get
			{
				return headHeight;
			}
			set
			{
				headHeight = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslPanelHead()
		{
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		protected override void OnPaintBackground(PaintEventArgs e)
		{
			base.OnPaintBackground(e);
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				using (LinearGradientBrush brush2 = new LinearGradientBrush(new PointF(0f, 0f), new PointF(0f, headHeight), HslHelper.GetColorLight(themeColor), themeColor))
				{
					e.Graphics.FillRectangle(brush2, 0, 0, base.Width, headHeight);
				}
				using (Brush brush = new SolidBrush(ForeColor))
				{
					e.Graphics.DrawString(Text, Font, brush, new Rectangle(0, 0, base.Width, headHeight), sf);
				}
				using Pen pen = new Pen(themeColor);
				e.Graphics.DrawRectangle(pen, 0, 0, base.Width - 1, base.Height - 1);
				e.Graphics.DrawLine(pen, 0, headHeight, base.Width - 1, headHeight);
			}
		}
	}
}
