using System.Runtime.CompilerServices;

namespace HslControls
{
	/// <summary>
	/// 一个工业物联网的控件库，基于C#开发，配套HslCommunication组件可以实现工业上位机软件的快速开发，本库将持续的迭代更新
	/// <br /><br />
	/// 官方网站：<a href="http://www.hslcommunication.cn/">http://www.hslcommunication.cn/</a>，包含组件的在线API地址以及一个MES DEMO的项目展示。
	/// <br /><br />
	/// <br /><br />
	/// </summary>
	/// <remarks>
	/// <br />
	/// <br />
	/// 打赏请扫码：<br />
	/// <img src="https://raw.githubusercontent.com/dathlin/HslCommunication/master/imgs/support.png" />
	/// </remarks>
	/// <revisionHistory>
	///     <revision date="2018-12-18" version="1.0.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>正式发布第一个版本的控件库。</item>
	///         </list>
	///     </revision>
	///     <revision date="2018-12-21" version="1.0.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>瓶子控件优化显示，底座高度可调节。</item>
	///             <item>新增一个传送带的控件，可调节方向，大小。</item>
	///         </list>
	///     </revision>
	///     <revision date="2018-12-24" version="1.0.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增一个传送带滚筒的控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2018-12-27" version="1.0.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增一个温度计的控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2018-12-30" version="1.0.4" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增一个风机控件。</item>
	///             <item>新增一个阀门控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-2" version="1.0.5" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增一个电池控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-5" version="1.0.6" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>修复电池控件初始值不为0的情况显示异常的bug。</item>
	///             <item>修复曲线控件在特殊的情况下的精度丢失问题。</item>
	///             <item>新增一个分类器的控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-10" version="1.1.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>激活方法新增是否激活成功的反馈。</item>
	///             <item>修复时钟控件分钟属性拼写错误。</item>
	///             <item>修复时钟控件在特别小的情况下显示错乱的异常。</item>
	///             <item>修复LED控件前景色的默认属性。</item>
	///             <item>新增一个五子棋的控件，支持人机对战。</item>
	///             <item>新增一个俄罗斯方块的控件，demo中支持正常玩法和变态玩法。</item>
	///             <item>新增一个状态集的控件，用于批量表示一些信号情况。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-18" version="1.2.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>修复状态集控件在矩形模式下显示模糊的bug。</item>
	///             <item>新增一个历史曲线的控件，支持一些复杂的交互操作。</item>
	///             <item>新增一个坦克大战的控件，目前比较粗糙。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-20" version="1.2.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslBottle控件升级为所有的颜色可调。</item>
	///             <item>电池控件新增是否启用动画的属性，可以自由的切换。</item>
	///             <item>历史曲线控件新增是否显示右坐标。</item>
	///             <item>历史曲线新增纵坐标轴的单位显示功能。</item>
	///             <item>历史曲线新增很多颜色信息的可配置功能，满足大家各种审美需求。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-24" version="1.2.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增一个甘特图控件，可以用来表示时间占用信息。</item>
	///             <item>曲线控件支持数据双击事件，可以方便的获取到数据索引和时间信息，无论图形是否缩放，都能获取到正确的数据。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-1-31" version="1.2.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>曲线控件修复一个调整大小时界面显示不正确的bug。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-2-11" version="1.2.4" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>修复所有的控件在不同的字体大小情况下显示错位的bug。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-2-16" version="1.2.5" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>修复实时曲线再显示追加批量数据出现的错位bug。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-3-5" version="1.2.6" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>实时曲线控件新增特殊点标记的功能。</item>
	///             <item>实时曲线控件的缓存数据量调整为4096。</item>
	///             <item>历史曲线控件新增特殊点标记的功能。</item>
	///             <item>新增的数据点标记功能支持标记点颜色，文本颜色，文本位置的设置。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-3-11" version="1.2.7" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>新增实时曲线的实时标注信息。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-3-14" version="2.0.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>集成了几个新的chart控件，来自鞍山-OldCat的贡献，感谢支持。</item>
	///             <item>历史曲线控件顶部曲线标题的宽度调整更长。</item>
	///             <item>历史曲线控件鼠标活动时的提示矩形框的宽度可设置。</item>
	///             <item>历史曲线控件新增线段标记功能。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-4-20" version="2.0.4" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>历史曲线新增移除标记的方法。</item>
	///             <item>历史曲线新增曲线标题宽度设计。</item>
	///             <item>历史曲线新增滚动条到右侧的公开方法。</item>
	///             <item>修改所有的自定义属性分类为HslControls。</item>
	///             <item>历史曲线支持鼠标选择区域后的事件触发。</item>
	///             <item>实时曲线新增曲线名称，当且图标名称设置为空的时候显示。</item>
	///             <item>实时曲线新增一个公开的方法，清楚所有曲线的数据，曲线重新从无开始显示。</item>
	///             <item>新增一个箭头控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-4-29" version="2.0.5" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>LED控件新增单位属性，可以设置显示单位信息。</item>
	///             <item>新增一个泵的控件，可以自由的调整2个口的位置。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-5-16" version="2.0.6" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>实时曲线新增横轴虚线的范围调整。</item>
	///             <item>历史曲线新增横轴虚线的范围调整。默认200个像素点显示一条虚线。</item>
	///             <item>历史曲线横轴的时间格式开放出来，现在可以根据自己的需求设置了。</item>
	///             <item>历史曲线新增了移除所有的前置的区间标记的内容的API接口，需要调用RenderCurveUI()来刷新界面。</item>
	///             <item>发布全新的安卓版本的控件库，目前实现了部分的控件内容。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-5-20" version="2.0.7" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>修复俄罗斯方块控件的一个空引用的bug。</item>
	///             <item>历史曲线新增移除所有鼠标选择的区间范围方法。</item>
	///             <item>时钟控件新增是否显示日期的属性选项。</item>
	///             <item>仪表盘控件的指针大小可以调整。</item>
	///             <item>新增一个全新的柱状图控件，可以支持颜色指定，详细见demo。</item>
	///             <item>安卓版本的控件库新增仪表盘，饼图，柱状图，实时历史曲线控件。</item>
	///             <item>祝天下有情人终成眷属。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-5-27" version="2.1.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>曲线控件的辅助线新增是否实线的选择，默认是虚线。</item>
	///             <item>历史曲线控件新增一个属性，是否禁用光标区间选择，禁用后，光标将无法选择区间。</item>
	///             <item>报警灯控件支持闪烁时间调节。</item>
	///             <item>历史曲线新增一个设置自定义的横轴的数据功能。不用强制为时间，可以为任意的字符串。</item>
	///             <item>温度控件的单位配置为可调，以便适用于更加普遍的情况，左右数据的映射关系的委托可自定义。</item>
	///             <item>HslMotor: 新增电机控件。</item>
	///             <item>安卓版控件新增传送带，报警灯，电机控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-6-12" version="2.1.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslLanternSimple: 新增一个鼠标触发点击的方法PerformClick( )</item>
	///             <item>HslBarChart: 最大值可以直接设置为小于0</item>
	///             <item>HslCurveHistory: 支持双击的时候，固定光标内容。</item>
	///             <item>HslCurveHistory: 新增Y轴的刻度线强制为整数的属性，这样刻度就不再出现小数点了。</item>
	///             <item>新增2个卡片容器的控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-6-16" version="2.1.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslCurve:修复实时曲线最大值最小值改成一样导致显示奔溃的bug。</item>
	///             <item>HslCurve:实时曲线目前已经支持完全自定义的横轴数据新增，不再强制为时间相关的横坐标。</item>
	///             <item>HslCurveHistory:历史曲线的光标挪动的提示位置在被阻挡时会自动挪到左侧。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-6-30" version="2.1.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslThermometer:修复一个委托属性导致的设计器显示失败的bug。</item>
	///             <item>winform:新增一个多彩进度条控件HslProgressColorful。</item>
	///             <item>wpf:新增wpf版本的控件，已实现HslBottle,HslIndicator,HslLanternAlarm,HslLanternSimple,HslPipeLine,HslSwitch。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-7-9" version="2.1.4" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslProgress:winform版本和安卓版的进度条新增显示任意的格式化文本的功能操作。</item>
	///             <item>wpf控件:新增一个指示灯，阀门，及水池的控件，详细参照wpf的demo。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-7-14" version="2.1.5" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslPipeLine:管道控件支持动画间隔设置，默认速度调整为0，中心线宽度及颜色可以调整。适用winform,wpf,andriod。</item>
	///             <item>wpf控件:新增一个柱状图控件，使用方式和winform是一样的。</item>
	///             <item>wpf控件:新增一个实时曲线控件，使用方式和winform是一样的。目前唯一不同的是点击曲线名，曲线还不能自动隐藏</item>
	///             <item>新增一个cnc机床的控件，支持仓门的开关状态，适用winform,wpf,andriod。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-7-21" version="2.1.6" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslMachineCenter添加新的加工中心控件，支持门的开合。适用winform,wpf,andriod。</item>
	///             <item>HslTitle，新增winform的窗体标题控件，可以用来做看板界面的标题栏。</item>
	///             <item>HslArrow: 箭头控件新增是否使用渐进色的属性，并默认为使用。</item>
	///             <item>HslButton: 新增是否使用渐进色的属性，默认为不使用。</item>
	///             <item>HslPanelTextBack: winform新增一个容器控件，可设置背景的容器控件，详细参照demo。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-7-24" version="2.1.7" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslCncCenter: 数控机床控件新增一个报警灯，可选三个状态显示。适用winform,wpf,andriod。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-7-31" version="2.1.8" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslGauge: 仪表盘控件现在支持赋值任意的值，当超过最大值或是最小值时，指针在最边上。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-8-7" version="2.1.9" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslCurve: 曲线控件支持标签文本的插入显示，适用winform,wpf,android。</item>
	///             <item>HslMachineCenter: 加工中心控件支持信号灯显示，适用winform,wpf,andriod。</item>
	///             <item>HslPumpOne: wpf版本控件支持泵控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-8-17" version="2.2.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslClock: 时钟控件新增属性是否显示星期几的信息，设置为false就不显示星期。</item>
	///             <item>HslGauge: 仪表盘新增一个属性，单位数据可以显示在界面的下方。</item>
	///             <item>HslCurveHistory: 历史曲线控件新增可标记的标题文本，在顶部固定区域显示的内容。</item>
	///             <item>HMILedSingle: 新增一个超级信号灯控件，支持完善的属性配置，颜色配置，形状配置，边距配置。</item>
	///             <item>HslVacuumPump: wpf 新增一个旋转泵控件，支持颜色，转动速度设置。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-8-24" version="2.2.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslVacuumPump:winfrom及安卓控件新增双叶泵控件。</item>
	///             <item>HslWaterBox: wpf版本新增一个水箱控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-8-30" version="2.2.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslPieChart: 饼图控件显示优化，支持文本颜色可调，文本区域范围可调，winform和Android已经适配。</item>
	///             <item>HslPieChart: 饼图控件在设计器模式下，初始化一个数据内容。</item>
	///             <item>HslPieChart: 饼图控件的百分比显示支持自定义的格式化操作。</item>
	///             <item>HslTitle: 标题控件的大小百分比调整。</item>
	///             <item>HslMoveText: winform，wpf，Android新增移动文本控件，可设置字体，颜色，移动速度。</item>
	///             <item>HslWaterPump: wpf控件新增水泵控件。</item>
	///             <item>发布全新的Demo程序，基于Dockpanel实现的界面，外加属性窗口可任意设置属性。</item>
	///             <item>新增控件库VIP群，入群费2000rmb，支持获取源代码，包含winform，wpf，安卓版本，支持永久更新，群号：893580884。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-9-26" version="2.2.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>大部分控件调整放大设置，不再随着字体变大而变大。</item>
	///             <item>新增一个数字键盘，支持输入数字的功能，详细参考demo，仅支持winform。</item>
	///             <item>新增控件库VIP群，入群费2000rmb，支持获取源代码，包含winform，wpf，安卓版本，支持永久更新，群号：893580884。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-11-6" version="2.2.4" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>历史曲线控件的曲线名称修复太长的情况造成换行的bug。</item>
	///             <item>新增一个冷却风扇控件，支持转速调节，详细参考demo，支持winform，wpf,android，其中wpf版本动画效果最好。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-11-9" version="2.2.5" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>实时曲线的控件支持曲线中断功能，新增数据的时候，添加 float.NaN 即可。</item>
	///             <item>历史的控件支持曲线中断功能，初始化曲线数据的时候，添加 float.NaN 即可。</item>
	///             <item>wpf版本新增一个已经在winform上存在的控件，分类器控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-12-12" version="2.2.6" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>历史的控件修复标记点索引为 float.NaN 时引发异常的bug。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-12-22" version="2.2.7" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslBottle: Wpf版本的控件修复BottleTag失败的bug。</item>
	///             <item>HslSignature: 新增签名控件，支持设置颜色，背景，画笔粗细，支持winform，wpf，安卓的版本。</item>
	///             <item>HslCircularGauge: Wpf版本新增仪表盘控件，感谢埃及朋友abdalaMask（QQ：3165336899）提供支持，详细功能见demo。</item>
	///         </list>
	///     </revision>
	///     <revision date="2019-12-28" version="2.2.8" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslDigitalInput: 数字键盘控件修复一些逻辑bug，增加掩码功能，这样就可以用于密码输入。</item>
	///             <item>HslCircularGauge: wpf版本的仪表盘控件优化，动画效果优化，更加流畅。</item>
	///             <item>wpf: Wpf版本新增七段数码管和十六段数码管的控件，感谢埃及朋友abdalaMask（QQ：3165336899）提供支持，详细功能见demo。</item>
	///             <item>wpf: Wpf版本角度仪表盘控件，感谢埃及朋友abdalaMask（QQ：3165336899）提供支持，详细功能见demo。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-3-8" version="2.2.9" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslPanel：新增一个增强型panel组件，继承系统panel，支持渐变色背景。</item>
	///             <item>HslBagFilter: 新增一个袋式除尘器控件。适用winform，安卓，wpf</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-3-17" version="2.3.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslStatusManagement：对每个的点阵信息，支持设置消息提示，当光标挪动到点阵上，提示信息文本。</item>
	///             <item>HslTruck: 新增一个卡车控件，支持两种状态，方向变换，升降功能，目前仅支持winform。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-4-12" version="2.3.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslIndicator: WPF版本的控件添加指示灯控件，感谢埃及朋友支持。</item>
	///             <item>hslcurvehistory：历史曲线控件的光标移动时X轴提示的信息支持自定义格式化操作。</item>
	///             <item>hslcurvehistory：历史曲线控件的光标移动到边界时，x轴提示信息显示不会遮挡。</item>
	///             <item>annotation：winform所有的控件增加设计时的注释信息，方便大家选取。</item>
	///             <item>HslDialPlate：winform增加一个新的流量表控件。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-5-10" version="2.3.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslDialPlate: 安卓版本添加流量表控件。</item>
	///             <item>hslcurve：实时曲线控件的样式，从是否平滑改为，线段，曲线，阶梯，阶梯不带竖线。</item>
	///             <item>hslcurvehistory：历史曲线控件的样式，从是否平滑改为，线段，曲线，阶梯，阶梯不带竖线。</item>
	///             <item>hslcurve：实时曲线控件增加一个属性PointsRadius，数据点的大小，默认为0，不显示，值越大，显示的数据点越大。</item>
	///             <item>hslcurvehistory：历史曲线控件增加一个属性PointsRadius，数据点的大小，默认为0，不显示，值越大，显示的数据点越大。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-6-30" version="2.3.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslCurveHistory: 修复在阶跃显示模式下，点位提示不正确的bug。</item>
	///             <item>HslBattery：增加一个WPF版本的电池控件，动画更加流畅。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-7-14" version="2.4.0" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>NoBoundaryControl: 添加一个基础的无界控件，支持缩放，拖拽，没有任何的其他实现，可以作为基础控件。</item>
	///             <item>HslChinaMap: 添加一个中国地图的控件，支持省份标记，地级市标记，支持任意的城市画线操作。</item>
	///             <item>HslArrow：增加一个WPF版本的箭头控件，虽然wpf本身支持旋转，但仍然实现方向可调节。</item>
	///             <item>普通VIP群的激活码，支持的连续运行时间调整为3个月，软件重启重新计时。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-7-17" version="2.4.1" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslChinaMap: 中国地图完善缺失的城市信息，添加了新疆的几个城市，添加了青海的自治州，添加了宁夏的地级市。</item>
	///             <item>HslChinaMap: 新增一个画图的API方法，允许用户把自定义的image绘制到地图的指定区域，参考demo的天气预报。</item>
	///             <item>HslGanttChart: WPF版本的控件添加一个甘特图的控件。</item>
	///             <item>HslBattery: WPF版本的电池控件修复一个显示的bug信息。</item>
	///             <item>HslControls企业授权费：3900rmb，一次付费，终身授权，开放源代码，支持后续更新。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-7-28" version="2.4.2" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslPipeLine: 修复更改端口样式的时候，底图不修改的BUG。</item>
	///             <item>HslPipeLineThree: 新增一个三通的管道，可以自由的控制线条流动信息，控制任意的线条是否显示。</item>
	///             <item>WPF: WPF版本的控件添加一个三通管道的控件。</item>
	///             <item>WPF: WPF版本的控件添加一个状态集的控件。</item>
	///             <item>HslControls企业授权费：3900rmb，一次付费，终身授权，开放源代码，支持后续更新。</item>
	///         </list>
	///     </revision>
	///     <revision date="2020-9-3" version="2.4.3" author="Richard.Hu">
	///         <list type="bullet">
	///             <item>HslTable: 新增一个表格控件，可以用来简单的显示不需要交互的数据信息。</item>
	///             <item>HslControls企业授权费：3900rmb，一次付费，终身授权，开放源代码，支持后续更新。</item>
	///         </list>
	///     </revision>
	/// </revisionHistory>
	[CompilerGenerated]
	public class NamespaceDoc
	{
	}
}
