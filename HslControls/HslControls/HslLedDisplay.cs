using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 数码管显示的控件信息，可以显示一串数码管的信息
	/// </summary>
	[Description("数码管显示控件，支持数字，小数点和特殊的英文字幕显示，支持显示单位")]
	public class HslLedDisplay : UserControl
	{
		private Color backColor = Color.FromArgb(62, 62, 62);

		private Brush backBrush = new SolidBrush(Color.FromArgb(62, 62, 62));

		private Brush foreBrush = new SolidBrush(Color.Tomato);

		private int displayNumber = 6;

		private int ledNumberSize = 12;

		private string displayText = "100.0";

		private string supportChars = "0123456789AbCcdEFHhJLoPrU: -";

		private string unitText = string.Empty;

		private int leftRightOffect = 10;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[46, 46, 46]")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的位数
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(6)]
		[Description("获取或设置数码管显示的位数")]
		public int DisplayNumber
		{
			get
			{
				return displayNumber;
			}
			set
			{
				if (value >= 0 && value < 100)
				{
					displayNumber = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的大小
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(12)]
		[Description("获取或设置数码管显示的大小")]
		public int LedNumberSize
		{
			get
			{
				return ledNumberSize;
			}
			set
			{
				ledNumberSize = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的内容
		/// </summary>
		[Category("HslControls")]
		[DefaultValue("100.0")]
		[Description("获取或设置数码管显示的内容")]
		public string DisplayText
		{
			get
			{
				return displayText;
			}
			set
			{
				displayText = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的背景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[62, 62, 62]")]
		[Description("获取或设置数码管数字的背景色")]
		public Color DisplayBackColor
		{
			get
			{
				return backColor;
			}
			set
			{
				backColor = value;
				backBrush.Dispose();
				backBrush = new SolidBrush(backColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的前景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Tomato")]
		[Description("获取或设置数码管数字的前景色")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
				foreBrush.Dispose();
				foreBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的单位文本
		/// </summary>
		[Category("HslControls")]
		[DefaultValue("")]
		[Description("获取或设置数码管数字的单位文本")]
		public string UnitText
		{
			get
			{
				return unitText;
			}
			set
			{
				unitText = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置数码管两端的空闲长度
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(10)]
		[Description("获取或设置数码管两端的空闲长度")]
		public int LeftRightOffect
		{
			get
			{
				return leftRightOffect;
			}
			set
			{
				leftRightOffect = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslLedDisplay()
		{
			InitializeComponent();
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			base.ForeColor = Color.Tomato;
		}

		/// <summary>
		/// 重绘控件的界面
		/// </summary>
		/// <param name="e">重绘的事件信息</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			if (base.Width < 3 || base.Height < 3)
			{
				return;
			}
			int offect = leftRightOffect;
			int right_offect = leftRightOffect;
			if (!string.IsNullOrEmpty(unitText))
			{
				right_offect = 2 + (int)e.Graphics.MeasureString(unitText, Font).Width;
			}
			float every_width = (float)(base.Width - offect - right_offect) / 1f / (float)displayNumber;
			int charCount = SumCharCount(displayText);
			for (int k = 0; k < displayNumber; k++)
			{
				DrawNumber(e.Graphics, (int)((float)k * every_width) + offect, 0, (int)every_width, base.Height, ' ', hasSpot: false);
			}
			if (string.IsNullOrEmpty(displayText))
			{
				return;
			}
			if (displayNumber >= charCount)
			{
				int m = displayNumber - 1;
				for (int j = displayText.Length - 1; j >= 0; j--)
				{
					if (supportChars.Contains(displayText[j]))
					{
						DrawNumber(e.Graphics, (int)((float)m * every_width) + offect, 0, (int)every_width, base.Height, displayText[j], IsCharAfterSpot(displayText, j));
						m--;
						if (m < 0)
						{
							break;
						}
					}
				}
			}
			else
			{
				int l = 0;
				for (int i = 0; i < displayText.Length; i++)
				{
					if (supportChars.Contains(displayText[i]))
					{
						DrawNumber(e.Graphics, (int)((float)l * every_width) + offect, 0, (int)every_width, base.Height, displayText[i], IsCharAfterSpot(displayText, i));
						l++;
						if (l >= displayNumber)
						{
							break;
						}
					}
				}
			}
			if (!string.IsNullOrEmpty(unitText))
			{
				e.Graphics.DrawString(unitText, Font, foreBrush, base.Width - right_offect - 2, 3f);
			}
			base.OnPaint(e);
		}

		private int SumCharCount(string value)
		{
			int count = 0;
			for (int i = 0; i < value.Length; i++)
			{
				if (supportChars.Contains(value[i]))
				{
					count++;
				}
			}
			return count;
		}

		private bool IsCharAfterSpot(string value, int index)
		{
			if (string.IsNullOrEmpty(value))
			{
				return false;
			}
			if (index >= value.Length - 1)
			{
				return false;
			}
			if (value[index + 1] == '.')
			{
				return true;
			}
			return false;
		}

		private void DrawHorizontalItem(Graphics g, Brush brush, int x, int y, int width)
		{
			Point[] points = new Point[7]
			{
				new Point(x, y),
				new Point(x - ledNumberSize / 2, y + ledNumberSize / 2),
				new Point(x, y + ledNumberSize),
				new Point(x + width, y + ledNumberSize),
				new Point(x + width + ledNumberSize / 2, y + ledNumberSize / 2),
				new Point(x + width, y),
				new Point(x, y)
			};
			g.FillPolygon(brush, points);
		}

		private void DrawVerticalItem(Graphics g, Brush brush, int x, int y, int height)
		{
			Point[] points = new Point[7]
			{
				new Point(x, y),
				new Point(x, y + height),
				new Point(x + ledNumberSize / 2, y + height + ledNumberSize / 2),
				new Point(x + ledNumberSize, y + height),
				new Point(x + ledNumberSize, y),
				new Point(x + ledNumberSize / 2, y - ledNumberSize / 2),
				new Point(x, y)
			};
			g.FillPolygon(brush, points);
		}

		private void DrawNumber(Graphics g, int x, int y, int width, int height, char charValue, bool hasSpot)
		{
			bool[] values = new bool[9];
			if (charValue == supportChars[0])
			{
				bool[] obj = new bool[9]
				{
					true,
					true,
					true,
					false,
					true,
					true,
					true,
					false,
					false
				};
				obj[7] = hasSpot;
				values = obj;
			}
			else if (charValue == supportChars[1])
			{
				values = new bool[9]
				{
					false,
					false,
					true,
					false,
					false,
					true,
					false,
					hasSpot,
					false
				};
			}
			else if (charValue == supportChars[2])
			{
				bool[] obj2 = new bool[9]
				{
					true,
					false,
					true,
					true,
					true,
					false,
					true,
					false,
					false
				};
				obj2[7] = hasSpot;
				values = obj2;
			}
			else if (charValue == supportChars[3])
			{
				bool[] obj3 = new bool[9]
				{
					true,
					false,
					true,
					true,
					false,
					true,
					true,
					false,
					false
				};
				obj3[7] = hasSpot;
				values = obj3;
			}
			else if (charValue == supportChars[4])
			{
				bool[] obj4 = new bool[9]
				{
					false,
					true,
					true,
					true,
					false,
					true,
					false,
					false,
					false
				};
				obj4[7] = hasSpot;
				values = obj4;
			}
			else if (charValue == supportChars[5])
			{
				bool[] obj5 = new bool[9]
				{
					true,
					true,
					false,
					true,
					false,
					true,
					true,
					false,
					false
				};
				obj5[7] = hasSpot;
				values = obj5;
			}
			else if (charValue == supportChars[6])
			{
				bool[] obj6 = new bool[9]
				{
					true,
					true,
					false,
					true,
					true,
					true,
					true,
					false,
					false
				};
				obj6[7] = hasSpot;
				values = obj6;
			}
			else if (charValue == supportChars[7])
			{
				bool[] obj7 = new bool[9]
				{
					true,
					false,
					true,
					false,
					false,
					true,
					false,
					false,
					false
				};
				obj7[7] = hasSpot;
				values = obj7;
			}
			else if (charValue == supportChars[8])
			{
				bool[] obj8 = new bool[9]
				{
					true,
					true,
					true,
					true,
					true,
					true,
					true,
					false,
					false
				};
				obj8[7] = hasSpot;
				values = obj8;
			}
			else if (charValue == supportChars[9])
			{
				bool[] obj9 = new bool[9]
				{
					true,
					true,
					true,
					true,
					false,
					true,
					true,
					false,
					false
				};
				obj9[7] = hasSpot;
				values = obj9;
			}
			else if (charValue == supportChars[10])
			{
				bool[] obj10 = new bool[9]
				{
					true,
					true,
					true,
					true,
					true,
					true,
					false,
					false,
					false
				};
				obj10[7] = hasSpot;
				values = obj10;
			}
			else if (charValue == supportChars[11])
			{
				bool[] obj11 = new bool[9]
				{
					false,
					true,
					false,
					true,
					true,
					true,
					true,
					false,
					false
				};
				obj11[7] = hasSpot;
				values = obj11;
			}
			else if (charValue == supportChars[12])
			{
				bool[] obj12 = new bool[9]
				{
					true,
					true,
					false,
					false,
					true,
					false,
					true,
					false,
					false
				};
				obj12[7] = hasSpot;
				values = obj12;
			}
			else if (charValue == supportChars[13])
			{
				bool[] obj13 = new bool[9]
				{
					false,
					false,
					false,
					true,
					true,
					false,
					true,
					false,
					false
				};
				obj13[7] = hasSpot;
				values = obj13;
			}
			else if (charValue == supportChars[14])
			{
				bool[] obj14 = new bool[9]
				{
					false,
					false,
					true,
					true,
					true,
					true,
					true,
					false,
					false
				};
				obj14[7] = hasSpot;
				values = obj14;
			}
			else if (charValue == supportChars[15])
			{
				bool[] obj15 = new bool[9]
				{
					true,
					true,
					false,
					true,
					true,
					false,
					true,
					false,
					false
				};
				obj15[7] = hasSpot;
				values = obj15;
			}
			else if (charValue == supportChars[16])
			{
				bool[] obj16 = new bool[9]
				{
					true,
					true,
					false,
					true,
					true,
					false,
					false,
					false,
					false
				};
				obj16[7] = hasSpot;
				values = obj16;
			}
			else if (charValue == supportChars[17])
			{
				bool[] obj17 = new bool[9]
				{
					false,
					true,
					true,
					true,
					true,
					true,
					false,
					false,
					false
				};
				obj17[7] = hasSpot;
				values = obj17;
			}
			else if (charValue == supportChars[18])
			{
				bool[] obj18 = new bool[9]
				{
					false,
					true,
					false,
					true,
					true,
					true,
					false,
					false,
					false
				};
				obj18[7] = hasSpot;
				values = obj18;
			}
			else if (charValue == supportChars[19])
			{
				bool[] obj19 = new bool[9]
				{
					false,
					false,
					true,
					false,
					false,
					true,
					true,
					false,
					false
				};
				obj19[7] = hasSpot;
				values = obj19;
			}
			else if (charValue == supportChars[20])
			{
				bool[] obj20 = new bool[9]
				{
					false,
					true,
					false,
					false,
					true,
					false,
					true,
					false,
					false
				};
				obj20[7] = hasSpot;
				values = obj20;
			}
			else if (charValue == supportChars[21])
			{
				bool[] obj21 = new bool[9]
				{
					false,
					false,
					false,
					true,
					true,
					true,
					true,
					false,
					false
				};
				obj21[7] = hasSpot;
				values = obj21;
			}
			else if (charValue == supportChars[22])
			{
				bool[] obj22 = new bool[9]
				{
					true,
					true,
					true,
					true,
					true,
					false,
					false,
					false,
					false
				};
				obj22[7] = hasSpot;
				values = obj22;
			}
			else if (charValue == supportChars[23])
			{
				values = new bool[9]
				{
					false,
					false,
					false,
					true,
					true,
					false,
					false,
					hasSpot,
					false
				};
			}
			else if (charValue == supportChars[24])
			{
				bool[] obj23 = new bool[9]
				{
					false,
					true,
					true,
					false,
					true,
					true,
					true,
					false,
					false
				};
				obj23[7] = hasSpot;
				values = obj23;
			}
			else if (charValue == supportChars[25])
			{
				values = new bool[9]
				{
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					true
				};
			}
			else if (charValue == supportChars[26])
			{
				values = new bool[9];
			}
			else if (charValue == supportChars[27])
			{
				values = new bool[9]
				{
					false,
					false,
					false,
					true,
					false,
					false,
					false,
					false,
					false
				};
			}
			DrawNumber(g, x, y, width, height, values);
		}

		private void DrawNumber(Graphics g, int x, int y, int width, int height, bool[] array)
		{
			if (array == null || array.Length < 9)
			{
				return;
			}
			int top = 5;
			int bottom = 5;
			int left = 3;
			int right = 4;
			int offect = 2;
			if (ledNumberSize < 6)
			{
				left = 2;
				right = 3;
				offect = 1;
			}
			if (ledNumberSize > 13)
			{
				left = 5;
				right = 6;
				offect = 3;
			}
			int every_width = width - left - right - 2 * ledNumberSize - 2 * offect;
			int every_height = (height - top - bottom - 3 * ledNumberSize - 4 * offect) / 2;
			if (array[0])
			{
				DrawHorizontalItem(g, foreBrush, x + left + ledNumberSize + offect, y + top, every_width);
			}
			else
			{
				DrawHorizontalItem(g, backBrush, x + left + ledNumberSize + offect, y + top, every_width);
			}
			if (array[1])
			{
				DrawVerticalItem(g, foreBrush, x + left, y + top + ledNumberSize + offect, every_height);
			}
			else
			{
				DrawVerticalItem(g, backBrush, x + left, y + top + ledNumberSize + offect, every_height);
			}
			if (array[2])
			{
				DrawVerticalItem(g, foreBrush, x + width - right - ledNumberSize, y + top + ledNumberSize + offect, every_height);
			}
			else
			{
				DrawVerticalItem(g, backBrush, x + width - right - ledNumberSize, y + top + ledNumberSize + offect, every_height);
			}
			if (array[3])
			{
				DrawHorizontalItem(g, foreBrush, x + left + ledNumberSize + offect, y + top + ledNumberSize + every_height + 2 * offect, every_width);
			}
			else
			{
				DrawHorizontalItem(g, backBrush, x + left + ledNumberSize + offect, y + top + ledNumberSize + every_height + 2 * offect, every_width);
			}
			if (array[4])
			{
				DrawVerticalItem(g, foreBrush, x + left, y + top + 2 * ledNumberSize + 3 * offect + every_height, every_height);
			}
			else
			{
				DrawVerticalItem(g, backBrush, x + left, y + top + 2 * ledNumberSize + 3 * offect + every_height, every_height);
			}
			if (array[5])
			{
				DrawVerticalItem(g, foreBrush, x + width - right - ledNumberSize, y + top + 2 * ledNumberSize + 3 * offect + every_height, every_height);
			}
			else
			{
				DrawVerticalItem(g, backBrush, x + width - right - ledNumberSize, y + top + 2 * ledNumberSize + 3 * offect + every_height, every_height);
			}
			if (array[6])
			{
				DrawHorizontalItem(g, foreBrush, x + left + ledNumberSize + offect, y + height - bottom - ledNumberSize, every_width);
			}
			else
			{
				DrawHorizontalItem(g, backBrush, x + left + ledNumberSize + offect, y + height - bottom - ledNumberSize, every_width);
			}
			if (x + width - offect * 2 < base.Width - 20)
			{
				if (array[7])
				{
					g.FillEllipse(foreBrush, x + width - offect * 2, y + height - bottom - ledNumberSize / 2 - offect, offect * 4, offect * 4);
				}
				else
				{
					g.FillEllipse(backBrush, x + width - offect * 2, y + height - bottom - ledNumberSize / 2 - offect, offect * 4, offect * 4);
				}
			}
			if (array[8])
			{
				Rectangle rec = new Rectangle(x + width / 2 - offect * 2, y + top + ledNumberSize + offect + every_height - offect * 4, offect * 4, offect * 4);
				g.FillEllipse(foreBrush, rec);
				rec.Y += offect + offect + ledNumberSize + offect + offect * 4;
				g.FillEllipse(foreBrush, rec);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.FromArgb(46, 46, 46);
			base.Name = "HslLedDisplay";
			base.Size = new System.Drawing.Size(325, 58);
			ResumeLayout(false);
		}
	}
}
