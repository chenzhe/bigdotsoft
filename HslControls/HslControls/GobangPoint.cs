using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 棋盘上一个棋子的类，指示是否有落子，下棋顺序等信息
	/// </summary>
	public class GobangPoint
	{
		/// <summary>
		/// 获取或设置当前的点位的玩家状态，无玩家，玩家一，玩家二
		/// </summary>
		public GobangPlayer GobangPlayer
		{
			get;
			set;
		} = GobangPlayer.NonePlayer;


		/// <summary>
		/// the order number
		/// </summary>
		public int StepNumber
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// indicates the weight score
		/// </summary>
		public int WeightScore
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 获取当前棋子的
		/// </summary>
		public Brush GetPawnBrush => GobangPlayer switch
		{
			GobangPlayer.Player1 => Brushes.Orange, 
			GobangPlayer.Player2 => Brushes.DimGray, 
			_ => Brushes.Transparent, 
		};

		/// <summary>
		/// 该坐标点位是否有棋子
		/// </summary>
		public bool IsEmpty => GobangPlayer == GobangPlayer.NonePlayer;

		/// <summary>
		/// 判断两个点位的玩家是否一致
		/// </summary>
		/// <param name="gp1">玩家一</param>
		/// <param name="gp2">玩家二</param>
		/// <returns>是否一致，是返回true，否返回false</returns>
		public static bool operator ==(GobangPoint gp1, GobangPoint gp2)
		{
			return gp1.GobangPlayer == gp2.GobangPlayer;
		}

		/// <summary>
		/// 判断两个点位的玩家是否不一致
		/// </summary>
		/// <param name="gp1">玩家一</param>
		/// <param name="gp2">玩家二</param>
		/// <returns>不一致，是返回true，否返回false</returns>
		public static bool operator !=(GobangPoint gp1, GobangPoint gp2)
		{
			return gp1.GobangPlayer != gp2.GobangPlayer;
		}

		/// <summary>
		/// 确定指定的对象是否等于当前的对象
		/// </summary>
		/// <param name="obj">指定的对象</param>
		/// <returns>是否相等</returns>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// 作为默认的哈希值
		/// </summary>
		/// <returns>哈希值</returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
