using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个电机的基本控件
	/// </summary>
	[Description("电机控件，支持颜色的设置，文本信息的设置")]
	public class HslMotor : UserControl
	{
		private StringFormat sf = null;

		private Color colorEdge = Color.FromArgb(89, 91, 96);

		private Color colorCenter = Color.FromArgb(200, 200, 200);

		private Pen penEdge = new Pen(Color.Gray);

		private Color borderColor = Color.Gray;

		private ArrowDirection arrowDirection = ArrowDirection.Right;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电机的主题色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置电机的主题色")]
		[DefaultValue(typeof(Color), "[89, 91, 96]")]
		public Color EdgeColor
		{
			get
			{
				return colorEdge;
			}
			set
			{
				colorEdge = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电机的比较淡的中间色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置电机的比较淡的中间色")]
		[DefaultValue(typeof(Color), "[200, 200, 200]")]
		public Color CenterColor
		{
			get
			{
				return colorCenter;
			}
			set
			{
				colorCenter = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置电机的边框的颜色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置电机的边框的颜色")]
		[DefaultValue(typeof(Color), "Gray")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				penEdge?.Dispose();
				penEdge = new Pen(borderColor, 1f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置点击的方向，默认是向右的方向
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置点击的方向，默认是向右的方向")]
		[DefaultValue(ArrowDirection.Right)]
		public ArrowDirection Direction
		{
			get
			{
				return arrowDirection;
			}
			set
			{
				arrowDirection = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的电机控件
		/// </summary>
		public HslMotor()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				RectangleF textRect = default(RectangleF);
				if (arrowDirection == ArrowDirection.Right)
				{
					textRect = new RectangleF((float)base.Width * 0.21f, 0f, (float)base.Width * 0.24f, (float)base.Height * 0.22f);
					PaintMain(g, base.Width, base.Height);
				}
				else if (arrowDirection == ArrowDirection.Left)
				{
					textRect = new RectangleF((float)base.Width * 0.55f, 0f, (float)base.Width * 0.24f, (float)base.Height * 0.22f);
					Matrix matrix = new Matrix();
					matrix.Scale(-1f, 1f);
					g.MultiplyTransform(matrix);
					g.TranslateTransform(-base.Width, 0f);
					PaintMain(g, base.Width, base.Height);
					g.ResetTransform();
				}
				else if (arrowDirection == ArrowDirection.Up)
				{
					textRect = new RectangleF(0f, (float)base.Height * 0.55f, (float)base.Width * 0.22f, (float)base.Height * 0.24f);
					g.RotateTransform(-90f);
					g.TranslateTransform(-base.Height, 0f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				else
				{
					textRect = new RectangleF((float)base.Width * 0.78f, (float)base.Height * 0.21f, (float)base.Width * 0.22f, (float)base.Height * 0.24f);
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				using (Brush brush2 = new SolidBrush(ForeColor))
				{
					g.DrawString(Text, Font, brush2, textRect, sf);
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.25f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				colorEdge,
				colorCenter,
				colorEdge
			};
			using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(new PointF(0f, height * 0.28f), new PointF(0f, height * 0.92f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
			{
				linearGradientBrush.InterpolationColors = colorBlend;
				g.FillRectangle(linearGradientBrush, new RectangleF(width * 0.2f, height * 0.28f, width * 0.41f, height * 0.64f));
			}
			using (LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(new PointF(0f, height * 0.25f), new PointF(0f, height * 0.95f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
			{
				linearGradientBrush2.InterpolationColors = colorBlend;
				g.FillEllipse(linearGradientBrush2, new RectangleF(0f, height * 0.25f, width * 0.2f, height * 0.7f));
				g.FillRectangle(linearGradientBrush2, new RectangleF(width * 0.1f, height * 0.25f, width * 0.11f, height * 0.7f));
				g.FillRectangle(linearGradientBrush2, new RectangleF(width * 0.6f, height * 0.25f, width * 0.25f, height * 0.7f));
				g.DrawLine(penEdge, width * 0.21f, height * 0.25f, width * 0.21f, height * 0.95f);
				g.DrawLine(penEdge, width * 0.6f, height * 0.25f, width * 0.6f, height * 0.95f);
			}
			using (LinearGradientBrush linearGradientBrush3 = new LinearGradientBrush(new PointF(0f, height * 0.52f), new PointF(0f, height * 0.68f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
			{
				linearGradientBrush3.InterpolationColors = colorBlend;
				g.FillRectangle(linearGradientBrush3, new RectangleF(width * 0.9f, height * 0.52f, width * 0.1f + 1f, height * 0.16f));
			}
			using (LinearGradientBrush linearGradientBrush4 = new LinearGradientBrush(new PointF(0f, height * 0.46f), new PointF(0f, height * 0.74f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
			{
				linearGradientBrush4.InterpolationColors = colorBlend;
				PointF[] points2 = new PointF[5]
				{
					new PointF(width * 0.84f, height * 0.46f),
					new PointF(width * 0.9f, height * 0.52f),
					new PointF(width * 0.9f, height * 0.68f),
					new PointF(width * 0.84f, height * 0.74f),
					new PointF(width * 0.84f, height * 0.46f)
				};
				g.FillPolygon(linearGradientBrush4, points2);
			}
			using (LinearGradientBrush linearGradientBrush5 = new LinearGradientBrush(new PointF(0f, height * 0.2f), new PointF(0f, height * 1f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
			{
				linearGradientBrush5.InterpolationColors = colorBlend;
				g.FillRectangle(linearGradientBrush5, new RectangleF(width * 0.82f, height * 0.2f, width * 0.04f, height * 0.8f));
				g.DrawRectangle(penEdge, width * 0.82f, height * 0.2f, width * 0.04f, height * 0.8f);
			}
			for (int i = 0; i < 12; i++)
			{
				float totleHeight = height * 0.64f / 12f;
				float everyHeight = height * 0.02f;
				if (everyHeight < 3f)
				{
					everyHeight = 3f;
				}
				using LinearGradientBrush brush = new LinearGradientBrush(new PointF(0f, height * 0.3f + totleHeight * (float)i), new PointF(0f, height * 0.3f + totleHeight * (float)i + everyHeight), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
				brush.InterpolationColors = colorBlend;
				g.FillRectangle(brush, new RectangleF(width * 0.21f, height * 0.3f + totleHeight * (float)i, width * 0.39f, everyHeight));
			}
			using (Brush brush3 = new SolidBrush(HslHelper.GetColorLight(colorEdge)))
			{
				PointF[] points = new PointF[5]
				{
					new PointF(width * 0.21f, height * 0.22f),
					new PointF(width * 0.23f, height * 0.28f),
					new PointF(width * 0.43f, height * 0.28f),
					new PointF(width * 0.45f, height * 0.22f),
					new PointF(width * 0.21f, height * 0.22f)
				};
				g.FillPolygon(brush3, points);
				ColorBlend colorBlend2 = new ColorBlend();
				colorBlend2.Positions = new float[5]
				{
					0f,
					0.1f,
					0.9f,
					0.95f,
					1f
				};
				colorBlend2.Colors = new Color[5]
				{
					colorEdge,
					HslHelper.GetColorLight(colorEdge),
					HslHelper.GetColorLight(colorEdge),
					colorCenter,
					colorEdge
				};
				using (LinearGradientBrush brush2 = new LinearGradientBrush(new PointF(width * 0.21f, 0f), new PointF(width * 0.45f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke))
				{
					brush2.InterpolationColors = colorBlend2;
					g.FillRectangle(brush2, new RectangleF(width * 0.21f, 0f, width * 0.24f, height * 0.22f));
				}
				g.DrawRectangle(penEdge, width * 0.21f, 0f, width * 0.24f, height * 0.22f);
				g.DrawPolygon(penEdge, points);
			}
			using LinearGradientBrush linearGradientBrush6 = new LinearGradientBrush(new PointF(width * 0.65f, 0f), new PointF(width * 0.75f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			linearGradientBrush6.InterpolationColors = colorBlend;
			g.FillEllipse(linearGradientBrush6, new RectangleF(width * 0.65f, height * 0.6f, width * 0.1f, width * 0.1f));
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslMotor";
			base.Size = new System.Drawing.Size(275, 134);
			ResumeLayout(false);
		}
	}
}
