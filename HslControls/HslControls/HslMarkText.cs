using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 曲线图里的文本标记信息
	/// </summary>
	public class HslMarkText
	{
		/// <summary>
		/// 文本的便宜标记信息
		/// </summary>
		public static readonly int MarkTextOffect = 5;

		/// <summary>
		/// 针对的曲线的关键字
		/// </summary>
		public string CurveKey
		{
			get;
			set;
		}

		/// <summary>
		/// 位置的索引
		/// </summary>
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// 标记的文本
		/// </summary>
		public string MarkText
		{
			get;
			set;
		}

		/// <summary>
		/// 文本的颜色
		/// </summary>
		public Brush TextBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 圆圈的画刷
		/// </summary>
		public Brush CircleBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 点的标记状态
		/// </summary>
		public MarkTextPositionStyle PositionStyle
		{
			get;
			set;
		} = MarkTextPositionStyle.Auto;


		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslMarkText()
		{
			CircleBrush = Brushes.DodgerBlue;
			TextBrush = Brushes.Black;
		}

		/// <summary>
		/// 根据数据和索引计算文本的绘制的位置
		/// </summary>
		/// <param name="data">数据信息</param>
		/// <param name="Index">索引</param>
		/// <returns>绘制的位置信息</returns>
		public static MarkTextPositionStyle CalculateDirectionFromDataIndex(float[] data, int Index)
		{
			float befor = (Index == 0) ? data[Index] : data[Index - 1];
			float after = (Index == data.Length - 1) ? data[Index] : data[Index + 1];
			if (befor < data[Index] && data[Index] < after)
			{
				return MarkTextPositionStyle.Left;
			}
			if (befor > data[Index] && data[Index] > after)
			{
				return MarkTextPositionStyle.Right;
			}
			if (befor <= data[Index] && data[Index] >= after)
			{
				return MarkTextPositionStyle.Up;
			}
			if (befor >= data[Index] && data[Index] <= after)
			{
				return MarkTextPositionStyle.Down;
			}
			return MarkTextPositionStyle.Up;
		}
	}
}
