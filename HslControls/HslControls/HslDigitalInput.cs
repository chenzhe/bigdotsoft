using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HslControls
{
	[Description("数字键盘控件，支持数字，小数点，支持掩码操作，可用来触摸输入一些数据信息")]
	public class HslDigitalInput : UserControl
	{
		private StringBuilder sb = new StringBuilder("0");

		private Color buttonColor = Color.Lavender;

		private Color textForeColor = Color.DimGray;

		private bool enableSpot = true;

		private bool enableNegative = true;

		private char digitalMask = '\0';

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		private SplitContainer splitContainer1;

		private Panel panel1;

		private HslLedDisplay hslLedDisplay1;

		private TableLayoutPanel tableLayoutPanel1;

		private HslButton hslButton15;

		private HslButton hslButton14;

		private HslButton hslButton13;

		private HslButton hslButton12;

		private HslButton hslButton11;

		private HslButton hslButton10;

		private HslButton hslButton9;

		private HslButton hslButton8;

		private HslButton hslButton7;

		private HslButton hslButton6;

		private HslButton hslButton5;

		private HslButton hslButton4;

		private HslButton hslButton3;

		private HslButton hslButton2;

		private HslButton hslButton1;

		public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
				hslButton1.Font = value;
				hslButton2.Font = value;
				hslButton3.Font = value;
				hslButton4.Font = value;
				hslButton5.Font = value;
				hslButton6.Font = value;
				hslButton7.Font = value;
				hslButton8.Font = value;
				hslButton9.Font = value;
				hslButton10.Font = value;
				hslButton11.Font = value;
				hslButton12.Font = value;
			}
		}

		/// <summary>
		/// 当按ok键触发的信息
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Action<string> OnOk
		{
			get;
			set;
		}

		/// <summary>
		/// 对结果的信息检查
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Func<string, bool> InputCheck
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[46, 46, 46]")]
		public Color LedBackColor
		{
			get
			{
				return hslLedDisplay1.BackColor;
			}
			set
			{
				hslLedDisplay1.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的位数
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(8)]
		[Description("获取或设置数码管显示的位数")]
		public int DisplayNumber
		{
			get
			{
				return hslLedDisplay1.DisplayNumber;
			}
			set
			{
				hslLedDisplay1.DisplayNumber = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的大小
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(6)]
		[Description("获取或设置数码管显示的大小")]
		public int LedNumberSize
		{
			get
			{
				return hslLedDisplay1.LedNumberSize;
			}
			set
			{
				hslLedDisplay1.LedNumberSize = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管显示的内容
		/// </summary>
		[Category("HslControls")]
		[DefaultValue("0")]
		[Description("获取或设置数码管显示的内容")]
		public string DisplayText
		{
			get
			{
				return sb.ToString();
			}
			set
			{
				sb = new StringBuilder(value);
				hslLedDisplay1.DisplayText = GetMaskTest(value);
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的背景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[62, 62, 62]")]
		[Description("获取或设置数码管数字的背景色")]
		public Color DisplayBackColor
		{
			get
			{
				return hslLedDisplay1.DisplayBackColor;
			}
			set
			{
				hslLedDisplay1.DisplayBackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的前景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Tomato")]
		[Description("获取或设置数码管数字的前景色")]
		public Color LedForeColor
		{
			get
			{
				return hslLedDisplay1.ForeColor;
			}
			set
			{
				hslLedDisplay1.ForeColor = value;
			}
		}

		/// <summary>
		/// 获取或设置数码管数字的单位文本
		/// </summary>
		[Category("HslControls")]
		[DefaultValue("")]
		[Description("获取或设置数码管数字的单位文本")]
		public string UnitText
		{
			get
			{
				return hslLedDisplay1.UnitText;
			}
			set
			{
				hslLedDisplay1.UnitText = value;
			}
		}

		/// <summary>
		/// 当需要设置密码的时候，设置为掩码，就不显示真实的数据信息
		/// </summary>
		[Category("HslControls")]
		[DefaultValue('\0')]
		[Description("当需要设置密码的时候，设置为掩码，就不显示真实的数据信息")]
		public char MaskChar
		{
			get
			{
				return digitalMask;
			}
			set
			{
				digitalMask = value;
				DisplayText = sb.ToString();
			}
		}

		/// <summary>
		/// 获取或设置数码管两端的空闲长度
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(10)]
		[Description("获取或设置数码管两端的空闲长度")]
		public int LeftRightOffect
		{
			get
			{
				return hslLedDisplay1.LeftRightOffect;
			}
			set
			{
				hslLedDisplay1.LeftRightOffect = value;
			}
		}

		/// <summary>
		/// 是否启用小数点
		/// </summary>
		public bool EnableSpot
		{
			get
			{
				return enableSpot;
			}
			set
			{
				enableSpot = value;
				hslButton12.Enabled = value;
			}
		}

		/// <summary>
		/// 是否启动负号
		/// </summary>
		public bool EnableNegative
		{
			get
			{
				return enableNegative;
			}
			set
			{
				enableNegative = value;
				hslButton10.Enabled = value;
			}
		}

		/// <summary>
		/// 获取或设置所有按钮的背景色
		/// </summary>
		[Description("获取或设置所有按钮的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Lavender")]
		public Color ButtonColor
		{
			get
			{
				return buttonColor;
			}
			set
			{
				buttonColor = value;
				hslButton1.OriginalColor = value;
				hslButton2.OriginalColor = value;
				hslButton3.OriginalColor = value;
				hslButton4.OriginalColor = value;
				hslButton5.OriginalColor = value;
				hslButton6.OriginalColor = value;
				hslButton7.OriginalColor = value;
				hslButton8.OriginalColor = value;
				hslButton9.OriginalColor = value;
				hslButton10.OriginalColor = value;
				hslButton11.OriginalColor = value;
				hslButton12.OriginalColor = value;
				hslButton13.OriginalColor = value;
				hslButton14.OriginalColor = value;
				hslButton15.OriginalColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return textForeColor;
			}
			set
			{
				textForeColor = value;
				hslButton1.ForeColor = value;
				hslButton2.ForeColor = value;
				hslButton3.ForeColor = value;
				hslButton4.ForeColor = value;
				hslButton5.ForeColor = value;
				hslButton6.ForeColor = value;
				hslButton7.ForeColor = value;
				hslButton8.ForeColor = value;
				hslButton9.ForeColor = value;
				hslButton10.ForeColor = value;
				hslButton11.ForeColor = value;
				hslButton12.ForeColor = value;
				hslButton13.ForeColor = value;
				hslButton14.ForeColor = value;
				hslButton15.ForeColor = value;
				Invalidate();
			}
		}

		public HslDigitalInput()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			hslButton1.Click += HslButton1_Click;
			hslButton2.Click += HslButton2_Click;
			hslButton3.Click += HslButton3_Click;
			hslButton4.Click += HslButton4_Click;
			hslButton5.Click += HslButton5_Click;
			hslButton6.Click += HslButton6_Click;
			hslButton7.Click += HslButton7_Click;
			hslButton8.Click += HslButton8_Click;
			hslButton9.Click += HslButton9_Click;
			hslButton10.Click += HslButton10_Click;
			hslButton11.Click += HslButton11_Click;
			hslButton12.Click += HslButton12_Click;
			hslButton13.Click += HslButton13_Click;
			hslButton14.Click += HslButton14_Click;
			hslButton15.Click += HslButton15_Click;
			hslLedDisplay1.DisplayText = "0";
		}

		private void HslButton15_Click(object sender, EventArgs e)
		{
			if (sb.Length > 0)
			{
				sb.Remove(sb.Length - 1, 1);
			}
			if (sb.Length == 0)
			{
				sb.Append("0");
			}
			DisplayText = sb.ToString();
		}

		private void HslButton14_Click(object sender, EventArgs e)
		{
			sb = new StringBuilder();
			sb.Append("0");
			DisplayText = sb.ToString();
		}

		private void HslButton13_Click(object sender, EventArgs e)
		{
			if (sb.ToString().EndsWith("."))
			{
				sb.Remove(sb.Length - 1, 1);
			}
			if (InputCheck != null && !InputCheck(sb.ToString()))
			{
				MessageBox.Show("Input Wrong, Please input again!");
			}
			else
			{
				OnOk?.Invoke(sb.ToString());
			}
		}

		private void HslButton12_Click(object sender, EventArgs e)
		{
			if (sb.ToString().IndexOf('.') < 0)
			{
				sb.Append(".");
				DisplayText = sb.ToString();
			}
		}

		private void HslButton11_Click(object sender, EventArgs e)
		{
			if (sb.ToString().IndexOf('.') >= 0 || (!sb.ToString().StartsWith("0") && !sb.ToString().StartsWith("-0")))
			{
				sb.Append("0");
				DisplayText = sb.ToString();
			}
		}

		private void HslButton10_Click(object sender, EventArgs e)
		{
			if (sb.ToString().StartsWith("-"))
			{
				sb.Remove(0, 1);
			}
			else
			{
				sb.Insert(0, "-");
			}
			DisplayText = sb.ToString();
		}

		private void LedAddText(string number)
		{
			if (sb.ToString().IndexOf(".") <= 0)
			{
				if (sb.ToString().StartsWith("0"))
				{
					sb.Remove(0, 1);
				}
				if (sb.ToString().StartsWith("-0"))
				{
					sb.Remove(1, 1);
				}
			}
			sb.Append(number);
			DisplayText = sb.ToString();
		}

		private void HslButton9_Click(object sender, EventArgs e)
		{
			LedAddText("9");
		}

		private void HslButton8_Click(object sender, EventArgs e)
		{
			LedAddText("8");
		}

		private void HslButton7_Click(object sender, EventArgs e)
		{
			LedAddText("7");
		}

		private void HslButton6_Click(object sender, EventArgs e)
		{
			LedAddText("6");
		}

		private void HslButton5_Click(object sender, EventArgs e)
		{
			LedAddText("5");
		}

		private void HslButton4_Click(object sender, EventArgs e)
		{
			LedAddText("4");
		}

		private void HslButton3_Click(object sender, EventArgs e)
		{
			LedAddText("3");
		}

		private void HslButton2_Click(object sender, EventArgs e)
		{
			LedAddText("2");
		}

		private void HslButton1_Click(object sender, EventArgs e)
		{
			LedAddText("1");
		}

		private string GetMaskTest(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return string.Empty;
			}
			if (digitalMask > '\0')
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < value.Length; i++)
				{
					if (char.IsDigit(value[i]))
					{
						stringBuilder.Append(digitalMask);
					}
					else
					{
						stringBuilder.Append(value[i]);
					}
				}
				return stringBuilder.ToString();
			}
			return value;
		}

		private void HslDigitalInput_Load(object sender, EventArgs e)
		{
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			splitContainer1 = new System.Windows.Forms.SplitContainer();
			panel1 = new System.Windows.Forms.Panel();
			tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			hslLedDisplay1 = new HslControls.HslLedDisplay();
			hslButton15 = new HslControls.HslButton();
			hslButton14 = new HslControls.HslButton();
			hslButton13 = new HslControls.HslButton();
			hslButton12 = new HslControls.HslButton();
			hslButton11 = new HslControls.HslButton();
			hslButton10 = new HslControls.HslButton();
			hslButton9 = new HslControls.HslButton();
			hslButton8 = new HslControls.HslButton();
			hslButton7 = new HslControls.HslButton();
			hslButton6 = new HslControls.HslButton();
			hslButton5 = new HslControls.HslButton();
			hslButton4 = new HslControls.HslButton();
			hslButton3 = new HslControls.HslButton();
			hslButton2 = new HslControls.HslButton();
			hslButton1 = new HslControls.HslButton();
			((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
			splitContainer1.Panel1.SuspendLayout();
			splitContainer1.Panel2.SuspendLayout();
			splitContainer1.SuspendLayout();
			panel1.SuspendLayout();
			tableLayoutPanel1.SuspendLayout();
			SuspendLayout();
			splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			splitContainer1.Location = new System.Drawing.Point(0, 0);
			splitContainer1.Name = "splitContainer1";
			splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			splitContainer1.Panel1.Controls.Add(panel1);
			splitContainer1.Panel2.Controls.Add(tableLayoutPanel1);
			splitContainer1.Size = new System.Drawing.Size(286, 305);
			splitContainer1.SplitterDistance = 56;
			splitContainer1.TabIndex = 0;
			panel1.Controls.Add(hslLedDisplay1);
			panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			panel1.Location = new System.Drawing.Point(0, 0);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(286, 56);
			panel1.TabIndex = 0;
			tableLayoutPanel1.ColumnCount = 3;
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334f));
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334f));
			tableLayoutPanel1.Controls.Add(hslButton15, 0, 4);
			tableLayoutPanel1.Controls.Add(hslButton14, 0, 4);
			tableLayoutPanel1.Controls.Add(hslButton13, 0, 4);
			tableLayoutPanel1.Controls.Add(hslButton12, 2, 3);
			tableLayoutPanel1.Controls.Add(hslButton11, 1, 3);
			tableLayoutPanel1.Controls.Add(hslButton10, 0, 3);
			tableLayoutPanel1.Controls.Add(hslButton9, 2, 2);
			tableLayoutPanel1.Controls.Add(hslButton8, 1, 2);
			tableLayoutPanel1.Controls.Add(hslButton7, 0, 2);
			tableLayoutPanel1.Controls.Add(hslButton6, 2, 1);
			tableLayoutPanel1.Controls.Add(hslButton5, 1, 1);
			tableLayoutPanel1.Controls.Add(hslButton4, 0, 1);
			tableLayoutPanel1.Controls.Add(hslButton3, 2, 0);
			tableLayoutPanel1.Controls.Add(hslButton2, 1, 0);
			tableLayoutPanel1.Controls.Add(hslButton1, 0, 0);
			tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 5;
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20f));
			tableLayoutPanel1.Size = new System.Drawing.Size(286, 245);
			tableLayoutPanel1.TabIndex = 0;
			hslLedDisplay1.BackColor = System.Drawing.Color.FromArgb(46, 46, 46);
			hslLedDisplay1.DisplayBackColor = System.Drawing.Color.FromArgb(62, 62, 62);
			hslLedDisplay1.DisplayNumber = 8;
			hslLedDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
			hslLedDisplay1.LedNumberSize = 6;
			hslLedDisplay1.Location = new System.Drawing.Point(0, 0);
			hslLedDisplay1.Name = "hslLedDisplay1";
			hslLedDisplay1.Size = new System.Drawing.Size(286, 56);
			hslLedDisplay1.TabIndex = 0;
			hslButton15.CustomerInformation = null;
			hslButton15.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton15.Location = new System.Drawing.Point(3, 199);
			hslButton15.Name = "hslButton15";
			hslButton15.Size = new System.Drawing.Size(89, 43);
			hslButton15.TabIndex = 14;
			hslButton15.Text = "Back";
			hslButton14.CustomerInformation = null;
			hslButton14.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton14.Location = new System.Drawing.Point(98, 199);
			hslButton14.Name = "hslButton14";
			hslButton14.Size = new System.Drawing.Size(89, 43);
			hslButton14.TabIndex = 13;
			hslButton14.Text = "Clear";
			hslButton13.CustomerInformation = null;
			hslButton13.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton13.Location = new System.Drawing.Point(193, 199);
			hslButton13.Name = "hslButton13";
			hslButton13.Size = new System.Drawing.Size(90, 43);
			hslButton13.TabIndex = 12;
			hslButton13.Text = "OK";
			hslButton12.CustomerInformation = null;
			hslButton12.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton12.Location = new System.Drawing.Point(193, 150);
			hslButton12.Name = "hslButton12";
			hslButton12.Size = new System.Drawing.Size(90, 43);
			hslButton12.TabIndex = 11;
			hslButton12.Text = ".";
			hslButton11.CustomerInformation = null;
			hslButton11.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton11.Location = new System.Drawing.Point(98, 150);
			hslButton11.Name = "hslButton11";
			hslButton11.Size = new System.Drawing.Size(89, 43);
			hslButton11.TabIndex = 10;
			hslButton11.Text = "0";
			hslButton10.CustomerInformation = null;
			hslButton10.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton10.Location = new System.Drawing.Point(3, 150);
			hslButton10.Name = "hslButton10";
			hslButton10.Size = new System.Drawing.Size(89, 43);
			hslButton10.TabIndex = 9;
			hslButton10.Text = "-";
			hslButton9.CustomerInformation = null;
			hslButton9.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton9.Location = new System.Drawing.Point(193, 101);
			hslButton9.Name = "hslButton9";
			hslButton9.Size = new System.Drawing.Size(90, 43);
			hslButton9.TabIndex = 8;
			hslButton9.Text = "9";
			hslButton8.CustomerInformation = null;
			hslButton8.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton8.Location = new System.Drawing.Point(98, 101);
			hslButton8.Name = "hslButton8";
			hslButton8.Size = new System.Drawing.Size(89, 43);
			hslButton8.TabIndex = 7;
			hslButton8.Text = "8";
			hslButton7.CustomerInformation = null;
			hslButton7.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton7.Location = new System.Drawing.Point(3, 101);
			hslButton7.Name = "hslButton7";
			hslButton7.Size = new System.Drawing.Size(89, 43);
			hslButton7.TabIndex = 6;
			hslButton7.Text = "7";
			hslButton6.CustomerInformation = null;
			hslButton6.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton6.Location = new System.Drawing.Point(193, 52);
			hslButton6.Name = "hslButton6";
			hslButton6.Size = new System.Drawing.Size(90, 43);
			hslButton6.TabIndex = 5;
			hslButton6.Text = "6";
			hslButton5.CustomerInformation = null;
			hslButton5.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton5.Location = new System.Drawing.Point(98, 52);
			hslButton5.Name = "hslButton5";
			hslButton5.Size = new System.Drawing.Size(89, 43);
			hslButton5.TabIndex = 4;
			hslButton5.Text = "5";
			hslButton4.CustomerInformation = null;
			hslButton4.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton4.Location = new System.Drawing.Point(3, 52);
			hslButton4.Name = "hslButton4";
			hslButton4.Size = new System.Drawing.Size(89, 43);
			hslButton4.TabIndex = 3;
			hslButton4.Text = "4";
			hslButton3.CustomerInformation = null;
			hslButton3.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton3.Location = new System.Drawing.Point(193, 3);
			hslButton3.Name = "hslButton3";
			hslButton3.Size = new System.Drawing.Size(90, 43);
			hslButton3.TabIndex = 2;
			hslButton3.Text = "3";
			hslButton2.CustomerInformation = null;
			hslButton2.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton2.Location = new System.Drawing.Point(98, 3);
			hslButton2.Name = "hslButton2";
			hslButton2.Size = new System.Drawing.Size(89, 43);
			hslButton2.TabIndex = 1;
			hslButton2.Text = "2";
			hslButton1.CustomerInformation = null;
			hslButton1.Dock = System.Windows.Forms.DockStyle.Fill;
			hslButton1.Location = new System.Drawing.Point(3, 3);
			hslButton1.Name = "hslButton1";
			hslButton1.Size = new System.Drawing.Size(89, 43);
			hslButton1.TabIndex = 0;
			hslButton1.Text = "1";
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Controls.Add(splitContainer1);
			base.Name = "HslDigitalInput";
			base.Size = new System.Drawing.Size(286, 305);
			base.Load += new System.EventHandler(HslDigitalInput_Load);
			splitContainer1.Panel1.ResumeLayout(false);
			splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
			splitContainer1.ResumeLayout(false);
			panel1.ResumeLayout(false);
			tableLayoutPanel1.ResumeLayout(false);
			ResumeLayout(false);
		}
	}
}
