namespace HslControls
{
	/// <summary>
	/// 管道线的端点朝向
	/// </summary>
	public enum HslPipeTurnDirection
	{
		/// <summary>
		/// 向上
		/// </summary>
		Up = 1,
		/// <summary>
		/// 向下
		/// </summary>
		Down,
		/// <summary>
		/// 向左
		/// </summary>
		Left,
		/// <summary>
		/// 向右
		/// </summary>
		Right,
		/// <summary>
		/// 无效果
		/// </summary>
		None
	}
}
