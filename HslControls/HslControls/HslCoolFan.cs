using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 冷却风扇控件
	/// </summary>
	[Description("冷却风扇控件，支持正反方向设置，支持转速设置，颜色设置")]
	public class HslCoolFan : UserControl
	{
		private StringFormat sf = null;

		private Color edgeColor = Color.Gray;

		private Color centerColor = Color.FromArgb(91, 92, 93);

		private Pen edgePen = new Pen(Color.Gray, 1f);

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		private float moveSpeed = 0.3f;

		private float startAngle = 0f;

		private Timer timer = null;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置阀门控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置阀门控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				edgePen = new Pen(value, 1f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置阀门控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置阀门控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[91, 92, 93]")]
		public Color CenterColor
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置泵的动画速度，0为静止，正数为正向流动，负数为反向流动
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置传送带流动的速度，0为静止，正数为正向流动，负数为反向流动")]
		[Category("HslControls")]
		[DefaultValue(0.3f)]
		public float MoveSpeed
		{
			get
			{
				return moveSpeed;
			}
			set
			{
				moveSpeed = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个冷却风扇控件
		/// </summary>
		public HslCoolFan()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			timer = new Timer();
			timer.Interval = 50;
			timer.Tick += Timer_Tick;
			timer.Start();
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, Math.Min(base.Width, base.Height), Math.Min(base.Width, base.Height));
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			g.TranslateTransform(width / 2f, width / 2f);
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.5f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(76, 76, 76),
				Color.FromArgb(245, 245, 245),
				Color.FromArgb(76, 76, 76)
			};
			LinearGradientBrush brush = new LinearGradientBrush(new PointF((0f - width) / 2f, 0f), new PointF(width / 2f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, (0f - width) / 2f, (0f - width) / 2f, width, width);
			brush.Dispose();
			using (Brush brush7 = new SolidBrush(Color.FromArgb(127, 127, 127)))
			{
				g.FillEllipse(brush7, new RectangleF((0f - width) * 0.46f, (0f - width) * 0.46f, width * 0.92f, width * 0.92f));
			}
			using (Brush brush6 = new SolidBrush(Color.FromArgb(35, 31, 32)))
			{
				g.FillEllipse(brush6, new RectangleF((0f - width) * 0.44f, (0f - width) * 0.44f, width * 0.88f, width * 0.88f));
			}
			g.RotateTransform(startAngle);
			for (int j = 0; j < 6; j++)
			{
				g.RotateTransform(60f);
				using Brush brush2 = new SolidBrush(Color.FromArgb(77, 77, 77));
				g.FillRectangle(brush2, new RectangleF(0f, (0f - width) * 0.08f, width * 0.4f, width * 0.16f));
			}
			for (int i = 0; i < 6; i++)
			{
				g.RotateTransform(60f);
				using GraphicsPath path = new GraphicsPath(FillMode.Alternate);
				path.AddLine(0f, (0f - width) * 0.08f, 0f, width * 0.08f);
				path.AddBezier(new PointF(0f, width * 0.08f), new PointF(width * 0.15f, width * 0.15f), new PointF(width * 0.25f, width * 0.15f), new PointF(width * 0.4f, width * 0.08f));
				path.AddLine(width * 0.4f, width * 0.08f, width * 0.4f, (0f - width) * 0.08f);
				path.AddBezier(new PointF(width * 0.4f, (0f - width) * 0.08f), new PointF(width * 0.25f, (0f - width) * 0.01f), new PointF(width * 0.15f, (0f - width) * 0.01f), new PointF(0f, (0f - width) * 0.08f));
				path.CloseFigure();
				brush = new LinearGradientBrush(new PointF(0f, width * 0.08f), new PointF(width * 0.4f, (0f - width) * 0.08f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
				colorBlend = new ColorBlend();
				colorBlend.Positions = new float[3]
				{
					0f,
					0.5f,
					1f
				};
				colorBlend.Colors = new Color[3]
				{
					centerColor,
					Color.FromArgb(236, 236, 236),
					centerColor
				};
				brush.InterpolationColors = colorBlend;
				g.FillPath(brush, path);
				brush.Dispose();
			}
			g.RotateTransform(0f - startAngle);
			using (Brush brush5 = new SolidBrush(Color.FromArgb(102, 102, 102)))
			{
				g.FillEllipse(brush5, new RectangleF((0f - width) * 0.13f, (0f - width) * 0.13f, width * 0.26f, width * 0.26f));
			}
			using (Brush brush4 = new SolidBrush(Color.FromArgb(229, 229, 229)))
			{
				g.FillEllipse(brush4, new RectangleF((0f - width) * 0.11f, (0f - width) * 0.11f, width * 0.22f, width * 0.22f));
			}
			using (Brush brush3 = new SolidBrush(Color.FromArgb(127, 127, 127)))
			{
				g.FillEllipse(brush3, new RectangleF((0f - width) * 0.05f, (0f - width) * 0.05f, width * 0.1f, width * 0.1f));
			}
			using (Brush foreBrush = new SolidBrush(ForeColor))
			{
				g.DrawString(Text, Font, foreBrush, new RectangleF((0f - width) / 2f, height * 0.38f, width, height * 0.55f), sf);
			}
			g.TranslateTransform((0f - width) / 2f, (0f - width) / 2f);
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (moveSpeed != 0f)
			{
				startAngle = (float)((double)startAngle + (double)(moveSpeed * 180f) / Math.PI / 10.0);
				if (startAngle <= -360f)
				{
					startAngle += 360f;
				}
				else if (startAngle >= 360f)
				{
					startAngle -= 360f;
				}
				Invalidate();
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslCoolFan";
			base.Size = new System.Drawing.Size(200, 200);
			ResumeLayout(false);
		}
	}
}
