using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个状态集显示的控件
	/// </summary>
	[Description("一个状态集显示控件，例如用来显示10×20仓库库位信息，还可以设置每个单元的提示消息")]
	public class HslStatusManagement : UserControl
	{
		private StringFormat sf = null;

		private int row = 2;

		private int col = 10;

		private float offect = 4f;

		private Color[,] arrayColor;

		private string[,] descriptions;

		private Color borderColor = Color.DimGray;

		private Pen borderPen = new Pen(Color.DimGray);

		private bool isRenderBorder = true;

		private HslRenderStyle renderStyle = HslRenderStyle.Rectangle;

		private Color defaultColor = Color.LightGray;

		private Point activePoint = new Point(-1, -1);

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置状态间距信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置状态间距信息")]
		[Category("HslControls")]
		[DefaultValue(4f)]
		public float SpacingOffect
		{
			get
			{
				return offect;
			}
			set
			{
				offect = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置状态集的行数信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置状态集的行数信息")]
		[Category("HslControls")]
		[DefaultValue(2)]
		public int RowCount
		{
			get
			{
				return row;
			}
			set
			{
				row = value;
				ArrayInni();
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置状态集的列数信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置状态集的列数信息")]
		[Category("HslControls")]
		[DefaultValue(10)]
		public int ColCount
		{
			get
			{
				return col;
			}
			set
			{
				col = value;
				ArrayInni();
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置是否显示边框信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置是否显示边框信息")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool IsRenderBorder
		{
			get
			{
				return isRenderBorder;
			}
			set
			{
				isRenderBorder = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置状态集的显示样式，支持矩形，椭圆，菱形
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置状态集的显示样式，支持矩形，椭圆，菱形")]
		[Category("HslControls")]
		[DefaultValue(HslRenderStyle.Rectangle)]
		public HslRenderStyle RenderStyle
		{
			get
			{
				return renderStyle;
			}
			set
			{
				renderStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置所有状态集合体的默认背景色，设置本数据将会强制所有的颜色恢复初始化
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置所有状态集合体的默认背景色，设置本数据将会强制所有的颜色恢复初始化")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LightGray")]
		public Color GeneralColor
		{
			get
			{
				return defaultColor;
			}
			set
			{
				defaultColor = value;
				ArrayInni();
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置所有状态集合体的边框颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置所有状态集合体的边框颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				borderPen.Dispose();
				borderPen = new Pen(borderColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个状态集的控件
		/// </summary>
		public HslStatusManagement()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			ArrayInni();
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.AntiAlias;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				PaintMain(g, base.Width, base.Height);
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			float itemWidth = (width - (float)col * offect) / (float)col;
			float itemHeight = (height - (float)row * offect) / (float)row;
			if (renderStyle == HslRenderStyle.Rectangle)
			{
				g.SmoothingMode = SmoothingMode.None;
			}
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					Rectangle rect = new Rectangle((int)(offect / 2f + (itemWidth + offect) * (float)j), (int)(offect / 2f + (itemHeight + offect) * (float)i), (int)itemWidth, (int)itemHeight);
					using Brush brush = new SolidBrush(arrayColor[i, j]);
					if (renderStyle == HslRenderStyle.Rectangle)
					{
						g.FillRectangle(brush, rect);
						if (isRenderBorder)
						{
							g.DrawRectangle(borderPen, rect);
						}
					}
					else if (renderStyle == HslRenderStyle.Ellipse)
					{
						g.FillEllipse(brush, rect);
						if (isRenderBorder)
						{
							g.DrawEllipse(borderPen, rect);
						}
					}
					else
					{
						g.FillPolygon(brush, HslHelper.GetRhombusFromRectangle(rect));
						if (isRenderBorder)
						{
							g.DrawPolygon(borderPen, HslHelper.GetRhombusFromRectangle(rect));
						}
					}
				}
			}
		}

		/// <summary>
		/// 通过行列信息来设置某个点的颜色
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="colIndex">列索引</param>
		/// <param name="color">颜色信息</param>
		public void SetColorByLocation(int rowIndex, int colIndex, Color color)
		{
			arrayColor[rowIndex, colIndex] = color;
			Invalidate();
		}

		/// <summary>
		/// 通过行列信息来复位某个点的颜色
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="colIndex">列索引</param>
		public void ResetColorByLocation(int rowIndex, int colIndex)
		{
			arrayColor[rowIndex, colIndex] = defaultColor;
			Invalidate();
		}

		/// <summary>
		/// 通过行列信息来设置某个点的颜色
		/// </summary>
		/// <param name="point">行列信息</param>
		/// <param name="color">颜色信息</param>
		public void SetColorByLocation(Point point, Color color)
		{
			SetColorByLocation(point.X, point.Y, color);
		}

		/// <summary>
		/// 通过行列信息来复位某个点的颜色
		/// </summary>
		/// <param name="point">行列信息</param>
		public void ResetColorByLocation(Point point)
		{
			ResetColorByLocation(point.X, point.Y);
		}

		/// <summary>
		/// 批量设置一些点的颜色信息
		/// </summary>
		/// <param name="points">点信息</param>
		/// <param name="color">颜色信息</param>
		public void SetColorByLocation(Point[] points, Color color)
		{
			if (points != null)
			{
				for (int i = 0; i < points.Length; i++)
				{
					arrayColor[points[i].X, points[i].Y] = color;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 批量复位一些点的颜色信息
		/// </summary>
		/// <param name="points">点信息</param>
		public void ResetColorByLocation(Point[] points)
		{
			if (points != null)
			{
				for (int i = 0; i < points.Length; i++)
				{
					arrayColor[points[i].X, points[i].Y] = defaultColor;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 将某一行的颜色都设置为指定的颜色
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="color">颜色信息</param>
		public void SetColorByRow(int rowIndex, Color color)
		{
			for (int i = 0; i < col; i++)
			{
				arrayColor[rowIndex, i] = color;
			}
			Invalidate();
		}

		/// <summary>
		/// 将某一行的颜色设置为指定的颜色
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="colors">颜色信息</param>
		public void SetColorByRow(int rowIndex, Color[] colors)
		{
			for (int i = 0; i < col; i++)
			{
				if (i < colors.Length)
				{
					arrayColor[rowIndex, i] = colors[i];
				}
			}
			Invalidate();
		}

		/// <summary>
		/// 将某一行的颜色都复位为默认的颜色
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		public void ResetColorByRow(int rowIndex)
		{
			for (int i = 0; i < col; i++)
			{
				arrayColor[rowIndex, i] = defaultColor;
			}
			Invalidate();
		}

		/// <summary>
		/// 将某一列的颜色都设置为指定的颜色
		/// </summary>
		/// <param name="colIndex">列索引</param>
		/// <param name="color">颜色信息</param>
		public void SetColorByCol(int colIndex, Color color)
		{
			for (int i = 0; i < row; i++)
			{
				arrayColor[i, colIndex] = color;
			}
			Invalidate();
		}

		/// <summary>
		/// 将某一列的颜色设置为指定的颜色
		/// </summary>
		/// <param name="colIndex">列索引</param>
		/// <param name="colors">颜色信息</param>
		public void SetColorByCol(int colIndex, Color[] colors)
		{
			for (int i = 0; i < row; i++)
			{
				if (i < colors.Length)
				{
					arrayColor[i, colIndex] = colors[i];
				}
			}
			Invalidate();
		}

		/// <summary>
		/// 将某一列的颜色都复位为默认的颜色
		/// </summary>
		/// <param name="colIndex">列索引</param>
		public void ResetColorByCol(int colIndex)
		{
			for (int i = 0; i < row; i++)
			{
				arrayColor[i, colIndex] = defaultColor;
			}
			Invalidate();
		}

		/// <summary>
		/// 将所有的位置的颜色都更新到指定的颜色
		/// </summary>
		/// <param name="color">颜色信息</param>
		public void SetColorAll(Color color)
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					arrayColor[i, j] = color;
				}
			}
			Invalidate();
		}

		/// <summary>
		/// 将所有的位置的颜色都更新到指定的颜色
		/// </summary>
		/// <param name="colors">颜色信息</param>
		public void SetColorAll(Color[] colors)
		{
			int index = 0;
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					if (index >= colors.Length)
					{
						break;
					}
					arrayColor[i, j] = colors[index];
					index++;
				}
			}
			Invalidate();
		}

		/// <summary>
		/// 将所有的位置的颜色都复位到指定的颜色
		/// </summary>
		public void ResetColorAll()
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					arrayColor[i, j] = defaultColor;
				}
			}
			Invalidate();
		}

		/// <summary>
		/// 获取指定位置的颜色信息，指定行索引，列索引
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="colIndex">列索引</param>
		/// <returns>颜色信息</returns>
		public Color GetColorFromPosition(int rowIndex, int colIndex)
		{
			return arrayColor[rowIndex, colIndex];
		}

		/// <summary>
		/// 设置指定行列的描述文本信息
		/// </summary>
		/// <param name="rowIndex">行索引</param>
		/// <param name="colIndex">列索引</param>
		/// <param name="description">描述信息</param>
		public void SetDescription(int rowIndex, int colIndex, string description)
		{
			descriptions[rowIndex, colIndex] = description;
		}

		/// <summary>
		/// 设置所有行列的描述文本信息
		/// </summary>
		/// <param name="description">描述信息</param>
		public void SetAllDescription(string description)
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					descriptions[i, j] = description;
				}
			}
		}

		/// <summary>
		/// 清楚所有行列的描述文本信息
		/// </summary>
		public void ClearAllDescription()
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					descriptions[i, j] = string.Empty;
				}
			}
		}

		/// <summary>
		/// 将所有的位置的描述都更新到指定的描述
		/// </summary>
		/// <param name="descs">描述信息</param>
		public void SetAllDescription(string[] descs)
		{
			int index = 0;
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					if (index >= descs.Length)
					{
						break;
					}
					descriptions[i, j] = descs[index];
					index++;
				}
			}
		}

		private void ArrayInni()
		{
			arrayColor = new Color[row, col];
			descriptions = new string[row, col];
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					arrayColor[i, j] = defaultColor;
					descriptions[i, j] = string.Empty;
				}
			}
		}

		private void HslStatusManagement_Load(object sender, EventArgs e)
		{
			base.MouseMove += HslStatusManagement_MouseMove;
			base.MouseLeave += HslStatusManagement_MouseLeave;
		}

		private void HslStatusManagement_MouseLeave(object sender, EventArgs e)
		{
			activePoint = new Point(-1, -1);
			Refresh();
		}

		private void HslStatusManagement_MouseMove(object sender, MouseEventArgs e)
		{
			activePoint = new Point(e.X, e.Y);
			Refresh();
		}

		private void HslStatusManagement_Paint(object sender, PaintEventArgs e)
		{
			if (activePoint.X < 0 || activePoint.Y < 0 || base.Width < 10 || base.Height < 10)
			{
				return;
			}
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			float itemWidth = (float)base.Width * 1f / (float)col;
			float itemHeight = (float)base.Height * 1f / (float)row;
			int rowIndex = (int)((float)activePoint.Y / itemHeight);
			int columnIndex = (int)((float)activePoint.X / itemWidth);
			if (!((float)activePoint.X < (float)columnIndex * itemWidth + offect / 2f) && !((float)activePoint.X > (float)(columnIndex + 1) * itemWidth - offect / 2f) && !((float)activePoint.Y < (float)rowIndex * itemHeight + offect / 2f) && !((float)activePoint.Y > (float)(rowIndex + 1) * itemHeight - offect / 2f) && rowIndex < row && columnIndex < col && !string.IsNullOrEmpty(descriptions[rowIndex, columnIndex]))
			{
				float textWidth = g.MeasureString(descriptions[rowIndex, columnIndex], Font).Width;
				RectangleF rectangle = new RectangleF(activePoint.X + 10, activePoint.Y - 10, textWidth + 20f, Font.Height + 10);
				if ((float)activePoint.X > (float)base.Width - textWidth - 20f)
				{
					rectangle.X = (float)activePoint.X - textWidth - 20f;
				}
				if (activePoint.Y > base.Height - Font.Height - 10)
				{
					activePoint.Y = activePoint.Y - Font.Height - 10;
				}
				g.FillRectangle(Brushes.DimGray, rectangle);
				g.DrawString(descriptions[rowIndex, columnIndex], Font, Brushes.Yellow, rectangle, sf);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslStatusManagement";
			base.Size = new System.Drawing.Size(375, 124);
			base.Load += new System.EventHandler(HslStatusManagement_Load);
			base.Paint += new System.Windows.Forms.PaintEventHandler(HslStatusManagement_Paint);
			ResumeLayout(false);
		}
	}
}
